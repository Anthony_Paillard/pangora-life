local autorized = true

function ban_request( info,type)
	if info ~= nil then
		PerformHttpRequest("https://www.family-v.fr/global_ban_api/bans/search.php/?id="..info.."&warning="..Config.WarningLevel, function(err, rText, headers)
            if(err==200)then
                    autorized= false
                    print("--[GLOBAL BAN] We got him boys ("..type..")")
                end
            end, "GET", "", {["Content-Type"] = "application/json"});
	end
end

AddEventHandler("playerConnecting", function(name, setCallback, deferrals)

    local _source = source
    deferrals.defer()

    Wait(1500)
    ban_request(GetPlayerIdentifiers(_source)[1],"steam")
    deferrals.update(': Vérification de vos données étape 1 . . .')

    Wait(1500)
    ban_request(GetPlayerIdentifiers(_source)[2],"licence")
    deferrals.update(': Vérification de vos données étape 2 . . .')

    Wait(1500)
    ban_request(GetPlayerIdentifiers(_source)[3],"ip")
    deferrals.update(': Vérification de vos données étape 3 . . .')

    Wait(1500)
    if(autorized)then
        deferrals.done()
        else
        deferrals.done("Désolé, vous n'êtes pas autorisé à nous rejoindre :)")
    end
    autorized = true

    end)