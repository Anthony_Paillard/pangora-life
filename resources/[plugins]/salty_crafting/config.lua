Config = {}

-- Ammo given by default to crafted weapons
Config.WeaponAmmo = 0

Config.Recipes = {
	-- Can be a normal ESX item
	--["lockpick"] = { 
		--{item = "bobbypin", quantity = 4 }, 
		--{item = "rubberband", quantity = 1 },
	--},
	
	-- Can be a weapon, must follow this format
	['WEAPON_KNIFE'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "manche", quantity = 2},
		{item = "planknife", quantity = 1},
	},
	['WEAPON_KNUCKLE'] = { 
		{item = "steel", quantity = 10 },
        {item = "planknucle", quantity = 1},		
	},
	['WEAPON_BAT'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "manche", quantity = 2 },
		{item = "planbat", quantity = 1 },
	},
	['WEAPON_DAGGER'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "plandagger", quantity = 1 }, 
		{item = "manche", quantity = 2 }, 
	},
	['WEAPON_HATCHET'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "planmachete", quantity = 1},
		{item = "manche", quantity = 2 }, 
	},
	['WEAPON_SWITCHBLADE'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "manche", quantity = 2},
		{item = "planswithblade", quantity = 1 }, 
	},
	['WEAPON_REVOLVER'] = { 
		{item = "steel", quantity = 10 }, 
		{item = "cadreREVOLVER", quantity = 1},
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
	},
	['WEAPON_PISTOL'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 },
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadrepistol", quantity = 1 },
	},
	['WEAPON_PISTOL_MK2'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadrepistolmk2", quantity = 1},
	},
	['WEAPON_COMBATPISTOL'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreCOMBATPISTOL", quantity = 1 },
	},
	['WEAPON_APPISTOL'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
	    {item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreAPPISTOL", quantity = 1 },
	},
	['WEAPON_PISTOL50'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadrePISTOL50", quantity = 1 },
	},
	['WEAPON_SNSPISTOL'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreSNSPISTOL", quantity = 1 },
	},
	['WEAPON_HEAVYPISTOL'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreHEAVYPISTOL", quantity = 1 },
	},
	['WEAPON_VINTAGEPISTOL'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreVINTAGEPISTOL", quantity = 1 },
	},
	['WEAPON_STUNGUN'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreSTUNGUN", quantity = 2},
		{item = "circuitelectronique", quantity = 10 }, 
	},
	['WEAPON_FLAREGUN'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5},
		{item = "colle", quantity = 4 }, 
		{item = "cadreFLAREGUN", quantity = 1 },
	},
	['WEAPON_SMG_MK2'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadreSMG_MK2", quantity = 1 },
	},
	['WEAPON_CARBINERIFLE_MK2'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadreCARBINERIFLE_MK2", quantity = 1 },
	},
	['WEAPON_PUMPSHOTGUN'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadrePUMPSHOTGUN", quantity = 1 },
	},
	['WEAPON_MUSKET'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadreMUSKET", quantity = 1 },
	},
	['WEAPON_HEAVYSNIPER'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 },
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadreHEAVYSNIPER", quantity = 1 },
	},
	['WEAPON_GRENADELAUNCHER_SMOKE'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadreGRENADELAUNCHER_SMOKE", quantity = 1 },
	},
	['WEAPON_DOUBLEACTION'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 5 },
		{item = "colle", quantity = 4 }, 
		{item = "cadreDOUBLEACTION", quantity = 1 },
	},
	['WEAPON_SPECIALCARBINE_MK2'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadreSPECIALCARBINE_MK2", quantity = 1 },
	},
	['WEAPON_PUMPSHOTGUN_MK2'] = { 
		{item = "steel", quantity = 4 }, 
		{item = "peinture", quantity = 2 }, 
		{item = "ressort", quantity = 10 },
		{item = "colle", quantity = 10 }, 
		{item = "cadrePUMPSHOTGUN_MK2", quantity = 1 }
	}
}

-- Enable a shop to access the crafting menu
Config.Shop = {
	useShop = true,
	shopCoordinates = { x=885.62, y=-3199.62, z=-99.2 },
	shopName = "Poste de Fabrication",
	--shopBlipID = 446,
	zoneSize = { x = 2.5, y = 2.5, z = 1.5 },
	zoneColor = { r = 255, g = 0, b = 0, a = 100 }
}

-- Enable crafting menu through a keyboard shortcut
Config.Keyboard = {
	useKeyboard = false,
	keyCode = 303
}
