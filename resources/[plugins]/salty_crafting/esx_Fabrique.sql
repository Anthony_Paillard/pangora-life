INSERT INTO `items` (`id`, `name`, `label`, `limit`) VALUES  
    ('manche', 'Manche Arme', 30),
    ('cadrepistolmk2', 'Cadre Pistol MK2', 5),
    ('cadrepistol', 'Cadre Pistolet', 5),
    ('cadreCOMBATPISTOL', 'Cadre Pistolet Combat', 5),
    ('cadreAPPISTOL', 'cadre Pistolet Auto', 5),
    ('cadrePISTOL50', 'cadre Pistolet50', 5),
    ('cadreSNSPISTOL', 'Cadre SNS Pistolet', 5),
    ('cadreHEAVYPISTOL', 'Cadre Pistolet lourd', 5),
    ('cadreVINTAGEPISTOL', 'Cadre Pistolet Vintage', 5),
    ('cadreSTUNGUN', 'CadreEnergy Tazer', 5),
    ('cadreFLAREGUN', 'Cadre Pistolet a fusée', 5),
    ('cadreSMG_MK2', 'Cadre SMGMK2', 5),
	('cadreREVOLVER', 'Cadre Revolver', 5),
    ('cadreCARBINERIFLE_MK2', 'Cadre Fusil Assault Mk2', 5),
    ('cadrePUMPSHOTGUN', 'Cadre Fusil a Pompe', 5),
    ('cadreMUSKET', 'Cadre Mousquet', 5),
    ('cadreHEAVYSNIPER', 'Cadre Sniper Lourd', 5),
    ('cadreGRENADELAUNCHER_SMOKE', 'Cadre Grenade fumigène', 5),
    ('cadreDOUBLEACTION', 'Cadre Pistolet DBaction', 5),
    ('cadreSPECIALCARBINE_MK2', 'Cadre Carabine Spéciale MK2', 5),
    ('cadrePUMPSHOTGUN_MK2', 'Cadre Fusil a Pompe MK2', 5),
	('planknife', 'Plan Couteau', 50),
	('planknucle', 'Plan Poing Américain', 50),
	('planbat', 'Plan Batte', 50),
	('plandagger', 'Plan de Dague', 50),
	('planmachete', 'Plan de Machète', 50),
	('planswithblade', 'Plan couteau Cran d\'arret', 50),
	('steel', 'Acier', 50),
;
