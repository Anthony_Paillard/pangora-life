local IsTornadoActive = false
local TornadoPosition = nil
local TornadoDestination = nil
local TornadoGirth = 4.0

local PossiblePositions = {
    -- -210.64 ,-576.35 ,33.59, 200.5
    {x = -210.64, y = -576.35, z = 33.59},
    {x = -321.64, y = -1462.35, z = 25.59},
}

AddEventHandler("gd_tornado:summon", function()
    local start = math.random(#PossiblePositions)
    local destination = math.random(#PossiblePositions-1)
    if start==destination then
        destination = #PossiblePositions
    end
    start = PossiblePositions[start]
    destination = PossiblePositions[destination]
    TornadoPosition = start
    TornadoDestination = destination
    IsTornadoActive = true
    TriggerClientEvent("gd_tornado:spawn", -1, start, destination)
    print("[Tornado] A tornado has spawned at " .. start.x .. ", " .. start.y .. ", " .. start.z)
end)

AddEventHandler("gd_tornado:move_here", function(x,y,z)
    x,y,z=tonumber(x),tonumber(y),tonumber(z)
    if x~=nil and y~=nil and z~=nil then
        TornadoDestination = {x=x,y=y,z=z}
        if not IsTornadoActive then
            TornadoPosition = PossiblePositions[math.random(#PossiblePositions)]
            print("[Tornado] A tornado has spawned at " .. TornadoPosition.x .. ", " .. TornadoPosition.y .. ", " .. TornadoPosition.z)
        end
        IsTornadoActive = true
        TriggerClientEvent("gd_tornado:spawn", -1, TornadoPosition, TornadoDestination)
        print("[Tornado] A tornado is moving to " .. x .. ", " .. y .. ", " .. z)
    end
end)

AddEventHandler("gd_tornado:summon_right_here", function(x,y,z)
    x,y,z=tonumber(x),tonumber(y),tonumber(z)
    if x~=nil and y~=nil and z~=nil then
        TornadoPosition = {x=x,y=y,z=z}
        if not IsTornadoActive then
            TornadoDestination = PossiblePositions[math.random(#PossiblePositions)]
        end
        IsTornadoActive = true
        TriggerClientEvent("gd_tornado:spawn", -1, TornadoPosition, TornadoDestination)
        print("[Tornado] A tornado has spawned at " .. x .. ", " .. y .. ", " .. z)
    end
end)

AddEventHandler("gd_tornado:dismiss", function()
    IsTornadoActive = false
    TriggerClientEvent("gd_tornado:delete", -1)
end)

RegisterCommand("tornado_summon", function()
    TriggerEvent("gd_tornado:summon")
end, true)

RegisterCommand("tornado_dismiss", function()
    TriggerEvent("gd_tornado:dismiss")
end, true)
