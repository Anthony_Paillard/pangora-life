local HasAlreadyEnteredMarker, LastZone = false, nil
local CurrentAction, CurrentActionMsg, CurrentActionData = nil, '', {}
local CurrentlyTowedVehicle, Blips, NPCOnJob, NPCTargetTowable, NPCTargetTowableZone = nil, {}, false, nil, nil
local NPCHasSpawnedTowable, NPCLastCancel, NPCHasBeenNextToTowable, NPCTargetDeleterZone = false, GetGameTimer() - 5 * 60000, false, false
local isDead, isBusy = false, false

ESX = nil



Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	ESX.PlayerData = ESX.GetPlayerData()
end)


RegisterNetEvent('trainsportation:spawn')
AddEventHandler('trainsportation:spawn', function(id, location)
	createTrain(id, location.x, location.y, location.z)
end)

function doTrains()
	if Config.ModelsLoaded then
		if (Config.EnterExitDelay == 0) then
			Config.EnterExitDelay = GetGameTimer()
		end
		if (Config.inTrain) then
			-- Speed Up/Forwards (W)
			if (IsControlPressed(0,71) and IsControlPressed(0,72) and Config.Debug and Config.Speed ~= 0) and (ESX.PlayerData.job.name == 'taxi')  then -- D(E)bug Break (W+S)
				debugLog("break:" .. GetEntityCoords(Config.TrainVeh))
				Config.Speed = 0
				SetTrainSpeed(Config.TrainVeh, 0)
			elseif (IsControlPressed(0,73))  and (ESX.PlayerData.job.name == 'taxi') then -- E Break (X)
				Config.Speed = 0
			elseif (IsControlPressed(0,71) and Config.Speed < getTrainSpeeds(Config.TrainVeh).MaxSpeed) and (ESX.PlayerData.job.name == 'taxi')  then  -- Forward (W)
				debugLog("W: " .. Config.Speed)
				Config.Speed = Config.Speed + getTrainSpeeds(Config.TrainVeh).Accel
			elseif (IsControlPressed(0,72) and Config.Speed  > -getTrainSpeeds(Config.TrainVeh).MaxSpeed) and (ESX.PlayerData.job.name == 'taxi') then -- Backwards (S)
				debugLog("S: " .. Config.Speed)
				Config.Speed = Config.Speed - getTrainSpeeds(Config.TrainVeh).Dccel
			end
			
			SetTrainCruiseSpeed(Config.TrainVeh,Config.Speed)
		elseif IsPedInAnyTrain(GetPlayerPed(-1)) then -- Should fix not being able to drive trains after restart resource.
			debugLog("I'm in a train? Did the resource restart...")
			if GetVehiclePedIsIn(GetPlayerPed(-1), false) == 0 then
				debugLog("Unable to get train, re-enter the train, or wait!")
			else
				debugLog("T:" .. GetVehiclePedIsIn(GetPlayerPed(-1), false) .. "|M:" .. GetEntityModel(GetVehiclePedIsIn(GetPlayerPed(-1), false)))
				Config.TrainVeh = GetVehiclePedIsIn(GetPlayerPed(-1), false)
				Config.inTrain = true
			end
		end

		-- Enter/Exit (F)
		if(IsControlPressed(0,75) and(GetGameTimer() - Config.EnterExitDelay) > Config.EnterExitDelayMax) then
			Config.EnterExitDelay = 0
			
			if(Config.inTrain or Config.inTrainAsPas) then
				--debugLog("exit")
				if (Config.TrainVeh ~= 0) then
					local off = GetOffsetFromEntityInWorldCoords(Config.TrainVeh, -2.0, -5.0, 0.6)
					SetEntityCoords(GetPlayerPed(-1), off.x, off.y, off.z,false,false,false,false)
				end
				Config.inTrain = false
				Config.inTrainAsPas = false
				Config.TrainVeh = 0
			else
				Config.TrainVeh = findNearestTrain()
				if (Config.TrainVeh ~= 0) then
					if (GetPedInVehicleSeat(Config.TrainVeh, 1) == 0) then -- If train has driver, then enter the back
						SetPedIntoVehicle(GetPlayerPed(-1),Config.TrainVeh,-1)
						Config.inTrain = true
						--debugLog("Set into Train!")
						--debugLog("T:" .. GetVehiclePedIsIn( ped, false ) .. "|M:" .. GetEntityModel(GetVehiclePedIsIn( ped, false )))
					elseif getCanPassenger(Config.TrainVeh) then
						local off = GetOffsetFromEntityInWorldCoords(Config.TrainVeh, 0.0, -5.0, 0.6)
						SetEntityCoords(GetPlayerPed(-1), off.x, off.y, off.z)
						Config.inTrainAsPas = true
						--debugLog("Set into Train as Passenger!")
						--debugLog("T:" .. GetVehiclePedIsIn( ped, false ) .. "|M:" .. GetEntityModel(GetVehiclePedIsIn( ped, false )))
					end
				end
			end
		end
		
		-- KP8 to delete train infront
		if(IsControlPressed(0,111) and(GetGameTimer() - Config.EnterExitDelay) > Config.EnterExitDelayMax) then
			Config.EnterExitDelay = 0
			local nT = findNearestTrain()
			if nT ~= 0 then
				DeleteMissionTrain(nT)
				Config.inTrain = false -- F while train doesn't have driver
				Config.inTrainAsPas = false -- F while train has driver
				Config.TrainVeh = 0
				Config.Speed = 0
			end
		end
	end
end

-- Load Models
Citizen.CreateThread(function()
	function RequestModelSync(mod) -- eh
		tempmodel = GetHashKey(mod)
		RequestModel(tempmodel)
		while not HasModelLoaded(tempmodel) do
			RequestModel(tempmodel)
			Citizen.Wait(0)
		end
	end

	function LoadTrainModels()
		DeleteAllTrains()
		Config.ModelsLoaded = false
		Log("Loading Train Models.")
		RequestModelSync("freight")
		RequestModelSync("freightcar")
		RequestModelSync("freightgrain")
		RequestModelSync("freightcont1")
		RequestModelSync("freightcont2")
		RequestModelSync("freighttrailer")
		RequestModelSync("tankercar")
		RequestModelSync("metrotrain")
		RequestModelSync("s_m_m_lsmetro_01")
		Log("Done Loading Train Models.")
		Config.ModelsLoaded = true
	end
	LoadTrainModels()
	
	while true do
		Wait(0)
		doTrains()
	end
end)