local blips = {
    -- Exemple {title="", colour=, id=, x=, y=, z=},
	 {title="Sécuro Serv", colour=75, id=120, x = -1372.93, y = -464.22, z = 71.04},
	 --{title="Taxi", colour=5, id=198, x = 911.108, y = -177.867, z = 74.283 },
	 {title="Los Santos Service", colour=4, id=605, x = -1348.52, y = -750.09, z = 22.31 },
}

Citizen.CreateThread(function()

    for _, info in pairs(blips) do
      info.blip = AddBlipForCoord(info.x, info.y, info.z)
      SetBlipSprite(info.blip, info.id)
      SetBlipDisplay(info.blip, 4)
      SetBlipScale(info.blip, 1.0)
      SetBlipColour(info.blip, info.colour)
      SetBlipAsShortRange(info.blip, true)
	  BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(info.title)
      EndTextCommandSetBlipName(info.blip)
    end
end)
