Config                            = {}

Config.Teleporters = {
	['BunkerArmesEntrée'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 971.77,
			['y'] = -2406.01,
			['z'] = 30.49,
			['Information'] = 'Entrer Dans la Fabrique D\'armes',
		},
		['Exit'] = {
			['x'] = 903.08,
			['y'] = -3182.44,
			['z'] = -98.06,
			['Information'] = 'Sortir'
		}
	},


	['SaintFiacre'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 1124.52,
			['y'] = -1522.96,
			['z'] = 34.00,
			['Information'] = 'Entrée Dans l\'Hopital'
		},

		['Exit'] = {
			['x'] = 1141.46,
			['y'] = -1593.81,
			['z'] = 4.00,
			['Information'] = 'Sortir'
		}
	},

	['LSPD'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 638.41,
			['y'] = 2.24,
			['z'] = 81.78,
			['Information'] = 'Bienvenue à la LSPD'
		},

		['Exit'] = {
			['x'] = 617.67,
			['y'] = -11.74,
			['z'] = 89.32,
			['Information'] = 'Sortir Du Batiment'
		}
	},

	['LSPD2'] = {
		['Job'] = 'police',
		['Enter'] = {
			['x'] = 619.99,
			['y'] = 17.04,
			['z'] = 87.00,
			['Information'] = 'Entrée Coté Rue'
		},

		['Exit'] = {
			['x'] = 624.77,
			['y'] = -2.72,
			['z'] = 43.39,
			['Information'] = 'Sortir'
		}
	},

	['LSPD3'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 611.94,
			['y'] = 5.31,
			['z'] = 43.00,
			['Information'] = 'Remonter dans les Bureaux'
		},

		['Exit'] = {
			['x'] = 618.45,
			['y'] = -10.38,
			['z'] = 89.32,
			['Information'] = 'Descendre'
		}
	},

	['LaboWeed'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 1459.07,
			['y'] = -1930.54,
			['z'] = 70.8,
			['Information'] = 'Labo Weed'
		},

		['Exit'] = {
			['x'] = 1065.001,
			['y'] = -3183.407,
			['z'] = -40.163,
			['Information'] = 'Sortir'
		}
	},

	['LaboMeth'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 1085.91,
			['y'] = -2400.34,
			['z'] = 29.66,
			['Information'] = 'Labo Meth'
		},

		['Exit'] = {
			['x'] = 997.745,
			['y'] = -3200.754,
			['z'] = -36.397,
			['Information'] = 'Sortir'
		}
	},

	['LaboOpium'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 226.79,
			['y'] = -1776.36,
			['z'] = 27.96,
			['Information'] = 'Labo Opium'
		},

		['Exit'] = {
			['x'] = 1088.39,
			['y'] = -3188.42,
			['z'] = -39.0,
			['Information'] = 'Sortir'
		}
	},


	['HELICO'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 1146.65,
			['y'] = -1559.60,
			['z'] = 4.00,
			['Information'] = 'Sortie vers Hélico'
		},

		['Exit'] = {
			['x'] = 1132.74,
			['y'] = -1596.63,
			['z'] = 34.00,
			['Information'] = 'Entée Arrière'
		}
	},

	['BureauSecuro'] = {
        ['Job'] = 'none',
        ['Enter'] = {
            ['x'] =-1370.21,
            ['y'] =-503.29,
            ['z'] = 32.15 ,
            ['Information'] = 'BureauSecuro'
        },

        ['Exit'] = {
            ['x'] =-1396.03,
            ['y'] = -478.78,
            ['z'] = 71.04,
            ['Information'] = 'Sortir'
        }
    },

['AscenceurSecuro'] = {
        ['Job'] = 'none',
        ['Enter'] = {
            ['x'] =-1392.84,
            ['y'] =-471.95,
            ['z'] = 83.44 ,
            ['Information'] = 'Monter Sur Le Toit'
        },

        ['Exit'] = {
            ['x'] =-1396.03,
            ['y'] = -478.78,
            ['z'] = 71.04,
            ['Information'] = 'Redescendre'
        }
    },

	--Next here
}
