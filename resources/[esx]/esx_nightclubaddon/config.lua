Config = {}

--C
Config.actionKey = 26

-- Marker
Config.zones = {

	NightclubEnter = {
		x = -1285.43,
		y = -651.11,
		z = 26.58,
		w = 2.0,
		h = 1.0,		
		visible = true,
		t = 31,
		color = {
			r = 102,
			g = 0,
			b = 0
		}
	},
	
	NightclubExit = {
		x = -1569.54,
		y = -3017.46,
		z = -74.30,
		w = 2.0,
		h = 1.0,		
		visible = true,
		t = 31,
		color = {
			r = 102,
			g = 0,
			b = 0
		}
	},
}

--L
Config.point = {

	NightclubEnter = {
		x = -1569.63,
		y = -3013.52,
		z = -75.50
	},
	
	NightclubExit = {
		x = -1289.04,
		y = -651.81,
		z = 25.58
	}
}


-- Automatic teleport
Config.auto = {
	'Nightclub',
	'NightclubEnter',
	'NightclubExit'
}
