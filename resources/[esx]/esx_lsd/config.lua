Config              = {}
Config.MarkerType   = 1
Config.DrawDistance = 100.0
Config.ZoneSize     = {x = 5.0, y = 5.0, z = 3.0}
--Config.MarkerColor  = {r = 100, g = 204, b = 100}

Config.RequiredCopsCoke  = 2
Config.RequiredCopsMeth  = 2
Config.RequiredCopsWeed  = 2
Config.RequiredCopsOpium = 2

Config.TimeToFarm    = 17 * 1000
Config.TimeToProcess = 46 * 1000
Config.TimeToSell    = 5  * 1000

Config.Locale = 'fr'

Config.Zones = {
	CokeField =			{x = 3823.32,   y = 4439.69,    z = 1.8,	name = _U('coke_field'),		sprite = 501,	color = 40},
	CokeProcessing =	{x = 1389.28,   y = 3604.81,    z = 37.94,	name = _U('coke_processing'),	sprite = 478,	color = 40},
	CokeDealer =		{x = -1126.29,  y = -1147.36,   z = 4.05,	name = _U('coke_dealer'),		sprite = 500,	color = 75},
	MethField =			{x = 102.46,    y = -3739.19,   z = 38.72,	name = _U('meth_field'),		sprite = 499,	color = 26},
	MethProcessing =	{x = 1005.49,   y = -3199.97,   z = -37.522,name = _U('meth_processing'),	sprite = 499,	color = 26},
	MethDealer =		{x = 1417.61,   y = 6339.29,    z = 23.39,	name = _U('meth_dealer'),		sprite = 500,	color = 75},
	WeedField =			{x = -1131.75,  y = 4937.15,    z = 221.27,	name = _U('weed_field'),		sprite = 496,	color = 52},
	WeedProcessing =	{x = 1037.22,   y = -3205.30,   z = -38.170,name = _U('weed_processing'),	sprite = 496,	color = 52},
	WeedDealer =		{x = -69.77,    y = -1231.23,   z = 31.48,	name = _U('weed_dealer'),		sprite = 500,	color = 75},
	OpiumField =		{x = -1146.29,	y = 4940.79,	z = 222.26,	name = _U('opium_field'),		sprite = 51,	color = 60},
	OpiumProcessing =	{x = 1094.27,	y = -3194.48,	z = -38.99,	name = _U('opium_processing'),	sprite = 51,	color = 60},
	OpiumDealer =		{x = 917.79,	y = 3655.29,	z = 31.48,	name = _U('opium_dealer'),		sprite = 500,	color = 75}
}
