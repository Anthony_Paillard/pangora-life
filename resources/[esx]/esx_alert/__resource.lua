server_scripts {
	'config.lua',
	'server.lua'
}
client_script {
	'config.lua',
	'client.lua'
}

ui_page {
	'html/index.html'
}

files {
	'html/index.html',
	'html/main.css',
	'html/main.js',
	'html/media/font/Bariol_Regular.otf',
	'html/media/font/Vision-Black.otf',
	'html/media/font/Vision-Bold.otf',
	'html/media/font/Vision-Heavy.otf',
	'html/media/img/bn.png',
	'html/media/img/logo.png',
	'html/media/img/lspd.png',
}
