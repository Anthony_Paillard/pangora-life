$(function() {
    window.addEventListener('message', function(event) {
        if (event.data.type == "alert") {
            switch (event.data.issuer) {
                case 'restart':
                    $("#live").attr('class', 'info');
                    $("#message").attr('class', '');
                    $("#logo").attr('src', 'media/img/bn.png');
                    break;
                case 'lspd':
                    $("#live").attr('class', 'info blue');
                    $("#message").attr('class', 'm-blue');
                    $("#logo").attr('src', 'media/img/lspd.png');
                    break;
                default:
                    $("#live").attr('class', 'info blue');
                    $("#message").attr('class', '');
                    $("#logo").attr('src', 'media/img/logo.png');
                    break;
            }

            $('#message').html(event.data.message);

            document.body.style.display = event.data.enable ? "block" : "none";
            setTimeout(function(){
                document.body.style.display = "none";
            }, 31000);
        }
    });
    
    document.body.style.display = "none";
});