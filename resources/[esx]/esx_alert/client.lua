RegisterNetEvent("SendAlert")
AddEventHandler("SendAlert", function(issuer, msg)
	SendNUIMessage({
		type    = "alert",
		enable  = true,
		issuer  = issuer,
		message = msg
	})
end)

RegisterNetEvent("alert:Send")
AddEventHandler("alert:Send", function(issuer)
	DisplayOnscreenKeyboard(1, "", "", "", "", "", "", 1200)
	while (UpdateOnscreenKeyboard() == 0) do
		DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
		local msg = GetOnscreenKeyboardResult()
		TriggerServerEvent("alert:sv", issuer, msg)
	end
end)

RegisterNetEvent("alert:SendMsg")
AddEventHandler("alert:SendMsg", function(issuer, msg)
	--DisplayOnscreenKeyboard(1, "", "", "", "", "", "", 1200)
		TriggerServerEvent("alert:sv", issuer, msg)
end)