Locales['fr'] = {
  -- Cloakroom
  ['vehicle_list'] = 'Sortir Véhicule',
  ['civil'] = 'tenue civile',
  ['job_clothes'] = 'tenue de travail',
  ['foodtruck_actions_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au menu.',
  
  -- Vehicles
  ['store_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
  ['wrong_veh'] = 'Vous ne pouvez ranger que des ~b~véhicules du Foodtruck~s~.',
  ['vehicles'] = 'véchicules',

  -- Menus
  ['recipe'] = 'recette',
  ['ingredients'] = 'ingrédients',
  ['price_unit'] = '$/unité',
  ['on_you'] = 'sur vous',
  ['action'] = 'action',
  ['buy_10'] = 'acheter 10',
  ['buy_50'] = 'acheter 50',
  ['cook'] = 'préparer',
  ['billing'] = 'facturation',
  ['gear'] = 'matériel',
  ['gears'] = 'matériel',
  ['purchased'] = 'Achat effectué.',
  ['deposit_stock'] = 'Déposer stock',
  ['take_stock'] = 'Prendre stock',
  ['inventory'] = 'Inventaire',
  ['quantity'] = 'Quantité',
  ['quantity_invalid'] = 'Quantité Invalide',
  ['added'] = 'Ajouté',
  ['have_withdrawn'] = 'Tu as récupéré',
    ['not_enough']= 'Pas assez de ~r~ ',
	['no_eau_sale']= '~r~Vous n\'avez plus de Viande',
	['press_collect_eau']= '~INPUT_CONTEXT~ Pour récolter de la ~g~Viande',
	['sold_one_eau']= 'Vous avez vendu ~g~x1 Viande~s~',
	['press_sell_eau']= '~INPUT_CONTEXT~ Pour vendre de la ~g~Viande',
	['sale_in_prog']= '~b~Vente en cours',
	['press_sell_raisin']= '~INPUT_CONTEXT~ Pour vendre de la ~g~Viande',
    ['inv_full']= '~r~Vous etes est plein',
	['no_pain_sale']= '~r~Vous n\'avez plus de Ressort',
	['press_collect_pain']= '~INPUT_CONTEXT~ Pour récolter de ~g~la salades.',
	['sold_one_pain']= 'Vous avez vendu ~g~x1 Ressort~s~',
	['press_sell_pain']= '~INPUT_CONTEXT~ Pour vendre des ~g~Ressorts',
	['earn_pain']= 'Vous avez gagné 4$',
	['earn_eau']= 'Vous avez gagné 4$',
	['earn_soci']= 'Votre société a gagnée 8$',
	['earn_soci2']= 'Votre société a gagnée 8$',
	['pickup_in_prog']= '~b~Ramassage en cours..',
	['press_collect_vin']= '~INPUT_CONTEXT~ Pour récolter du ~g~Cola',
	['GPS']= 'GPS Tacos',





  -- Boss Menu
  ['boss_actions'] = 'actions patron',

  -- Food
  ['grill'] = 'grill',
  ['table'] = 'table',
  ['chair'] = 'chair',
  ['clean'] = 'Ranger matériel',
  ['bread'] = 'pain',
  ['meat'] = 'viande',
  ['vegetables'] = 'légumes',
  ['tacos'] = 'tacos',
  ['burger'] = 'burger',
  ['drank_coke'] = 'Vous avez bu un ~b~Cola',
  ['eat_burger'] = 'Vous avez mangé un ~b~Burger',
  ['eat_taco'] = 'Vous avez mangé un ~b~Tacos',
  ['eat_sushi'] = 'Vous avez mangé un ~b~Sushi',

  -- F6 Menu

  -- Phone
  ['alert_foodtruck'] = 'alerte Client',
  -- MISC
  ['blip_market'] = 'marché',
  ['blip_foodtruck'] = 'tacos',

  ['take'] = 'appuyez sur ~INPUT_CONTEXT~ pour prendre le',

  ['clean_too_far'] = 'vous êtes trop éloigné de votre matériel pour le ranger.',
  ['cooked'] = 'cuisson terminée !',
  ['no_player_nearby'] = 'Aucun joueur à proximité',
  ['bill_amount'] = 'Montant de la facture',
  ['missing_ingredients'] = 'il vous manque des ingrédients !',
  ['already_cooking'] = 'une recette est déjà en préparation !',
  ['start_cooking_hint'] = 'appuyez sur ~INPUT_CONTEXT~ pour cuisiner.',
  ['need_more_exp'] = 'Vous n\'êtes ~r~pas assez expérimenté~s~ pour effectuer cette action.',
  ['foodtruck_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder menu.',
  ['foodtruck_market_menu'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au marché.',
  ['no_money'] = 'Vous n\'avez pas assez d\'argent',
}
