Config                        = {}
Config.DrawDistance           = 100.0
Config.Locale                 = 'fr'
Config.PriceResell            = {eau = 9}

local seconde = 1000
local minute = 60 * seconde

Config.Fridge = {
	meat = 300,
	packaged_chicken = 100,
	bread = 200,
	water = 100,
	cola = 100,
	vegetables = 100
} -- maxquantity

Config.Recipes = {
	tacos = {
		Ingredients = {
			bread 				= { "Pain"		, 1 },
			meat				= { "Viande"	, 2 },
			vegetables 			= { "Légumes"	, 1 }
		},
		Price = 100,
		CookingTime = 30 * seconde,
		Item = 'tacos',
		Name = 'Tacos'
	},
	burger = {
		Ingredients = {
			bread 				= { "Pain"		, 1 },
			meat		= { "Viande"	, 2 },
			vegetables 			= { "Légumes"	, 1 }
		},
		Price = 100,
		CookingTime = 30 * seconde,
		Item = 'burger',
		Name = 'Burger'
	}
}

Config.Zones = {
	Actions = {
		Pos   = {x = 20.602, y = -1604.479, z = 28.390},
		Size  = {x = 1.5, y = 1.5, z = 0.4},
		Color = {r = 102, g = 102, b = 204},
		Type  = 27
	},
	VehicleSpawnPoint = {
		Pos   = {x = 33.698, y = -1606.941, z = 29.277},
		Size  = {x = 3.0, y = 3.0, z = 0.4},
		Type  = -1
	},
	VehicleDeleter = {
		Pos   = {x = 17.169, y = -1611.975, z = 28.282},
		Size  = {x = 3.0, y = 3.0, z = 0.4},
		Color = {r = 255, g = 0, b = 0},
		Type  = 27
	},
	Market = {
		Pos   = {x = 0, y = 0, z = 0},
		Size  = {x = 1.5, y = 1.5, z = 0.4},
		Color = {r = 0, g = 255, b = 0},
		Type  = 1
	},
EauFarm = {
        Pos   = { x = 2434.574, y = 5011.574, z = 45.932 },
        Size  = { x = 3.0001, y = 3.0001, z = 1.0001 },
        Color = { r = 255, g = 255, b = 0, a = 160},
        Type  = 27,
    },

PainFarm = {
        Pos   = { x = 290.515, y = 6635.375, z = 28.452 },
        Size  = { x = 3.0001, y = 3.0001, z = 1.0001 },
        Color = { r = 255, g = 255, b = 0, a = 160},
        Type  = 27,
    },

VinFarm = {
        Pos   = { x = 538.002, y = 101.878, z = 95.640 },
        Size  = { x = 3.0001, y = 3.0001, z = 1.0001 },
        Color = { r = 255, g = 255, b = 0, a = 160},
        Type  = 27,
    },


	
	EauResell = {
        Pos   = { x = 131.309, y = 6428.535, z = 31.364 },
        Size  = { x = 2.0001, y = 2.0001, z = 2.0001 },
        Color = { r = 255, g = 255, b = 0, a = 160},
        Type  = 27,
    }

}