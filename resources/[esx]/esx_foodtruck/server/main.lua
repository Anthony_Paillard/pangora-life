ESX                = nil
PlayersHarvesting  = {}
local PlayersHarvestingEau    = {}
local PlayersHarvestingPain    = {}
local PlayersHarvestingVin    = {}
local PlayersSellingEau 	  = {}
MarketPrices		= {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_phone:registerNumber', 'foodtruck', 'Client foodtruck', false, false)
TriggerEvent('esx_society:registerSociety', 'foodtruck', 'foodtruck', 'society_foodtruck', 'society_foodtruck', 'society_foodtruck', {type = 'public'})

  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_foodtruck', function(account)
    societyAccount = account
  end)


if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'foodtruck', Config.MaxInService)
end
---Eau
local function HarvestEau(source)
  
  SetTimeout(5000, function()

    if PlayersHarvestingEau[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local eau = xPlayer.getInventoryItem('meat')
	  local EauQuantity = xPlayer.getInventoryItem('meat').count

      if EauQuantity >= 30 then
        TriggerClientEvent('esx:showNotification', source, _U('inv_full'))
      else
        xPlayer.addInventoryItem('meat', 1)
        HarvestEau(source)
      end

    end
  end)
end
---Pain
local function HarvestPain(source)
  
  SetTimeout(5000, function()

    if PlayersHarvestingPain[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local pain = xPlayer.getInventoryItem('vegetables')
	  local PainQuantity = xPlayer.getInventoryItem('vegetables').count

      if PainQuantity >= 30 then
        TriggerClientEvent('esx:showNotification', source, _U('inv_full'))
      else
        xPlayer.addInventoryItem('vegetables', 1)
        HarvestPain(source)
      end

    end
  end)
end
---Vin
local function HarvestVin(source)
  
  SetTimeout(5000, function()

    if PlayersHarvestingVin[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local vin = xPlayer.getInventoryItem('cola')
	  local VinQuantity = xPlayer.getInventoryItem('cola').count

      if VinQuantity >= 10 then
        TriggerClientEvent('esx:showNotification', source, _U('inv_full'))
      else
        xPlayer.addInventoryItem('cola', 1)
        HarvestVin(source)
      end

    end
  end)
end
RegisterServerEvent('esx_foodtruck:startHarvestVin')
AddEventHandler('esx_foodtruck:startHarvestVin', function()

  local _source = source

  PlayersHarvestingVin[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

  HarvestVin(_source)

end)

RegisterServerEvent('esx_foodtruck:stopHarvestVin')
AddEventHandler('esx_foodtruck:stopHarvestVin', function()

  local _source = source

  PlayersHarvestingVin[_source] = false

end)

RegisterServerEvent('esx_foodtruck:startHarvestPain')
AddEventHandler('esx_foodtruck:startHarvestPain', function()

  local _source = source

  PlayersHarvestingPain[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

  HarvestPain(_source)

end)

RegisterServerEvent('esx_foodtruck:stopHarvestPain')
AddEventHandler('esx_foodtruck:stopHarvestPain', function()

  local _source = source

  PlayersHarvestingPain[_source] = false

end)

RegisterServerEvent('esx_foodtruck:startHarvestEau')
AddEventHandler('esx_foodtruck:startHarvestEau', function()

  local _source = source

  PlayersHarvestingEau[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('pickup_in_prog'))

  HarvestEau(_source)

end)

RegisterServerEvent('esx_foodtruck:stopHarvestEau')
AddEventHandler('esx_foodtruck:stopHarvestEau', function()

  local _source = source

  PlayersHarvestingEau[_source] = false

end)

local function SellEau(source)

  SetTimeout(2000, function()

  	local societyAccount = nil

  	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_foodtruck', function(account)
  		societyAccount = account
  	end)

    if PlayersSellingEau[source] == true and societyAccount ~= nil then

      local xPlayer  = ESX.GetPlayerFromId(source)

      local eauQuantity = xPlayer.getInventoryItem('meat').count

      if eauQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, _U('no_eau_sale'))
      else
        xPlayer.removeInventoryItem('meat', 1)
        xPlayer.addMoney(Config.PriceResell.eau)
        societyAccount.addMoney(Config.PriceResell.eau * 2)
        TriggerClientEvent('esx:showNotification', source, _U('sold_one_eau'))
		TriggerClientEvent('esx:showNotification', source, _U('earn_eau'))
		TriggerClientEvent('esx:showNotification', source, _U('earn_soci'))

        SellEau(source)
      end

    end
  end)
end

RegisterServerEvent('esx_foodtruck:startSellEau')
AddEventHandler('esx_foodtruck:startSellEau', function()

  local _source = source

  PlayersSellingEau[_source] = true

  TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))

  SellEau(_source)

end)

RegisterServerEvent('esx_foodtruck:stopSellEau')
AddEventHandler('esx_foodtruck:stopSellEau', function()

  local _source = source

  PlayersSellingEau[_source] = false

end)
AddEventHandler('onMySQLReady', function ()
	MySQL.Async.fetchAll("SELECT * FROM `shops` WHERE `name` = 'Market'",
		{},
		function(result)
			MySQL.Async.fetchAll("SELECT * FROM `items`",
				{},
				function(result2)
					for i=1, #result2, 1 do
						for j=1, #result, 1 do
							if result[j].item == result2[i].name then
								table.insert(MarketPrices, {label = result2[i].label, item = result[j].item, price = result[j].price})
								break
							end
						end
					end
				end
			)			
		end
	)
end)

RegisterServerEvent('esx_foodtruck:getStockItem')
AddEventHandler('esx_foodtruck:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_foodtruck', function(inventory)

    local item = inventory.getItem(itemName)

    if count > 0 and item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_foodtruck:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_foodtruck', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_foodtruck:putStockItems')
AddEventHandler('esx_foodtruck:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_foodtruck', function(inventory)

    local item = xPlayer.getInventoryItem(itemName)

    if count >= 0 and item.count >= count then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_foodtruck:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)


RegisterServerEvent('esx_foodtruck:buyItem')
AddEventHandler('esx_foodtruck:buyItem', function(qtty, item)
	local _source = source	
	local xPlayer = ESX.GetPlayerFromId(_source)

	for i=1, #MarketPrices, 1 do
		if item == MarketPrices[i].item then
			if qtty == -1 then -- ready for when I add the 'buy max' option
				local delta = max - stock
				local total = MarketPrices[i].price * delta
				if xPlayer.getMoney() > total then
					xPlayer.addInventoryItem(item, delta)
					xPlayer.removeMoney(total)
					TriggerClientEvent('esx:showNotification', _source, _U('purchased'))
				else
					TriggerClientEvent('esx:showNotification', _source, _U('no_money'))
				end
			else
				local total = MarketPrices[i].price * qtty
				if xPlayer.getMoney() > total then
					xPlayer.addInventoryItem(item, qtty)
					xPlayer.removeMoney(total)
					TriggerClientEvent('esx:showNotification', _source, _U('purchased'))
				else
					TriggerClientEvent('esx:showNotification', _source, _U('no_money'))
				end
			end
			break
		end
	end
	
	TriggerClientEvent('esx_foodtruck:refreshMarket', _source)
end)

RegisterServerEvent('esx_foodtruck:removeItem')
AddEventHandler('esx_foodtruck:removeItem', function(item, count)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	xPlayer.removeInventoryItem(item, count)
end)

RegisterServerEvent('esx_foodtruck:addItem')
AddEventHandler('esx_foodtruck:addItem', function(item, count)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	xPlayer.addInventoryItem(item, count)
end)

---------------------------- register usable item --------------------------------------------------
ESX.RegisterUsableItem('cola', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('cola', 1)
	TriggerClientEvent('esx_status:add', source, 'thirst', 300000)
	TriggerClientEvent('esx_basicneeds:onDrink', source, 'prop_ecola_can')
    TriggerClientEvent('esx:showNotification', source, _U('drank_coke'))
end)

ESX.RegisterUsableItem('burger', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('burger', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 300000)
	TriggerClientEvent('esx_basicneeds:onEat', source, 'prop_cs_burger_01')
    TriggerClientEvent('esx:showNotification', source, _U('eat_burger'))
end)

ESX.RegisterUsableItem('tacos', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('tacos', 1)
	TriggerClientEvent('esx_status:add', source, 'hunger', 300000)
	TriggerClientEvent('esx_status:add', source, 'thirst', 300000)
	TriggerClientEvent('esx_basicneeds:onEat', source, 'prop_taco_01')
    TriggerClientEvent('esx:showNotification', source, _U('eat_taco'))
end)

ESX.RegisterUsableItem('sushi', function(source)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('sushi', 1)
	TriggerClientEvent('esx_status:add', source, 'sushi', 300000)
	TriggerClientEvent('esx_basicneeds:onEat', source, 'prop_taco_01')
    TriggerClientEvent('esx:showNotification', source, _U('eat_sushi'))
end)