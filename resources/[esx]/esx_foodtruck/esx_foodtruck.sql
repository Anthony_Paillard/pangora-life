SET @job_name = 'foodtruck';
SET @society_name = 'society_foodtruck';
SET @job_Name_Caps = 'Foodtruck';



INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_foodtruck', 'Foodtruck', 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  ('Foodtruck', 'Foodtruck', 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('Foodtruck', 0, 'cook', 'Cuisinier', 500, '{}', '{}'),
  ('Foodtruck', 1, 'boss', 'Patron', 1000, '{}', '{}')
;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES  
    ('cola', 'Cola', 20),
    ('vegetables', 'Légume', 20),
    ('meat', 'Viande', 20),
    ('tacos', 'Tacos', 20),
    ('burger', 'Burger', 20)
;

INSERT INTO `shops` (`name`, `item`, `price`) VALUES
('LTDgasoline', 'cola', 100),
('LTDgasoline', 'vegetables', 100),
('LTDgasoline', 'meat', 100)
;
