Config                			= {}
Config.DrawDistance   			= 100
Config.Size           			= { x = 1.5, y = 1.5, z = 1.5 }
Config.Color          			= { r = 0, g = 0, b = 0 }
Config.Type           			= 23
Config.Locale         			= 'fr'
Config.EnableLicense  			= true
Config.LicensePrice   			= 10
Config.EnableClipGunShop		= true
Config.EnableClipBlackWeashop	= true

Config.weaponaddons = {
	BlackWeashop = {
		--{name="clip",label="chargeur",price=50},  
		--{name="armor",label="Gilet par balle",price=1200},  
		--{name="flashlight",label="Lampe Torche",price=2400},  
		--{name="nightvision_scope",label="Lunette à Vision Nocturne",price=3600},  
		--{name="grip",label="Poignée",price=2400},  
		--{name="lowrider",label="Skin Lowrider",price=6125},
		--{name="yusuf",label="Skin de Luxe",price=6125},
		--{name="scope",label="Viseur",price=30000},
		--{name="advanced_scope",label="Viseur Avancé",price=60000},
		--{name="lazer_scope",label="Viseur Laser",price=4500},
		--{name="compansator",label="Compensateur",price=6100},
		--{name="barrel",label="Canon",price=8500},
		--{name="sprayweapon",label="Spray Armes",price=20000},
		--{name="cagoule",label="Cagoule",price=200},
		--{name="serflex",label="Serflex",price=70},
		--{name="plier",label="Pince",price=70},
		--{name="sprayweapon",label="Spray Armes",price=20000},
	},
	GunShop = {
		{name="clip",label="chargeur",price=75},
		--{name="armor",label="Gilet par balle",price=2450},  
		--{name="flashlight",label="Lampe Torche",price=4900},  
		--{name="nightvision_scope",label="Lunette à Vision Nocturne",price=7350},  
		--{name="grip",label="Poignée",price=4900},  
		--{name="lowrider",label="Skin Lowrider",price=12250},
		--{name="yusuf",label="Skin de Luxe",price=12250},
		--{name="scope",label="Viseur",price=49000},
		--{name="advanced_scope",label="Viseur Avancé",price=98000},
		--{name="lazer_scope",label="Viseur Laser",price=6860},
		--{name="compansator",label="Compensateur",price=12250},
		--{name="barrel",label="Canon",price=17150},
		--{name="sprayweapon",label="Spray Armes",price=24500},
	}
}

Config.Zones = {

    GunShop = {
        legal = 0,
        Items = {},
        Pos   = {
            { x = 23.93,      y = -1105.65,   z = 28.79 },
            { x = 2566.72,    y = 292.0916,     z = 107.73 },
			{ x = -331.036,    y = 6086.006,     z = 30.454 },
        }
    },

    BlackWeashop = {
        legal = 1,
        Items = {},
        Pos   = {
           -- { x = 1689.24,   y = 3757.89,  z = 33.70 },
			--{ x = -956.367,   y = -224.804,  z = 18.748 },
			--{ x = -2602.674,   y = 1894.781,  z = 162.104 },
        }
    },

}

Config.Weapons = {
 {name="GunShop",item="WEAPON_KNIFE",price=120},
 --{name="GunShop",item="WEAPON_HAMMER",price=80}, 
 --{name="GunShop",item="GADGET_PARACHUTE",price=50},
 --{name="GunShop",item="WEAPON_CROWBAR",price=128}, 
 --{name="GunShop",item="WEAPON_DAGGER",price=992}, 
 --{name="GunShop",item="WEAPON_KNUCKLE",price=150},
 --{name="GunShop",item="WEAPON_HATCHET",price=720}, 
 --{name="GunShop",item="WEAPON_MACHETE",price=600},
 --{name="GunShop",item="WEAPON_SWITCHBLADE",price=500},
 --{name="GunShop",item="WEAPON_WRENCH",price=624}, 
 --{name="GunShop",item="WEAPON_FLASHLIGHT",price=150},
 --{name="GunShop",item="WEAPON_SMG",price=43650}, 
 --{name="GunShop",item="WEAPON_ASSAULTSMG",price=40000},
 --{name="GunShop",item="WEAPON_COMBATPDW",price=30000},
 --{name="GunShop",item="WEAPON_MINISMG",price=46075}, 
 --{name="GunShop",item="WEAPON_CARBINERIFLE",price=65000},
 --{name="GunShop",item="WEAPON_ADVANCEDRIFLE",price=70000},
 --{name="BlackWeashop",item="WEAPON_SPECIALCARBINE",price=65000},
 --{name="GunShop",item="WEAPON_PUMPSHOTGUN",price=40000},
 --{name="GunShop",item="WEAPON_SAWNOFFSHOTGUN",price=102949}, 
 --{name="GunShop",item="WEAPON_BULLPUPSHOTGUN",price=70000},
 --{name="GunShop",item="WEAPON_SNIPERRIFLE",price=441000}, 
 --{name="GunShop",item="WEAPON_GRENADE",price=36750}, 
 --{name="GunShop",item="WEAPON_SMOKEGRENADE",price=14700}, 
 -- {name="GunShop",item="WEAPON_FLAREGUN",price=12250}, 
 --{name="GunShop",item="WEAPON_FIREEXTINGUISHER",price=5000},
 --{name="GunShop",item="WEAPON_ASSAULTRIFLE",price=90000},
 --{name="BlackWeashop",item="WEAPON_BOTTLE",price=1500},
 --{name="BlackWeashop",item="WEAPON_BATTLEAXE",price=3700},
 --{name="BlackWeashop",item="WEAPON_POOLCUE",price=2100},
 --{name="BlackWeashop",item="WEAPON_MICROSMG",price=17000},
 --{name="BlackWeashop",item="WEAPON_MG",price=40000},
 --{name="BlackWeashop",item="WEAPON_COMBATMG",price=50000},
 --{name="BlackWeashop",item="WEAPON_GUSENBERG",price=80000},
 --{name="BlackWeashop",item="WEAPON_MACHINEPISTOL",price=7500},
 --{name="BlackWeashop",item="WEAPON_REVOLVER",price=50000},
 --{name="BlackWeashop",item="WEAPON_HEAVYPISTOL",price=10000},
 --{name="BlackWeashop",item="WEAPON_BULLPUPRIFLE",price=55000},
 --{name="BlackWeashop",item="WEAPON_COMPACTRIFLE",price=50000},
 --{name="BlackWeashop",item="WEAPON_ASSAULTSHOTGUN",price=25000},
 --{name="BlackWeashop",item="WEAPON_DBSHOTGUN",price=25000},
 --{name="BlackWeashop",item="WEAPON_HEAVYSHOTGUN",price=30000},
 --{name="BlackWeashop",item="WEAPON_HEAVYSNIPER",price=375000},
 --{name="BlackWeashop",item="WEAPON_MARKSMANRIFLE",price=25000},
 --{name="BlackWeashop",item="WEAPON_BZGAS",price=7000},
 --{name="BlackWeashop",item="WEAPON_MOLOTOV",price=8000},
 --{name="BlackWeashop",item="WEAPON_PIPEBOMB",price=10000},
 --{name="BlackWeashop",item="WEAPON_COMBATPDW",price=15000},
 {name="GunShop",item="WEAPON_PISTOL",price=5000},
 --{name="GunShop",item="WEAPON_COMBATPISTOL",price=5000},
 --{name="GunShop",item="WEAPON_APPISTOL",price=10000},
 --{name="GunShop",item="WEAPON_PISTOL50",price=10000},
 {name="GunShop",item="WEAPON_SNSPISTOL",price=5000},
 --{name="GunShop",item="WEAPON_MARKSMANPISTOL",price=17000},
 {name="GunShop",item="WEAPON_STUNGUN",price=9800},
 --{name="GunShop",item="WEAPON_BAT",price=300},
 --{name="GunShop",item="WEAPON_FLARE",price=5000}, 
 --{name="GunShop",item="WEAPON_SNSPISTOL_MK2",price=24500}, 
 --{name="BlackWeashop",item="WEAPON_SNSPISTOL_MK2",price=40000},
 --{name="BlackWeashop",item="WEAPON_REVOLVER_MK2",price=50000},
 --{name="GunShop",item="WEAPON_DOUBLEACTION",price=53900}, 
 --{name="BlackWeashop",item="WEAPON_DOUBLEACTION",price=27000},
 --{name="BlackWeashop",item="WEAPON_SPECIALCARBINE_MK2",price=150000},
 --{name="GunShop",item="WEAPON_BULLPUPRIFLE_MK2",price=10000}, 
 --{name="BlackWeashop",item="WEAPON_BULLPUPRIFLE_MK2",price=10000}, 
 --{name="GunShop",item="WEAPON_PUMPSHOTGUN_MK2",price=10000}, 
 --{name="BlackWeashop",item="WEAPON_PUMPSHOTGUN_MK2",price=57000}, 
 --{name="BlackWeashop",item="WEAPON_MARKSMANRIFLE_MK2",price=270000}, 
 --{name="BlackWeashop",item="WEAPON_ASSAULTRIFLE_MK2",price=75000}, 
 --{name="GunShop",item="WEAPON_CARBINERIFLE_MK2",price=10000}, 
 --{name="BlackWeashop",item="WEAPON_CARBINERIFLE_MK2",price=95000}, 
 --{name="BlackWeashop",item="WEAPON_COMBATMG_MK2",price=70000}, 
 --{name="BlackWeashop",item="WEAPON_HEAVYSNIPER_MK2",price=290000}, 
 --{name="GunShop",item="WEAPON_PISTOL_MK2",price=10000}, 
 --{name="BlackWeashop",item="WEAPON_PISTOL_MK2",price=40000},
 --{name="GunShop",item="WEAPON_SMG_MK2",price=85750}, 
 --{name="BlackWeashop",item="WEAPON_SMG_MK2",price=80000},
 --{name="GunShop",item="WEAPON_REVOLVER_MK2",price=147000}, 
 --{name="GunShop",item="WEAPON_HEAVYPISTOL",price=30000},
 --{name="GunShop",item="WEAPON_MUSKET",price=4000}
 }
