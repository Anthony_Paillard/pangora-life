ESX               = nil
local ItemsLabels = {}
local GunShopPrice = Config.EnableClip.GunShop.Price
local GunShopLabel = Config.EnableClip.GunShop.Label
local BlackWeashopPrice = Config.EnableClip.BlackWeashop.Price
local BlackWeashopLabel = Config.EnableClip.BlackWeashop.Label

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function LoadLicenses (source)
  TriggerEvent('esx_license:getLicenses', source, function (licenses)
    TriggerClientEvent('esx_weashop:loadLicenses', source, licenses)
  end)
end

if Config.EnableLicense == true then
  AddEventHandler('esx:playerLoaded', function (source)
    LoadLicenses(source)
  end)
end

RegisterServerEvent('esx_weashop:buyLicense')
AddEventHandler('esx_weashop:buyLicense', function ()
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)

  if xPlayer.get('money') >= Config.LicensePrice then
    xPlayer.removeMoney(Config.LicensePrice)

    TriggerEvent('esx_license:addLicense', _source, 'weapon', function ()
      LoadLicenses(_source)
    end)
  else
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
  end
end)


ESX.RegisterServerCallback('esx_weashop:requestDBItems', function(source, cb)

  MySQL.Async.fetchAll(
    'SELECT * FROM weashops',
    {},
    function(result)

      local shopItems  = {}

      for i=1, #result, 1 do

        if shopItems[result[i].name] == nil then
          shopItems[result[i].name] = {}
        end

        table.insert(shopItems[result[i].name], {
          name  = result[i].item,
          price = result[i].price,
          label = ESX.GetWeaponLabel(result[i].item)
        })
      end
	  
	  if Config.EnableClipGunShop == true then
		table.insert(shopItems["GunShop"], {
          name  = "clip",
          price = GunShopPrice,--Config.EnableClip.GunShop.Price,
          label = GunShopLabel--Config.EnableClip.GunShop.label
        })
		table.insert(shopItems["GunShop"], {
          name  = "armor",
          price = 5000,--Config.EnableClip.BlackWeashop.Price,
          label = '️Gilet par balle'--Config.EnableClip.BlackWeashop.label
        })
		table.insert(shopItems["GunShop"], {
          name  = "flashlight",
          price = 10000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Lampe Torche'--Config.EnableClip.BlackWeashop.label
        })
		table.insert(shopItems["GunShop"], {
          name  = "nightvision_scope",
          price = 15000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Lunette à Vision Nocturne'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["GunShop"], {
          name  = "grip",
          price = 10000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Poignée'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["GunShop"], {
          name  = "lowrider",
          price = 25000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Skin Lowrider'--Config.EnableClip.BlackWeashop.label
        })		
		table.insert(shopItems["GunShop"], {
          name  = "yusuf",
          price = 25000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Skin de Luxe'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["GunShop"], {
          name  = "scope",
          price = 35000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Viseur'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["GunShop"], {
          name  = "advanced_scope",
          price = 55000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Viseur Avancé'--Config.EnableClip.BlackWeashop.label
        })			
		table.insert(shopItems["GunShop"], {
          name  = "lazer_scope",
          price = 45000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Viseur Laser'--Config.EnableClip.BlackWeashop.label
        })
		table.insert(shopItems["GunShop"], {
          name  = "compansator",
          price = 25000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Compensateur'--Config.EnableClip.BlackWeashop.label
        })		
		table.insert(shopItems["GunShop"], {
          name  = "barrel",
          price = 35000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Canon'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["GunShop"], {
          name  = "sprayweapon",
          price = 50000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Spray Armes'--Config.EnableClip.BlackWeashop.label
        })			
		end
		
		if Config.EnableClipGunShop == true then
		table.insert(shopItems["BlackWeashop"], {
          name  = "clip",
          price = GunShopPrice,--Config.EnableClip.GunShop.Price,
          label = GunShopLabel--Config.EnableClip.GunShop.label
		})	
       	table.insert(shopItems["BlackWeashop"], {
          name  = "armor",
          price = 3500,--Config.EnableClip.BlackWeashop.Price,
          label = '️Gilet par balle'--Config.EnableClip.BlackWeashop.label
        })
		table.insert(shopItems["BlackWeashop"], {
          name  = "flashlight",
          price = 5000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Lampe Torche'--Config.EnableClip.BlackWeashop.label
        })
		table.insert(shopItems["BlackWeashop"], {
          name  = "nightvision_scope",
          price = 7500,--Config.EnableClip.BlackWeashop.Price,
          label = 'Lunette à Vision Nocturne'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["BlackWeashop"], {
          name  = "grip",
          price = 5000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Poignée'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["BlackWeashop"], {
          name  = "lowrider",
          price = 12500,--Config.EnableClip.BlackWeashop.Price,
          label = 'Skin Lowrider'--Config.EnableClip.BlackWeashop.label
        })		
		table.insert(shopItems["BlackWeashop"], {
          name  = "yusuf",
          price = 12500,--Config.EnableClip.BlackWeashop.Price,
          label = 'Skin de Luxe'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["BlackWeashop"], {
          name  = "scope",
          price = 20000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Viseur'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["BlackWeashop"], {
          name  = "advanced_scope",
          price = 30000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Viseur Avancé'--Config.EnableClip.BlackWeashop.label
        })			
		table.insert(shopItems["BlackWeashop"], {
          name  = "lazer_scope",
          price = 35000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Viseur Laser'--Config.EnableClip.BlackWeashop.label
        })
		table.insert(shopItems["BlackWeashop"], {
          name  = "compansator",
          price = 15000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Compensateur'--Config.EnableClip.BlackWeashop.label
        })		
		table.insert(shopItems["BlackWeashop"], {
          name  = "barrel",
          price = 25000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Canon'--Config.EnableClip.BlackWeashop.label
        })	
		table.insert(shopItems["BlackWeashop"], {
          name  = "sprayweapon",
          price = 30000,--Config.EnableClip.BlackWeashop.Price,
          label = 'Spray Armes'--Config.EnableClip.BlackWeashop.label
        })			
		end
      cb(shopItems)

    end
  )

end)


RegisterServerEvent('esx_weashop:buyItem')
AddEventHandler('esx_weashop:buyItem', function(itemName, price, zone)

  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(source)
  local account = xPlayer.getAccount('black_money')

  if zone=="BlackWeashop" then
    if account.money >= price then
		if itemName == "clip" or itemName == "armor" or itemName == 'flashlight' or itemName == "nightvision_scope" or itemName == 'grip' or itemName == 'lowrider' or itemName == 'yusuf'or itemName == 'scope'or itemName == 'advanced_scope'or itemName == 'lazer_scope'or itemName == 'compansator' or itemName == 'barrel' or itemName == 'sprayweapon' then
			xPlayer.addInventoryItem(itemName, 1)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. itemName)
			TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. itemName,'CHAR_DETONATEBOMB','Marché Noir')
		else
			xPlayer.addWeapon(itemName, 42)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName))
			TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName),'CHAR_DETONATEBOMB','Marché Noir')
		end
		xPlayer.removeAccountMoney('black_money', price)
	else
		--TriggerClientEvent('esx:showNotification', _source, _U('not_enough_black'))
		TriggerClientEvent('esx_extended:showNotification', _source, _U('not_enough_black'),'CHAR_BLOCKED','Attention')
	end

  else if xPlayer.get('money') >= price then
		if itemName == "clip" or itemName == "armor" or itemName == 'flashlight' or itemName == "nightvision_scope" or itemName == 'grip' or itemName == 'lowrider' or itemName == 'yusuf'or itemName == 'scope'or itemName == 'advanced_scope'or itemName == 'lazer_scope'or itemName == 'compansator' or itemName == 'barrel' or itemName == 'sprayweapon' then
			xPlayer.addInventoryItem(itemName, 1)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. itemName)
			 TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. itemName,'CHAR_AMMUNATION','Ammunation')
		else			
			xPlayer.addWeapon(itemName, 42)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName))
			 TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName),'CHAR_AMMUNATION','Ammunation')
		end
		xPlayer.removeMoney(price)
  else
   -- TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
	TriggerClientEvent('esx_extended:showNotification', _source, _U('not_enough'),'CHAR_BLOCKED','Banque')
  end
  end

end)

-- thx to Pandorina for script
RegisterServerEvent('esx_weashop:remove')
AddEventHandler('esx_weashop:remove', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('clip', 1)
end)

ESX.RegisterUsableItem('clip', function(source)
	TriggerClientEvent('esx_weashop:clipcli', source)
end)
