
ESX               = nil
local ItemsLabels = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

-- function LoadLicenses (source)
  -- TriggerEvent('esx_license:getLicenses', source, function (licenses)
    -- TriggerClientEvent('esx_weashop:loadLicenses', source, licenses)
  -- end)
-- end

-- if Config.EnableLicense == true then
  -- AddEventHandler('esx:playerLoaded', function (source)
    -- LoadLicenses(source)
  -- end)
-- end

RegisterServerEvent('esx_weashop:buyLicense')
AddEventHandler('esx_weashop:buyLicense', function ()
  local _source = source
  local xPlayer = ESX.GetPlayerFromId(source)

  if xPlayer.get('money') >= Config.LicensePrice then
    xPlayer.removeMoney(Config.LicensePrice)
	xPlayer.addInventoryItem("weapon", 1)
    -- TriggerEvent('esx_license:addLicense', _source, 'weapon', function ()
      -- LoadLicenses(_source)
    -- end)
  else
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
  end
end)



ESX.RegisterServerCallback('esx_weashop:requestDBItems', function(source, cb)
	local shopItems  = {}
	for i=1, #Config.Weapons, 1 do
		if shopItems[Config.Weapons[i].name] == nil then
          shopItems[Config.Weapons[i].name] = {}
        end

        table.insert(shopItems[Config.Weapons[i].name], {
          name  = Config.Weapons[i].item,
          price = Config.Weapons[i].price,
          label = ESX.GetWeaponLabel(Config.Weapons[i].item)
        })
    end
	
	if Config.EnableClipGunShop then
		for i=1, #Config.weaponaddons.GunShop, 1 do
			table.insert(shopItems.GunShop, {
			  name  = Config.weaponaddons.GunShop[i].name,
			  price = Config.weaponaddons.GunShop[i].price,
			  label = Config.weaponaddons.GunShop[i].label
			})
		end
	end
	
	if Config.EnableClipBlackWeashop then
		for i=1, #Config.weaponaddons.BlackWeashop, 1 do
			table.insert(shopItems.BlackWeashop, {
			  name  = Config.weaponaddons.BlackWeashop[i].name,
			  price = Config.weaponaddons.BlackWeashop[i].price,
			  label = Config.weaponaddons.BlackWeashop[i].label
			})
		end
	end
	
	cb(shopItems)
end)


RegisterServerEvent('esx_weashop:buyweapon')
AddEventHandler('esx_weashop:buyweapon', function(itemName, price, zone)

  local _source = source
  local xPlayer  = ESX.GetPlayerFromId(source)
  local account = xPlayer.getAccount('black_money')

  if zone=="BlackWeashop" then
   -- if account.money >= price then
   if xPlayer.get('money') >= price then
		if itemName == "clip" or itemName == "plier"or itemName == "serflex"or itemName == "cagoule" or itemName == "armor" or itemName == 'flashlight' or itemName == "nightvision_scope" or itemName == 'grip' or itemName == 'lowrider' or itemName == 'yusuf'or itemName == 'scope'or itemName == 'advanced_scope'or itemName == 'lazer_scope'or itemName == 'compansator' or itemName == 'barrel' or itemName == 'sprayweapon' then
			xPlayer.addInventoryItem(itemName, 1)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. itemName)
			TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. itemName,'CHAR_DETONATEBOMB','Marché Noir')
		else
			xPlayer.addWeapon(itemName, 42)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName))
			TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName),'CHAR_DETONATEBOMB','Marché Noir')
		end
		xPlayer.removeMoney(price)
	else
		--TriggerClientEvent('esx:showNotification', _source, _U('not_enough_black'))
		TriggerClientEvent('esx_extended:showNotification', _source, _U('not_enough'),'CHAR_BLOCKED','Attention')
	end

  else if xPlayer.get('money') >= price then
		if itemName == "clip" or itemName == "plier"or itemName == "serflex"or itemName == "cagoule" or itemName == "armor" or itemName == 'flashlight' or itemName == "nightvision_scope" or itemName == 'grip' or itemName == 'lowrider' or itemName == 'yusuf'or itemName == 'scope'or itemName == 'advanced_scope'or itemName == 'lazer_scope'or itemName == 'compansator' or itemName == 'barrel' or itemName == 'sprayweapon' then
			xPlayer.addInventoryItem(itemName, 1)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. itemName)
			 TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. itemName,'CHAR_AMMUNATION','Ammunation')
		else			
			xPlayer.addWeapon(itemName, 42)
			--TriggerClientEvent('esx:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName))
			 TriggerClientEvent('esx_extended:showNotification', _source, _U('buy') .. ESX.GetWeaponLabel(itemName),'CHAR_AMMUNATION','Ammunation')
		end
		xPlayer.removeMoney(price)
  else
   -- TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
	TriggerClientEvent('esx_extended:showNotification', _source, _U('not_enough'),'CHAR_BLOCKED','Banque')
  end
  end

end)

-- thx to Pandorina for script
RegisterServerEvent('esx_weashop:remove')
AddEventHandler('esx_weashop:remove', function()
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem('clip', 1)
end)

ESX.RegisterUsableItem('clip', function(source)
	TriggerClientEvent('esx_weashop:clipcli', source)
end)


function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end