RegisterNetEvent("gcPhone:tchat_receive")
AddEventHandler("gcPhone:tchat_receive", function(message)
  SendNUIMessage({event = 'tchat_receive', message = message})
end)


function hasPuce (cb)
  if (ESX == nil) then return cb(0) end
  ESX.TriggerServerCallback('gcphone:getItemAmount', function(qtty)
    cb(qtty > 0)
  end, 'black_chip')
end

function ShowNoPuceWarning ()
  if (ESX == nil) then return end
  ESX.ShowNotification("l'application ne fonctionne pas...")
end

RegisterNetEvent("gcPhone:tchat_channel")
AddEventHandler("gcPhone:tchat_channel", function(channel, messages)
  hasPuce(function (hasPuce)
    if hasPuce == true then
      SendNUIMessage({event = 'tchat_channel', messages = messages})
    else
      ShowNoPuceWarning()
    end
  end)
end)

RegisterNUICallback('tchat_addMessage', function(data, cb)
  hasPuce(function (hasPuce)
    if hasPuce == true then
      TriggerServerEvent('gcPhone:tchat_addMessage', data.channel, data.message)
    else
      ShowNoPuceWarning()
    end
  end)
end)

RegisterNUICallback('tchat_getChannel', function(data, cb)
  hasPuce(function (hasPuce)
    if hasPuce == true then
      TriggerServerEvent('gcPhone:tchat_channel', data.channel)
    else
      ShowNoPuceWarning()
    end
  end)
end)
