Locales['en'] = {
    ['press_to_open'] = 'Appuyer sur ~INPUT_CONTEXT~ Pour accèder a la Boutique.',
	['press_to_rob'] = 'Appuyer sur ~INPUT_CONTEXT~ pour ~r~Braquer ~w~Le Magasin.',
	['press_to_open_center'] = 'Appuyer sur ~INPUT_CONTEXT~ Pour ouvrir le ShopCenter.',
	['name_shop'] = 'Comment va s\'appeler votre Boutique?',
	['buy_shop'] = 'Acheter Magasin ',
	['robbery_cancel'] = '~r~Le Braquage est Annulé (tu t\'es éloigné)',
	['set_price'] = 'A quel Prix vous vendez ?',
	['how_much'] = 'Combien?',
}
