Config = {}
Config.Locale = 'fr'
Config.NumberOfCopsRequired = 4

Banks = {
	["fleeca"] = {
		position = { ['x'] = 147.04908752441, ['y'] = -1044.9448242188, ['z'] = 29.36802482605 },
		reward = math.random(700000,1200000),
		nameofbank = "Fleeca Bank",
		lastrobbed = 0
	},
	["fleeca2"] = {
		position = { ['x'] = -2957.6674804688, ['y'] = 481.45776367188, ['z'] = 15.697026252747 },
		reward = math.random(600000,1200000),
		nameofbank = "Fleeca Bank (Highway)",
		lastrobbed = 0
	},
	["blainecounty"] = {
		position = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498 },
		reward = math.random(700000,1200000),
		nameofbank = "Blaine County Savings",
		lastrobbed = 0
	},
	["vangelico"] = {
		position = { ['x'] = -616.56463623047, ['y'] = -236.38865661621, ['z'] = 37.057018250029 },
		reward = math.random(850000,1300000),
		nameofbank = "vangelico",
		lastrobbed = 0
	},	
	 ["Brinks Coffre"] = {
	  position = { ['x'] = -3.6958174705505, ['y'] = -687.935546875, ['z'] = 15.130598068237 },
	  reward = math.random(850000,1400000),
	  nameofbank = "Brinks Coffre",
	  lastrobbed = 0
	},
	
	["PrincipalBank"] = {
		position = { ['x'] = 255.001098632813, ['y'] = 225.855895996094, ['z'] = 101.005694274902 },
		reward = math.random(900000,1500000),
		nameofbank = "Principal bank",
		lastrobbed = 0
	}
}