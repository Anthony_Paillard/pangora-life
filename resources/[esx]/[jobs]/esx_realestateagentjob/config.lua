Config              = {}
Config.DrawDistance = 100.0
Config.MarkerColor  = { r = 120, g = 120, b = 240 }
Config.Locale       = 'fr'

Config.Zones = {
	OfficeEnter = {
		Pos   = { x = -903.12, y = -231.34, z = 39.489 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = 1
	},

	OfficeExit = {
		Pos   = { x = -141.226, y = -614.166, z = 167.820 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = 1
	},

	OfficeInside = {
		Pos   = { x = -140.969, y = -616.785, z = 167.820 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = -1
	},

	OfficeOutside = {
		Pos   = { x = -202.238, y = -578.193, z = 39.500 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = -1
	},

	OfficeActions = {
		Pos   = { x = -896.92, y = -236.43, z = 38.87 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = -1
	}
}
