Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 120, g = 120, b = 240 }
Config.EnablePlayerManagement     = true -- enables the actual car dealer job. You'll need esx_addonaccount, esx_billing and esx_society
Config.EnableOwnedVehicles        = true
Config.EnableSocietyOwnedVehicles = true -- use with EnablePlayerManagement disabled, or else it wont have any effects
Config.ResellPercentage           = 50

Config.Locale                     = 'fr'

Config.LicenseEnable = true -- require people to own drivers license when buying vehicles? Only applies if EnablePlayerManagement is disabled. Requires esx_license

-- looks like this: 'LLL NNN'
-- The maximum plate length is 8 chars (including spaces & symbols), don't go past it!
Config.PlateLetters  = 3
Config.PlateNumbers  = 3
Config.PlateUseSpace = true

Config.Zones = {

	ShopEntering = {
		Pos   = { x = -204.71, y = -1342.8, z = 29.94 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = 1
	},

	ShopInside = {
		Pos     = { x = -211.68, y = -1324.66, z = 29.9 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Heading = 89.5,
		Type    = -1
	},

	ShopOutside = {
		Pos     = { x = -211.68, y = -1324.66, z = 29.9 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Heading = 355.5,
		Type    = -1
	},

	BossActions = {
		Pos   = { x = -204.58, y = -1342.36, z = 33.9  },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = 1
	},

	--[[ GiveBackVehicle = {
		Pos   = { x = -496.15, y = -600.02, z = 36.31 },
		Size  = { x = 3.0, y = 3.0, z = 1.0 },
		Type  = (Config.EnablePlayerManagement and 1 or -1)
	}, ]]--

	ResellVehicle = {
		Pos   = { x = -44.630, y = -1080.738, z = 25.683 },
		Size  = { x = 3.0, y = 3.0, z = 1.0 },
		Type  = 1
	}

}
