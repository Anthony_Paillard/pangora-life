Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.MafiaStations = {

  Mafia = {
  
    Blip = {
      Pos     = {x = 818.26063, y = -2155.46411, z = 28.61899},
      Sprite  = 120,
      Display = 4,
      Scale   = 1.0,
      Color   = 1,
    },
    
    AuthorizedWeapons = {
      { name = 'WEAPON_PISTOL_MK2',         price = 0 },
      { name = 'WEAPON_SMG_MK2',            price = 0 },
      { name = 'WEAPON_COMBATMG_MK2',       price = 0 },
      { name = 'WEAPON_ASSAULTRIFLE_MK2',   price = 0 },
      { name = 'WEAPON_CARBINERIFLE_MK2',   price = 0 },
      { name = 'WEAPON_HEAVYSNIPER_MK2',    price = 0 },
      { name = 'WEAPON_SNSPISTOL_MK2',      price = 0 },
      { name = 'WEAPON_REVOLVER_MK2',       price = 0 },
      { name = 'WEAPON_SPECIALCARBINE_MK2', price = 0 },
      { name = 'WEAPON_BULLPUPRIFLE_MK2',   price = 0 },
      { name = 'WEAPON_HEAVYSNIPER',        price = 0 },
      { name = 'WEAPON_PUMPSHOTGUN_MK2',    price = 0 },
      { name = 'WEAPON_MARKSMANRIFLE_MK2',  price = 0 },
      { name = 'WEAPON_STICKYBOMB',         price = 0 },
      { name = 'WEAPON_SMOKEGRENADE',       price = 0 },
	  { name = 'WEAPON_BZGAS',              price = 0 },
	  { name = 'GADGET_PARACHUTE',          price = 0 },
	  { name = 'WEAPON_MUSKET',             price = 0 },
    },


	  AuthorizedVehicles = {
		  { name = 'akuma',    label = 'Moto' },
		  { name = 'dukes2',      label = 'Duke o death' },
		  { name = 'halftrack',  label = 'Camion' },
	  },

    Cloakrooms = {
	  {x = -1355.40, y = -749.42, z = 33.22},
    },

    Armories = {
      {x = -1354.69, y = -756.73, z = 21.30},
    },

    Vehicles = {
      {
        Spawner    = {x = -1345.55, y = -758.60, z = 21.45},
        SpawnPoint = {x = -1342.36, y = -754.12, z = 21.45},
        Heading    = 37.31,
      }
    },

    Helicopters = {
      {
        Spawner    = {x = -1354.12, y = -749.71, z = 42.79},
        SpawnPoint = {x = -1364.72, y = -728.84, z = 42.62},
        Heading    = 133.89,
      }
    },

    VehicleDeleters = {
      {x = -1357.99, y = -758.28, z = 21.30},
      
    },

    BossActions = {
      {x = -1348.33, y = -758.04, z = 37.30},
    },

  },

}


