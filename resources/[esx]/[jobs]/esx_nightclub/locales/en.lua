Locales['en'] = {
    -- Cloakroom
    ['cloakroom']                = 'Vestiaires',
    ['citizen_wear']             = 'Tenue de civil',
    ['barman_outfit']            = 'Tenue de barman',
    ['dancer_outfit_1']          = 'Tenue de danseuse 1',
    ['dancer_outfit_2']          = 'Tenue de danseuse 2',
    ['dancer_outfit_3']          = 'Tenue de danseuse 3',
    ['dancer_outfit_4']          = 'Tenue de danseuse 4',
    ['dancer_outfit_5']          = 'Tenue de danseuse 5',
    ['dancer_outfit_6']          = 'Tenue de danseuse 6',
    ['dancer_outfit_7']          = 'Tenue de danseuse 7',
    ['no_outfit']                = 'Cette tenue n\' est pas pour vous...',
    ['open_cloackroom']          = 'apuyez sur ~INPUT_CONTEXT~ pour vous changer',
  
    -- Vault  
    ['get_weapon']               = 'Prendre armes',
    ['put_weapon']               = 'Déposer armes',
    ['get_weapon_menu']          = 'Coffre - Prendre armes',
    ['put_weapon_menu']          = 'Coffre - Déposer armes',
    ['get_object']               = 'Prendre objets',
    ['put_object']               = 'Déposer objets',
    ['vault']                    = 'Coffre',
    ['open_vault']               = 'Appuyez sur  ~INPUT_CONTEXT~ pour acceder au coffre',
  
    -- Fridge  
    ['get_object']               = 'Prendre objets',
    ['put_object']               = 'Déposer objets',
    ['fridge']                   = 'Refrigérateur',
    ['open_fridge']              = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le réfrigérateur',
    ['nightclub_fridge_stock']   = 'réfrigérateur du Club',
    ['fridge_inventory']         = 'Contenus du réfrigérateur',
  
    -- Shops  
    ['shop']                     = 'Magasin',
    ['shop_menu']                = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le magasin.',
    ['bought']                   = 'vous avez acheté 1x ~b~',
    ['not_enough_money']         = 'Vous n\'avez pas assez d\'argents.',
    ['max_item']                 = 'Vous ne pouvez pas en porter d\'avantage.',
  
    -- Vehicles  
    ['vehicle_menu']             = 'Véhicules',
    ['vehicle_out']              = 'Le véhicules est déja sorti',
    ['vehicle_spawner']          = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir le véhicule',
    ['store_vehicle']            = 'Appuyez sur ~INPUT_CONTEXT~ pour rentrer le véhicule',
    ['service_max']              = 'Service maximum: ',
    ['spawn_point_busy']         = 'Le véhicule est a coté du point de sortie',
  
    -- Boss Menu  
    ['take_company_money']       = 'Retirer argent de société',
    ['deposit_money']            = 'Déposer argent de société',
    ['amount_of_withdrawal']     = 'Montant',
    ['invalid_amount']           = 'Montant invalide',
    ['amount_of_deposit']        = 'Déposer le montant',
    ['open_bossmenu']            = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu',
    ['invalid_quantity']         = 'Quantitée invalide',
    ['you_removed']              = 'vous avez supprimé x',
    ['you_added']                = 'Vous avez ajouté x',
    ['quantity']                 = 'Quantitée',
    ['inventory']                = 'Inventaire',
    ['nightclub_stock']          = 'Stocks',
  
    -- Billing Menu  
    ['billing']                  = 'Facture',
    ['no_players_nearby']        = 'Aucuns joueurs a proximité',
    ['billing_amount']           = 'Montant',
    ['amount_invalid']           = 'Montant invalide',
  
    -- Crafting Menu  
    ['crafting']                 = 'Mixer',
    ['martini']                  = 'Martini Blanc',
    ['icetea']                   = 'Ice Tea',
    ['drpepper']                 = 'Dr. Pepper',
    ['saucisson']                = 'Saucisson',
    ['grapperaisin']             = 'Grappe de raisin',
    ['energy']                   = 'Energy Drink',
    ['jager']                    = 'Jägermeister',
    ['limonade']                 = 'Limonade',
    ['vodka']                    = 'Vodka',
    ['ice']                      = 'Glaçons',
    ['soda']                     = 'Soda',
    ['whisky']                   = 'Whisky',
    ['rhum']                     = 'Rhum',
    ['tequila']                  = 'Tequila',
    ['menthe']                   = 'Menthe',
    ['jusfruit']                 = 'Jus de fruit',
    ['jagerbomb']                = 'Jägerbomb',
    ['bolcacahuetes']            = 'Cacahuetes',
    ['bolnoixcajou']             = 'Noix de cajou',
    ['bolpistache']              = 'Pistaches',
    ['bolchips']                 = 'Chips',
    ['jagerbomb']                = 'Jägerbomb',
    ['golem']                    = 'Golem',
    ['whiskycoca']               = 'Whisky-coca',
    ['vodkaenergy']              = 'Vodka-energy',
    ['vodkafruit']               = 'Vodka-jus de fruit',
    ['rhumfruit']                = 'Rum-jus de fruit',
    ['teqpaf']                   = 'Tequila\'paf',
    ['rhumcoca']                 = 'Rhum-coca',
    ['mojito']                   = 'Mojito',
    ['mixapero']                 = 'Aperitif Mix',
    ['metreshooter']             = 'metre de shooter',
    ['jagercerbere']             = 'Jäger Cerberus',
    ['assembling_cocktail']      = 'Mix en cours..!',
    ['craft_miss']               = 'Vous avez raté votre mixe ...',
    ['not_enough']               = 'pas assez de ~r~ ',
    ['craft']                    = 'mixe terminé',
  
    -- Misc  
    ['map_blip']                 = 'Bahamas Mamas',
    ['nightclub']                = 'Bahamas Mamas',
  
    -- Phone  
    ['nightclub_phone']            = 'Bahamas Mamas',
    ['nightclub_customer']         = 'Citizen',
  
    -- Teleporters
    ['e_to_enter_1']             = 'Appuyez sur ~INPUT_PICKUP~ pour aller derrière le bar',
    ['e_to_exit_1']              = 'Appuyez sur ~INPUT_PICKUP~ pour aller devant le bar',
    ['e_to_enter_2']             = 'Appuyez sur ~INPUT_PICKUP~ pour aller a l\'étage.',
    ['e_to_exit_2']              = 'Appuyez sur ~INPUT_PICKUP~ pour aller à l\ffice.',
    
}
