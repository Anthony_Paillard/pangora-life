INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_epicerie', 'Epicerie', 1)
;

INSERT INTO `addon_inventory` (`name`, `label`, `shared`) VALUES
('epicerie', 'Epicerie', 1),
('society_epicerie_reserve', 'Epicerie (réserve)', 1);

INSERT INTO `datastore` (name, label, shared) VALUES 
    ('society_epicerie', 'Epicerie', 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
  ('epicerie', 'Walker Logistic', 1)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('epicerie', 0, 'stagiere', 'Stagire', 450, '{}', '{}'),
  ('epicerie', 1, 'epicier', 'Vendeur', 650, '{}', '{}'),
  ('epicerie', 2, 'co-gerant', 'Co-gérant', 800, '{}', '{}'),
  ('epicerie', 3, 'boss', 'Patron', 1000, '{}', '{}')
;

INSERT INTO `items` (`name`, `label`) VALUES  
    ('metal', 'Metal'),
    ('ressort', 'Ressort'),
    ('peinture', 'Peinture'), 
    ('circuitelectronique', 'Circuit Electronique'),
    ('colle', 'Colle'),
;
