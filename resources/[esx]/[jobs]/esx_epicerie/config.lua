Config                            = {}
Config.DrawDistance               = 100.0

Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.EnableVaultManagement      = true
Config.EnableHelicopters          = false
Config.EnableMoneyWash            = false
Config.MaxInService               = -1
Config.PriceResell                = {eau = 30, pain = 40}
Config.Locale                     = 'fr'

Config.MissCraft                  = 10 -- %


Config.AuthorizedVehicles = {
    { name = 'brickade',     label = 'Véhicule de transport' },
	{ name = 'pounder2',     label = 'Camion' },
	{ name = 'baller4',  	 label = 'Véhicule de vente' },
	{ name = 'trailers4',  	 label = 'Remorque (Ayez un Camion avant !!!)' },
	
}

Config.Blips = {
    
    Blip = {
      Pos     = { x = 152.528, y = -3210.697, z = 5.990 },
      Sprite  = 607,
      Display = 4,
      Scale   = 0.9,
      Colour  = 0,
    },

}

Config.Zones = {

    Cloakrooms = {
        Pos   = { x = 121.14, y = -3215.00, z = 5.02 },
        Size  = { x = 1.5, y = 1.5, z = 1.0 },
        Color = { r = 0, g = 0, b = 255 },
        Type  = 27,
    },

    Vaults = {
        Pos   = { x = 145.64, y = -3219.34, z = 4.89 },
        Size  = { x = 1.3, y = 1.3, z = 1.0 },
        Color = { r = 0, g = 0, b = 255 },
        Type  = 23,
    },

    Vehicles = {
        Pos          = { x = 156.67, y = -3208.39, z = 5.94 },
        SpawnPoint   = { x = 161.44, y = -3204.14, z = 4.96 },
        Size         = { x = 1.8, y = 1.8, z = 1.0 },
        Color        = { r = 255, g = 255, b = 0 },
        Type         = 23,
        Heading      = 271.48,
    },

    VehicleDeleters = {
        Pos   = { x = 139.85, y = -3204.22, z = 4.85 },
        Size  = { x = 3.0, y = 3.0, z = 0.2 },
        Color = { r = 255, g = 255, b = 0 },
        Type  = 1,
    },
	
	EauFarm = {
        Pos   = { x = 2457.95, y = -396.87, z = 92.06 },
        Size  = { x = 3.0001, y = 3.0001, z = 1.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },
	
	EauResell = {
        Pos   = { x = -2305.76, y = 3427.84, z = 30.05 },
        Size  = { x = 2.0001, y = 2.0001, z = 2.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },
	
	PainFarm = {
        Pos   = { x = 1865.52, y = 2707.79, z = 44.93 },
        Size  = { x = 2.0001, y = 2.0001, z = 2.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },
	
	PainResell = {
        Pos   = { x = 0, y = 0, z = 0 },
        Size  = { x = 2.0001, y = 2.0001, z = 2.0001 },
        Color = { r = 0, g = 155, b = 255, a = 160},
        Type  = 27,
    },

--[[
    Helicopters = {
        Pos          = { x = 137.177, y = -1278.757, z = 28.371 },
        SpawnPoint   = { x = 138.436, y = -1263.095, z = 28.626 },
        Size         = { x = 1.8, y = 1.8, z = 1.0 },
        Color        = { r = 255, g = 255, b = 0 },
        Type         = 23,
        Heading      = 207.43,
    },

    HelicopterDeleters = {
        Pos   = { x = 133.203, y = -1265.573, z = 28.396 },
        Size  = { x = 3.0, y = 3.0, z = 0.2 },
        Color = { r = 255, g = 255, b = 0 },
        Type  = 1,
    },
]]--

    BossActions = {
        Pos   = { x = 152.528, y = -3210.697, z = 4.990 },
        Size  = { x = 1.5, y = 1.5, z = 1.0 },
        Color = { r = 0, g = 100, b = 0 },
        Type  = 1,
    },

-----------------------
-------- SHOPS --------

    Flacons = {
        Pos   = { x = 2571.97, y = 293.11, z = 107.73 },
        Size  = { x = 1.6, y = 1.6, z = 1.0 },
        Color = { r = 238, g = 0, b = 0 },
        Type  = 23,
        Items = {
            { name = 'flashlight',      		label = _U('flashlight'),   price = 500 },
            { name = 'grip',     				label = _U('grip'),   price = 500 },
            { name = 'nightvision_scope',       label = _U('nightvision_scope'),    price = 5000 },
            --{ name = 'armor',    				label = _U('armor'), price = 500 },
            { name = 'medikit',    				label = _U('medikit'), price = 15 },
			{ name = 'bandage',    				label = _U('bandage'), price = 15 }
        },
    },

    NoAlcool = {
        Pos   = { x = -493.73, y = -2851.26, z = 8.29 },
        Size  = { x = 1.6, y = 1.6, z = 1.0 },
        Color = { r = 238, g = 110, b = 0 },
        Type  = 23,
        Items = {
            { name = 'water',                    label = _U('water'),     price = 10 },
            --{ name = 'pills',                    label = _U('Pillule'),     price = 0 },
            --{ name = 'Expresso',                 label = _U('Expresso'),     price = 10 },
            --{ name = 'vegetables',               label = _U('Salade'),     price = 10 },
            { name = 'binoculars',               label = _U('binoculars'),     price = 50 },
            { name = 'croquettes',               label = _U('Croquettes'),     price = 10 },
            { name = 'lighter',                  label = _U('Briquet'),     price = 10 },
            { name = 'gps',                      label = _U('gps'),     price = 50 },
            --{ name = 'fishlicence',              label = _U('Permis de Pêche'),     price = 0 },
            --{ name = 'brolly',                   label = _U('Parapluie'),     price = 15 },
            --{ name = 'KitPic',                   label = _U('Kit de Picnic'),     price = 10 },
            --{ name = 'ball',                     label = _U('Ballon'),     price = 10 },
            { name = 'mask_swim',                label = _U('Masque de Plongée'),     price = 50 },
            --{ name = 'condom',                   label = _U('Preservatif'),     price = 10 },
            --{ name = 'magazine',                 label = _U('Magazine Playboy'),     price = 10 },
            --{ name = 'notepad',                  label = _U('Bloc Notes'),     price = 0 },
            --{ name = 'rose',                     label = _U('rose'),     price = 10 },
            --{ name = 'tatgun',                   label = _U('Dermographe'),     price = 0 },
            --{ name = 'item',                     label = _U('Tente'),     price = 10 },
            --{ name = 'item4',                    label = _U('Barbecue'),     price = 10 },
            { name = 'phone',                    label = _U('phone'),     price = 100 },
            --{ name = 'fishingrod',               label = _U('Canne a pêche'),     price = 0 },
            --{ name = 'bait',                     label = _U('Appat a poisson'),     price = 0 },
            { name = 'bread',                 label = _U('bread'),     price = 10 },
            --{ name = 'TheMenthe',                label = _U('The a la Menthe'),     price = 0 },
            --{ name = 'phone',                    label = _U('Téléphone'),     price = 0 },
            --{ name = 'donuts',                   label = _U('Donuts'),     price = 0 },
            --{ name = 'coffee',                   label = _U('Café'),     price = 0 },
            --{ name = 'luckystrike',              label = _U('Lucky Strike'),     price = 0 },
           --{ name = 'SoupedeTomate',            label = _U('Soupe de Tomate'),     price = 0 },

        },
    },

    Apero = {
        Pos   = { x = 1165.68, y = -1347.43, z = 34.74 },
        Size  = { x = 1.6, y = 1.6, z = 1.0 },
        Color = { r = 142, g = 125, b = 76 },
        Type  = 23,
        Items = {
			{ name = 'colle',     			    label = _U('colle'),     price = 2 },
			{ name = 'metal',     			   	label = _U('metal'), price = 5 },
            { name = 'ressort',  				label = _U('ressort'),    price = 15 },
            { name = 'circuitelectronique',     label = _U('circuitelectronique'), price = 20 },
			{ name = 'peinture',    			label = _U('peinture'), price = 20 },
			{ name = 'steel',    			label = _U('steel'), price = 5 },
            --{ name = 'tracer_clip',    			 label = _U('tracer_clip'),     price = 30 },
            --{ name = 'extended_magazine',      label = _U('extended_magazine'),      price = 40 },
            --{ name = 'very_extended_magazine', label = _U('very_extended_magazine'),         price = 60 }
        },
    },

    Ice = {
        Pos   = { x = -1812.82, y = 3092.39, z = 031.84 },
        Size  = { x = 1.6, y = 1.6, z = 1.0 },
        Color = { r = 255, g = 255, b = 255 },
        Type  = 23,
        Items = {
            { name = 'manche',     label = _U('manche'),      price = 5 },
            { name = 'cadrepistolmk2',  label = _U('cadrepistolmk2'),   price = 5 },
			{ name = 'cadrepistol',  label = _U('cadrepistol'),   price = 5 },
			{ name = 'cadreSNSPISTOL',  label = _U('cadreSNSPISTOL'),   price = 5 },
			{ name = 'cadrePISTOL50',  label = _U('cadrePISTOL50'),   price = 5 },
			{ name = 'cadreCOMBATPISTOL',  label = _U('cadreCOMBATPISTOL'),   price = 5 },
			{ name = 'cadreHEAVYPISTOL',  label = _U('cadreHEAVYPISTOL'),   price = 5 },
			{ name = 'cadreVINTAGEPISTOL',  label = _U('cadreVINTAGEPISTOL'),   price = 5 },
			{ name = 'cadreSTUNGUN',  label = _U('cadreSTUNGUN'),   price = 5 },
			{ name = 'cadreFLAREGUN',  label = _U('cadreFLAREGUN'),   price = 5 },
			{ name = 'cadreSMG_MK2',  label = _U('cadreSMG_MK2'),   price = 5 },
			{ name = 'cadreREVOLVER',  label = _U('cadreREVOLVER'),   price = 5 },
			{ name = 'cadreCARBINERIFLE_MK2',  label = _U('cadreCARBINERIFLE_MK2'),   price = 5 },
			{ name = 'cadrePUMPSHOTGUN',  label = _U('cadrePUMPSHOTGUN'),   price = 5 },
			{ name = 'cadreMUSKET',  label = _U('cadreMUSKET'),   price = 5 },
			{ name = 'cadreHEAVYSNIPER',  label = _U('cadreHEAVYSNIPER'),   price = 5 },
			{ name = 'cadreGRENADELAUNCHER_SMOKE',  label = _U('cadreGRENADELAUNCHER_SMOKE'),   price = 5 },
			{ name = 'cadreDOUBLEACTION',  label = _U('cadreDOUBLEACTION'),   price = 5 },
			{ name = 'cadreSPECIALCARBINE_MK2',  label = _U('cadreSPECIALCARBINE_MK2'),   price = 5 },
			{ name = 'cadrePUMPSHOTGUN_MK2',  label = _U('cadrePUMPSHOTGUN_MK2'),   price = 5 },
			{ name = 'planknucle',  label = _U('planknucle'),   price = 5 },
			{ name = 'planbat',  label = _U('planbat'),   price = 5 },
			{ name = 'plandagger',  label = _U('plandagger'),   price = 5 },
			{ name = 'planmachete',  label = _U('planmachete'),   price = 5 },
			{ name = 'planswithblade',  label = _U('planswithblade'),   price = 5 }
        },
    },

}


-----------------------
----- TELEPORTERS -----
Config.TeleportZones = {}
--Config.TeleportZones = {
  --EnterBuilding = {
  --  Pos       = { x = 132.608, y = -1293.978, z = 28.269 },
  --  Size      = { x = 1.2, y = 1.2, z = 0.1 },
   -- Color     = { r = 128, g = 128, b = 128 },
   -- Marker    = 1,
   -- Hint      = _U('e_to_enter_1'),-
   -- Teleport  = { x = 126.742, y = -1278.386, z = 28.569 }
  --},

  --ExitBuilding = {
  --  Pos       = { x = 132.517, y = -1290.901, z = 28.269 },
   -- Size      = { x = 1.2, y = 1.2, z = 0.1 },
   -- Color     = { r = 128, g = 128, b = 128 },
   -- Marker    = 1,
   -- Hint      = _U('e_to_exit_1'),
   -- Teleport  = { x = 131.175, y = -1295.598, z = 28.569 },
 -- },

--[[
 -- EnterHeliport = {
 --   Pos       = { x = 126.843, y = -729.012, z = 241.201 },
  --  Size      = { x = 2.0, y = 2.0, z = 0.2 },
 --   Color     = { r = 204, g = 204, b = 0 },
 --   Marker    = 1,
 --   Hint      = _U('e_to_enter_2),
 --   Teleport  = { x = -65.944, y = -818.589, z =  320.801 }
 -- },

 -- ExitHeliport = {
 --   Pos       = { x = -67.236, y = -821.702, z = 320.401 },
 --   Size      = { x = 2.0, y = 2.0, z = 0.2 },
  --  Color     = { r = 204, g = 204, b = 0 },
  --  Marker    = 1,
  --  Hint      = _U('e_to_exit_2'),
  --  Teleport  = { x = 124.164, y = -728.231, z = 241.801 },
 -- },
]]--
--}


-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Epicerie = {
  barman_outfit = {
    male = {
        ['tshirt_1'] = 75,  ['tshirt_2'] = 0,
        ['torso_1'] = 248,   ['torso_2'] = 15,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 44,
        ['pants_1'] = 9,   ['pants_2'] = 7,
        ['shoes_1'] = 24,   ['shoes_2'] = 0,
        ['helmet_1'] = 63,  ['helmet_2'] = 6
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 8,    ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 5,
        ['pants_1'] = 1,   ['pants_2'] = 4,
        ['shoes_1'] = 0,    ['shoes_2'] = 0,
        ['chain_1'] = 0,    ['chain_2'] = 2
    }
  },
  dancer_outfit_1 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 111,   ['torso_2'] = 3,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 4,
        ['pants_1'] = 24,   ['pants_2'] = 0,
        ['shoes_1'] = 30,   ['shoes_2'] = 1,
        ['chain_1'] = 0,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 4,
        ['pants_1'] = 22,   ['pants_2'] = 0,
        ['shoes_1'] = 18,   ['shoes_2'] = 0,
        ['chain_1'] = 61,   ['chain_2'] = 1
    }
  },
  dancer_outfit_2 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 62,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 1,
        ['pants_1'] = 4,    ['pants_2'] = 0,
        ['shoes_1'] = 34,   ['shoes_2'] = 0,
        ['chain_1'] = 118,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 2,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 4,
        ['pants_1'] = 20,   ['pants_2'] = 2,
        ['shoes_1'] = 18,   ['shoes_2'] = 2,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dancer_outfit_3 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 15,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 4,    ['pants_2'] = 0,
        ['shoes_1'] = 34,   ['shoes_2'] = 0,
        ['chain_1'] = 118,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 22,   ['torso_2'] = 1,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 19,   ['pants_2'] = 1,
        ['shoes_1'] = 19,   ['shoes_2'] = 3,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dancer_outfit_4 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 15,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 61,   ['pants_2'] = 5,
        ['shoes_1'] = 34,   ['shoes_2'] = 0,
        ['chain_1'] = 118,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 82,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 63,   ['pants_2'] = 11,
        ['shoes_1'] = 41,   ['shoes_2'] = 11,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dancer_outfit_5 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 15,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 21,   ['pants_2'] = 0,
        ['shoes_1'] = 34,   ['shoes_2'] = 0,
        ['chain_1'] = 118,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 15,   ['torso_2'] = 5,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 63,   ['pants_2'] = 2,
        ['shoes_1'] = 41,   ['shoes_2'] = 2,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dancer_outfit_6 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 15,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 81,   ['pants_2'] = 0,
        ['shoes_1'] = 34,   ['shoes_2'] = 0,
        ['chain_1'] = 118,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 18,   ['torso_2'] = 3,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 63,   ['pants_2'] = 10,
        ['shoes_1'] = 41,   ['shoes_2'] = 10,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  },
  dancer_outfit_7 = {
    male = {
        ['tshirt_1'] = 15,  ['tshirt_2'] = 0,
        ['torso_1'] = 15,   ['torso_2'] = 0,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 40,
        ['pants_1'] = 61,   ['pants_2'] = 9,
        ['shoes_1'] = 16,   ['shoes_2'] = 9,
        ['chain_1'] = 118,  ['chain_2'] = 0
    },
    female = {
        ['tshirt_1'] = 3,   ['tshirt_2'] = 0,
        ['torso_1'] = 111,  ['torso_2'] = 6,
        ['decals_1'] = 0,   ['decals_2'] = 0,
        ['arms'] = 15,
        ['pants_1'] = 63,   ['pants_2'] = 6,
        ['shoes_1'] = 41,   ['shoes_2'] = 6,
        ['chain_1'] = 0,    ['chain_2'] = 0
    }
  }
}
