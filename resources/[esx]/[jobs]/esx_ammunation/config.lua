Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 21
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 0, g = 255, b = 255 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.MaxInService               = -1
Config.Locale = 'fr'

Config.AmmuStations = {

  Ammu = {

    Blip = {
      Pos     = { x = 11.48, y = -1110.21, z = 28.86 },
      Sprite  = 119,
      Display = 4,
      Scale   = 1.2,
      Colour  = 77,
    },

    AuthorizedWeapons = {
    },

	  AuthorizedVehicles = {
		  { name = 'burrito3',  label = 'Burrito' },
	  },

    Armories = {
      { x = 23.59, y = -1105.65, z = 29.79 },
    },

    Cloakrooms = {
   --   { x = 1172.7686767575, y = 2636.0771484375, z = 36.78857421875},
    },


    Vehicles = {
      {
        Spawner    = { x = -7.51, y = -1109.06, z = 27.86 },
        SpawnPoint = { x = -7.51, y = -1109.06, z = 27.86},
        Heading    = 158.5,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 113.30500793457, y = -3109.3337402344, z = 6.0060696601868 },
        SpawnPoint = { x = 112.94457244873, y = -3102.5942382813, z = 6.0050659179688 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = -9.78, y =-1116.0, z = 27.08 },

    },

    BossActions = {
      { x = 11.48, y = -1110.21, z = 29.86 },
    },

  },

}
