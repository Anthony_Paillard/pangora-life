Locales['fr'] = {

    ['clients'] = 'clients',
    ['remove_comp_money'] = 'Retirer argent société',
    ['dep_money'] = 'Déposer argent',
    ['realtor'] = 'Gouverneur',
    ['amount_withdraw'] = 'montant du retrait',
    ['invalid_amount'] = 'montant invalide',
    ['amount_deposit'] = 'montant du dépôt',
    ['press_to_access'] = 'Appuyez sur ~INPUT_CONTEXT~ pour accéder au menu',
    ['amount'] = 'Montant',
    ['no_play_near'] = 'Aucun joueur à proximité',
    ['realtors'] = 'Gouvernement',
    ['client'] = 'Citoyen',
    ['boss_action'] = 'action Patron',
    ['veh_spawn'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir un véhicule',
    ['gouvernor'] = 'Menu Gouvernement',
    ['veh_delete'] = 'Appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule.',
    ['cloakroom'] = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le vestiaire.',
    ['clothes_civil'] = 'Tenue Civile',
    ['clothes_job'] = 'Tenue Travail',
    ['press_to_tp'] = 'Appuyez sur ~INPUT_CONTEXT~ pour entrer dans votre bureau',
    ['press_to_ring'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sonner au Gouvernement',
    ['press_to_exit'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir du gouvernement',
    ['press_to_garage'] = 'Appuyez sur ~INPUT_CONTEXT~ pour entrer dans le garage',
    ['press_to_exit_garage'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir du garage',
    ['press_to_exit_with_vehicle'] = 'Appuyez sur ~INPUT_CONTEXT~ pour sortir avec la voiture',

    ['press_to_helico'] = "Appuyez sur ~INPUT_CONTEXT~ pour vous rendre à l'hélico",
    ['press_to_exit_helico'] = "Appuyez sur ~INPUT_CONTEXT~ pour rentrer dans votre propriété",
    ['helico_spawn'] = "Appuyez sur ~INPUT_CONTEXT~ pour sortir l'hélico",
    ['helico_delete'] = "Appuyez sur ~INPUT_CONTEXT~ pour ranger l'hélico",

  -- Action Menu
  ['citizen_interaction'] = 'interaction avec le civil',
  ['vehicle_interaction'] = 'interaction véhicule',
  ['object_spawner'] = 'placer objets',

  ['id_card'] = 'carte d\'identité',
  ['no_players_nearby'] = 'aucun joueur à proximité',

  ['vehicle_info'] = 'infos véhicule',
  ['pick_lock'] = 'crocheter véhicule',
  ['vehicle_unlocked'] = 'véhicule ~g~déverrouillé~s~',
  ['no_vehicles_nearby'] = 'aucun véhicule à proximité',
  
  -- Vehicle Info Menu
  ['plate'] = 'Plaque d\'immatriculation',
  ['owner_unknown'] = 'propriétaire',
  ['owner'] = 'propriétaire',
}