Locales['de'] = {
    -- Cloakroom
    ['cloakroom']                = 'Vestiaires',
    ['no_outfit']                = 'Aucunes tenues disponible...',
    ['open_cloackroom']          = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir les vestiaire',
  
    -- Vault  
    ['get_weapon']               = 'Prendre armes',
    ['put_weapon']               = 'déposer armes',
    ['get_weapon_menu']          = 'Coffre - Prendre armes',
    ['put_weapon_menu']          = 'Coffre - déposer weglegen',
    ['get_object']               = 'Prendre Objets',
    ['put_object']               = 'déposer Objets',
    ['vault']                    = 'Coffre',
    ['open_vault']               = 'Appuyez sur ~INPUT_CONTEXT~ Pour ouvrir le coffre',
  
    -- Fridge  
    ['get_object']               = 'Prendre armes',
    ['put_object']               = 'déposer armes',
    ['fridge']                   = 'Réfrigérateur',
    ['open_fridge']              = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le réfrigérateur',
    ['casino_fridge_stock']      = 'Stock du casino',
    ['fridge_inventory']         = 'Stock réfrigérateur',
  
    -- Shops  
    ['shop']                     = 'Magasin',
    ['shop_menu']                = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir la boutique.',
    ['bought']                   = 'Vous avez acheté ~b~1x ~b~',
    ['not_enough_money']         = 'Vous n\'avez pas assez d\'argent.',
    ['max_item']                 = 'Vous avez trop d\'objets.',
  
    -- Vehicles  
    ['vehicle_menu']             = 'Vehicules',
    ['vehicle_out']              = 'votre véhicule est déja sortis!',
    ['vehicle_spawner']          = 'Appuyez sur ~INPUT_CONTEXT~ sortir un vehicule',
    ['store_vehicle']            = 'Appyez sur ~INPUT_CONTEXT~ pour stocker un vehicule',
    ['service_max']              = 'Full service: ',
    ['spawn_point_busy']         = 'Le lieu de spawn du véhicule est encombré!',
  
    -- Boss Menu  
    ['take_company_money']       = 'Prendre argent de société',
    ['deposit_money']            = 'Déposer argent société',
    ['amount_of_withdrawal']     = 'Montant',
    ['invalid_amount']           = 'Montant invalide',
    ['amount_of_deposit']        = 'Montant',
    ['open_bossmenu']            = 'Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu du patrons',
    ['invalid_quantity']         = 'Montant invalide',
    ['you_removed']              = 'Vous avez supprimé x',
    ['you_added']                = 'Vous avez ajouté x',
    ['quantity']                 = 'Montant',
    ['inventory']                = 'Inventaire',
    ['casino_stock']             = 'Stock du casino',
  
    -- Billing Menu  
    ['billing']                  = 'Facture',
    ['no_players_nearby']        = 'Aucuns joueurs a proximité',
    ['billing_amount']           = 'Montant de la facture',
    ['amount_invalid']           = 'Facture invalide',
  
    -- Crafting Menu  
    ['crafting']                 = 'Mixes',
    ['martini']                  = 'White Martini',
    ['icetea']                   = 'Ice Tea',
    ['drpepper']                 = 'Dr. Pepper',
    ['saucisson']                = 'Sausage',
    ['grapperaisin']             = 'Bunch of grapes',
    ['energy']                   = 'Energy Drink',
    ['jager']                    = 'Jägermeister',
    ['limonade']                 = 'Lemonade',
    ['vodka']                    = 'Vodka',
    ['ice']                      = 'Ice',
    ['soda']                     = 'Soda',
    ['whisky']                   = 'Whisky',
    ['rhum']                     = 'Rhum',
    ['tequila']                  = 'Tequila',
    ['menthe']                   = 'Mint',
    ['jusfruit']                 = 'Fruchtsaft',
    ['jagerbomb']                = 'Jägerbomb',
    ['bolcacahuetes']            = 'Peanuts',
    ['bolnoixcajou']             = 'Cashew nuts',
    ['bolpistache']              = 'Pistachios',
    ['bolchips']                 = 'Crisps',
    ['jagerbomb']                = 'Jägerbomb',
    ['golem']                    = 'Golem',
    ['whiskycoca']               = 'Whisky-coke',
    ['vodkaenergy']              = 'Vodka-energy',
    ['vodkafruit']               = 'Vodka-fruit juice',
    ['rhumfruit']                = 'Rum-fruit juice',
    ['teqpaf']                   = 'Teq\'paf',
    ['rhumcoca']                 = 'Rhum-coke',
    ['mojito']                   = 'Mojito',
    ['mixapero']                 = 'Aperitif Mix',
    ['metreshooter']             = 'Shooter meter',
    ['jagercerbere']             = 'Jäger Cerberus',
    ['assembling_cocktail']      = 'Vous avez réussi votre mix!',
    ['craft_miss']               = 'Vous avez raté votre mix ...',
    ['not_enough']               = 'Pas assez de ~r~',
    ['craft']                    = 'Vollständiges Mischen von ~g~ abgeschlossen!',
	['cchip']                    = 'Jetons',
	
	-- Usable Itmems
	['used_icetea'] = 'Vous avez bu ~y~1x~s~ ~g~Eistee ~s~!',
	['used_limonade'] = 'Vous avez bu ~y~1x ~g~Limonade ~s~!',
	['used_drpepper'] = 'Vous avez bu ~y~1x~ ~g~Dr. Pepper ~s~!',
	['used_energy'] = 'Vous avez bu ~y~1x ~g~Energy Drink ~s~!',
	['used_vodka'] = 'Vous avez bu ~y~1x ~g~Vodka ~s~!',
	['used_jager'] = 'Vous avez bu ~y~1x~ ~g~Jägermeister ~s~!',
	['used_rhum'] = 'Vous avez bu ~y~1x ~g~Rhum ~s~!',
	['used_whisky'] = 'Vous avez bu ~y~1x ~g~Whisky ~s~!',
	['used_martini'] = 'Vous avez bu ~y~1x ~g~Martini ~s~!',
	['used_mixapero'] = 'Vous avez bu ~y~1x ~g~Mixapero ~s~!',
	['used_ovoes'] = 'Vous avez bu ~y~1x ~g~Ovoes ~s~!',
	['used_vitelaass'] = 'Vous avez bu ~y~1x ~g~Vitelaass ~s~!',
	['used_polvogre'] = 'Vous avez bu ~y~1x ~g~Polvogre ~s~!',
	['used_bolcacahuetes'] = 'Vous avez bu ~y~1x ~g~Erdnüsse ~s~!',
	['used_bolnoixcajou'] = 'Vous avez bu ~y~1x ~g~Cashewnüsse ~s~!',
	['used_bolpistache'] = 'Vous avez bu ~y~1x ~g~Pistazien ~s~!',
	['used_bolchips'] = 'Vous avez bu ~y~1x ~g~Chips ~s~!',
	['used_saucisson'] = 'Vous avez bu ~y~1x ~g~Wurst ~s~!',
	['used_grapperaisin'] = 'Vous avez bu ~y~1x ~g~Trauben ~s~!',
	['used_jagerbomb'] = 'Vous avez bu ~y~1x ~g~Jägerbombe ~s~!',
	['used_golem'] = 'Vous avez bu ~y~1x ~g~Golem ~s~!',
	['used_whiskycoca'] = 'Vous avez bu ~y~1x ~g~Whisky-Cola ~s~!',
	['used_vodkaenergy'] = 'Vous avez bu ~y~1x ~g~Vodka-Energy ~s~!',
	['used_vodkafruit'] = 'Vous avez bu ~y~1x ~g~Vodka mit Saft ~s~!',
	['used_rhumfruit'] = 'Vous avez bu ~y~1x ~g~Whisky-Cola ~s~!',
	['used_teqpaf'] = 'Vous avez bu ~y~1x ~g~Teqpaf ~s~!',
	['used_rhumcoca'] = 'Vous avez bu ~y~1x ~g~Rhum-Cola ~s~!',
	['used_mojito'] = 'Vous avez bu ~y~1x ~g~Mojito ~s~!',
  
    -- Blips 
    ['map_blip']                 = 'Casino - Diamond',
    ['casino']                   = 'Casino - Diamond',
  
    -- Phone  
    ['casino_phone']             = 'Casino - Diamond',
    ['casino_customer']          = 'Kunde',
	['alert_casino']             = 'Kunde',
  
    -- Teleporters
    ['e_to_enter_1']             = 'Appuyez sur ~INPUT_PICKUP~ pour entrer',
    ['e_to_exit_1']              = 'Appuyez sur  ~INPUT_PICKUP~ pour sortir',
    ['e_to_enter_2']             = 'Appuyez sur  ~INPUT_PICKUP~ pour entrer.',
    ['e_to_exit_2']              = 'Appuyez sur  ~INPUT_PICKUP~ pour sortir.',
    
  }