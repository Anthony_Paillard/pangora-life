Locales['fr'] = {
  --Global menus
  ['belongs']                         = 'Vous appartient désormais',
  ['bleach amount']                   = 'Montant à blanchir',
  ['purchase_type']                   = 'type d\'achat',
  ['society_type']                    = 'societé',
  ['staff_type']                      = 'personnel',
  ['broke_company']                   = 'Vous n\'avez pas assez d\'argent sur votre compte société',
  ['buy_vehicle_shop']                = 'Acheter %s pour $%s?',
  ['buy_vehicle']                     = 'Acheter Moto',
  ['car_dealer']                      = 'Concessionnaire Motos',
  ['create_bill']                     = 'Créer facture',
  ['dealer_boss']                     = 'Concessionnaire - Patron',
  ['delivered']                       = 'Moto ~g~rendu~s~ au concessionnaire',
  ['depop_vehicle']                   = 'Rentrer véhicule',
  ['deposit_amount']                  = 'Montant du dépôt',
  ['deposit_money']                   = 'Déposer argent',
  ['experianced']                     = 'Experimente',
  ['get_rented_vehicles']             = 'Véhicules en location',
  ['invalid_amount']                  = 'Montant invalide',
  ['invoice_amount']                  = 'Montant de la facture',
  ['leaving']                         = 'Sortir une Moto personnel',
  ['no']                              = 'Non',
  ['no_players']                      = 'Aucun joueur à proximité',
  ['not_enough_money']                = 'Vous n\'avez pas assez d\'argent',
  ['not_rental']                      = 'Ce n\'est pas un ~r~Moto de location~s~',
  ['not_yours']                       = 'Ce véhicule ne vous appartient pas',
  ['paid_rental']                     = 'Vous avez ~g~payé~s~ votre location de Moto: ~g~$%s~s~',
  ['personal_vehicle']                = 'Moto personnelle',
  ['pop_vehicle']                     = 'Sortir Moto',
  ['recruit']                         = 'Recrue',
  ['rent_vehicle']                    = 'Concessionnaire - Moto en location',
  ['rental_amount']                   = 'Montant de la location',
  ['sell_menu']                       = 'Appuez sur ~INPUT_CONTEXT~ pour vendre ~y~%s~s~ au prix de ~g~$%s~s~',
  ['set_vehicle_owner_rent']          = 'Attribuer Moto [Location]',
  ['set_vehicle_owner_sell']          = 'Attribuer Moto [Vente]',
  ['set_vehicle_owner_sell_society']  = 'Attribuer Moto [Vente] [Société]',
  ['shop_menu']                       = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au menu',
  ['the_boss']                        = 'Patron',
  ['vehicle']                         = 'La Moto',
  ['vehicle_dealer']                  = 'Concessionnaire - Motos',
  ['vehicle_menu']                    = 'Appuez sur ~INPUT_CONTEXT~ pour rendre votre véhicule',
  ['vehicle_purchased']               = 'Vous avez acheté un véhicule',
  ['vehicle_set_owned']               = 'La Moto ~y~%s~s~ a été attribué à ~b~%s~s~',
  ['vehicle_set_rented']              = 'La Moto ~y~%s~s~ a été loué à ~b~%s~s~',
  ['vehicle_sold']                    = 'Moto ~g~vendu~s~',
  ['vehicle_sold_to']                 = 'Moto ~y~%s~s~ vendu à ~b~%s~s~',
  ['wash_money']                      = 'Blanchir argent',
  ['withdraw_amount']                 = 'Montant du retrait',
  ['withdraw_money']                  = 'Retirer argent société',
  ['yes']                             = 'Oui',
  ['deposit_stock']                   = 'déposer Stock',
  ['take_stock']                      = 'prendre Stock',
  ['dealership_stock']                = 'concession Stock',
  ['amount']                          = 'quantité',
  ['quantity_invalid']                = 'quantité invalide',
  ['inventory']                       = 'inventaire',
  ['dealership']                      = 'concession Moto',
  ['dealer_customers']                = 'client concession',
  ['have_withdrawn']                  = 'Vous avez retiré ',
  ['added']                           = 'vous avez ajouté ',
}
