Config.Jobs.fermier = {
  BlipInfos = {
    Sprite = 206,
    Color = 8
  },
  Vehicles = {
    Truck = {
      Spawner = 1,
      Hash = "tractor2",
      Trailer = "graintrailer",
      HasCaution = true
    }
  },
  Zones = {
    CloakRoom = {
      Pos   = {x = 1930.08, y = 4635.42, z = 39.461},
      Size  = {x = 2.0, y = 2.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = true,
      Name  = _U('m_fermier_locker'),
      Type  = "cloakroom",
      Hint  = _U('cloak_change'),
      GPS   = 0
      -- GPS = {x = 1935.21, y = 4645.14, z = 39.61}
    },

    Ble = {
      Pos   = {x = 2537.26, y = 4812.46, z = 33.10},
      Size  = {x = 25.0, y = 25.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('m_ble'),
      Type  = "work",
      Item  = {
        {
          name   = _U('m_ble'),
          db_name= "ble",
          time   = 1000,
          max    = 50,
          add    = 1,
          remove = 1,
          requires = "nothing",
          requires_name = "Nothing",
          drop   = 100
        }
      },
      Hint  = _U('m_recoltble'),
      GPS = {x = 2897.90, y = 4379.80, z = 49.37}
    },

    Moulure = {
      Pos   = {x = 2897.90, y = 4379.80, z = 49.37},
      Size  = {x = 25.0, y = 25.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('m_ble_smelting'),
      Type  = "work",
      Item  = {
        {
          name   = _U('m_farine'),
          db_name= "farine",
          time   = 1000,
          max    = 50,
          add    = 1,
          remove = 2,
          requires = "ble",
          requires_name = _U('ble'),
          drop   = 100
        },
      },
      Hint  = _U('m_ble_button'),
      GPS = {x = 131.01, y = 6427.95, z = 30.36}
    },

    VehicleSpawner = {
      Pos   = {x = 1935.21, y = 4645.14, z = 39.61},
      Size  = {x = 2.0, y =2.0, z = 1.0},
      Color = {r = 0, g = 153, b = 239},
      Marker= 27,
      Blip  = false,
      Name  = _U('spawn_veh'),
      Type  = "vehspawner",
      Spawner = 1,
      Hint  = _U('spawn_veh_button'),
      Caution = 500,
      GPS = {x = 2537.26, y = 4812.46, z = 33.10}
    },

    VehicleSpawnPoint = {
      Pos   = {x = 1929.48, y = 4641.16, z = 39.49},
      Size  = {x = 5.0, y = 5.0, z = 1.0},
      Marker= -1,
      Blip  = false,
      Name  = _U('service_vh'),
      Type  = "vehspawnpt",
      Spawner = 1,
      Heading = 69.00,
      GPS = {x = 2537.26, y = 4812.46, z = 33.10}
    },

    VehicleDeletePoint = {
      Pos   = {x = 1958.48, y = 4641.48, z = 39.78},
      Size  = {x = 5.0, y = 5.0, z = 1.0},
      Color = {r = 255, g = 0, b = 0},
      Marker= 27,
      Blip  = false,
      Name  = _U('return_vh'),
      Type  = "vehdelete",
      Hint  = _U('return_vh_button'),
      Spawner = 1,
      Caution = 500,
      Teleport = 0
    },

    farine = {
      Pos   = {x = 131.01, y = 6427.95, z = 30.39},
      Size  = {x = 2.0, y = 2.0, z = 2.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('m_sell_farine'),
      Type  = "delivery",
      Spawner = 1,
      Item  = {
        {
          name   = _U('delivery'),
          time   = 1000,
          remove = 1,
          max    = 50, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
          price  = 20,
          requires = "farine",
          requires_name = _U('m_farine'),
          drop   = 100
        }
      },
      Hint  = _U('m_deliver_farine'),
      GPS = {x = 1958.48, y = 4641.48, z = 39.78},
    }
  }
}
