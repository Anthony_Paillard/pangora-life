Config.Jobs.surfeur = {
  BlipInfos = {
    Sprite = 471,
    Color = 18
  },
  Vehicles = {
    Truck = {
      Spawner = 1,
      Hash = "paradise",
      Trailer = "none",
      HasCaution = true
    }
  },
  Zones = {
    CloakRoom = {
      Pos   = {x = -1219.18, y = -1518.01, z = 3.35},
      Size  = {x = 2.0, y = 2.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = true,
      Name  = _U('sf_locker_room'),
      Type  = "cloakroom",
      Hint  = _U('cloak_change'),
      GPS   = 0
    },
	
    Wood = {
      Pos   = {x = -2091.78, y = 2642.44, z = 1.89},
      Size  = {x = 10.0, y = 10.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('sf_mapblip'),
      Type  = "work",
      Item  = {
        {
          name   = _U('sf_wood'),
          db_name= "bamboo",
          time   = 1000,
          max    = 25,
          add    = 1,
          remove = 1,
          requires = "nothing",
          requires_name = "Nothing",
          drop   = 100
        }
      },
      Hint  = _U('sf_pickup'),
      GPS = {x = -1209.46, y = -1793.38, z = 2.90}
    },

    Tailleplanche = {
      Pos   = {x = -1209.46, y = -1793.38, z = 2.90},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('sf_cutwood'),
      Type  = "work",
      Item  = {
        {
          name   = _U('sf_cutwood'),
          db_name= "planchetaille",
          time   = 1000,
          max    = 50,
          add    = 1,
          remove = 1,
          requires = "bamboo",
          requires_name = _U('sf_wood'),
          drop   = 100
        }
      },
      Hint  = _U('sf_cutwood_button'),
      GPS = {x = -1205.46, y = -1790.38, z = 2.90}
    },

    Vernisage = {
      Pos   = {x = -1205.46, y = -1790.38, z = 2.90},
      Size  = {x = 5.0, y = 5.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('sf_board'),
      Type  = "work",
      Item  = {
        {
          name   = _U('sf_planks'),
          db_name= "planchesurf",
          time   = 1000,
          max    = 50,
          add    = 1,
          remove = 1,
          requires = "planchetaille",
          requires_name = _U('sf_cutwood'),
          drop   = 100
        }
      },
      Hint  = _U('sf_pick_boards'),
      GPS = {x = 143.15, y = 6446.16, z = 30.46}
    },

    VehicleSpawner = {
      Pos   = {x = -1218.85, y = -1512.78, z = 3.39},
      Size  = {x = 2.0, y =2.0, z = 1.0},
      Color = {r = 0, g = 153, b = 239},
      Marker= 27,
      Blip  = false,
      Name  = _U('spawn_veh'),
      Type  = "vehspawner",
      Spawner = 1,
      Hint  = _U('spawn_veh_button'),
      Caution = 500,
      GPS = {x = -2091.78, y = 2642.44, z = 1.89}
    },

    VehicleSpawnPoint = {
      Pos   = {x = -1190.29, y = -1460.37, z = 4.37, angle = 37.172},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Marker= -1,
      Blip  = false,
      Name  = _U('service_vh'),
      Type  = "vehspawnpt",
      Spawner = 1,
      Heading = 175.0,
      GPS = 0
    },

    VehicleDeletePoint = {
      Pos   = {x = -1191.43, y = -1470.60, z = 3.47},
      Size  = {x = 5.0, y = 5.0, z = 1.0},
      Color = {r = 255, g = 0, b = 0},
      Marker= 27,
      Blip  = false,
      Name  = _U('return_vh'),
      Type  = "vehdelete",
      Hint  = _U('return_vh_button'),
      Spawner = 1,
      Caution = 500,
      Teleport = 0
    },

    Delivery = {
      Pos   = {x = 143.15, y = 6446.16, z = 30.56},
      Size  = {x = 2.0, y = 2.0, z = 2.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('delivery_point'),
      Type  = "delivery",
      Spawner = 1,
      Item  = {
        {
          name   = _U('delivery'),
          time   = 1000,
          remove = 1,
          max    = 50, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
          price  = 13,
          requires = "planchesurf",
          requires_name = _U('sf_board'),
          drop   = 100
        }
      },
      Hint  = _U('sf_deliver_button'),
      GPS = {x = -2091.78, y = 2642.44, z = 1.89}
    }
  }
}
