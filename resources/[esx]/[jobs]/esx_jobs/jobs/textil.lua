Config.Jobs.textil = {
  BlipInfos = {
    Sprite = 366,
    Color = 4,
	Name = "textil"
  },
  Vehicles = {
    Truck = {
      Spawner = 1,
      Hash = "youga2",
      Trailer = "none",
      HasCaution = true
    }
  },
  Zones = {
    CloakRoom = {
      Pos   = {x = 706.735412597656, y = -960.902893066406, z = 29.3953247070313},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Color = {r = 204, g = 204, b = 0},
      Marker= 1,
      Blip  = true,
      Name  = _U('dd_dress_locker'),
      Type  = "cloakroom",
      Hint  = _U('cloak_change'),
      GPS = 0
    },

    Whool = {
      Pos   = {x = 1978.92407226563, y = 5171.70166015625, z = 46.6391181945801},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Color = {r = 204, g = 204, b = 0},
      Marker= 1,
      Blip  = true,
      Name  = _U('dd_wool'),
      Type  = "work",
      Item  = {
        {
          name   = _U('dd_wool'),
          db_name= "whool",
          time   = 3000,
          max    = 60,
          add    = 1,
          remove = 1,
          requires = "nothing",
          requires_name = "Nothing",
          drop   = 100
        }
      },
      Hint  = _U('dd_pickup'),
      GPS = 0
    },

    Fabric = {
      Pos   = {x = 720.28, y = -967.20, z = 29.40},
      Size  = {x = 1.5, y = 1.5, z = 1.0},
      Color = {r = 204, g = 204, b = 0},
      Marker= 1,
      Blip  = true,
      Name  = _U('dd_fabric'),
      Type  = "work",
      Item  = {
        {
          name   = _U('dd_fabric'),
          db_name= "fabric",
          time   = 5000,
          max    = 80,
          add    = 1,
          remove = 1,
          requires = "whool",
          requires_name = _U('dd_wool'),
          drop   = 100
        }
      },
      Hint  = _U('dd_makefabric'),
      GPS = 0
    },

    Clothe = {
      Pos   = {x = 711.42, y = -969.36, z = 29.40},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Color = {r = 204, g = 204, b = 0},
      Marker= 1,
      Blip  = true,
      Name  = _U('dd_clothing'),
      Type  = "work",
      Item  = {
        {
          name   = _U('dd_clothing'),
          db_name= "clothe",
          time   = 4000,
          max    = 60,
          add    = 1,
          remove = 1,
          requires = "fabric",
          requires_name = _U('dd_fabric'),
          drop   = 100
        }
      },
      Hint  = _U('dd_makeclothing'),
      GPS = 0
    },

    VehicleSpawner = {
      Pos   = {x = 740.808776855469, y = -970.066650390625, z = 23.4693908691406},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Color = {r = 204, g = 204, b = 0},
      Marker= 1,
      Blip  = false,
      Name  = _U('spawn_veh'),
      Type  = "vehspawner",
      Spawner = 1,
      Hint  = _U('spawn_veh_button'),
      Caution = 500,
      GPS = 0
    },

    VehicleSpawnPoint = {
      Pos   = {x = 747.31396484375, y = -966.235778808594, z = 23.705005645752},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Marker= -1,
      Blip  = false,
      Name  = _U('service_vh'),
      Type  = "vehspawnpt",
      Spawner = 1,
      Heading = 270.1,
      GPS = 0
    },

    VehicleDeletePoint = {
      Pos   = {x = 693.796325683594, y = -963.010498046875, z = 22.8247337341309},
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Color = {r = 255, g = 0, b = 0},
      Marker= 1,
      Blip  = false,
      Name  = _U('return_vh'),
      Type  = "vehdelete",
      Hint  = _U('return_vh_button'),
      Spawner = 1,
      Caution = 500,
      GPS = 0,
      Teleport = 0
    },

    Delivery = {
      Pos   = {x = -1177.789,y = -814.259,z = 13.466},
      Color = {r = 204, g = 204, b = 0},
      Size  = {x = 5.0, y = 5.0, z = 3.0},
      Marker= 1,
      Blip  = true,
      Name  = _U('delivery_point'),
      Type  = "delivery",
      Spawner = 1,
      Item  = {
        {
          name   = _U('delivery'),
          time   = 500,
          remove = 1,
          max    = 100, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
          price  = 45,
          requires = "clothe",
          requires_name = _U('dd_clothing'),
          drop   = 100
        }
      },
      Hint  = _U('dd_deliver_clothes'),
      GPS = 0
    }
  }
}
