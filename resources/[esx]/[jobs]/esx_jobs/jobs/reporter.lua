Config.Jobs.reporter = {
  BlipInfos = {
    Sprite = 184,
    Color = 1,
	Name = "reporter"
  },
  Vehicles = {
    Truck = {
      Spawner = 1,
      Hash = "rumpo",
      Trailer = "none",
      HasCaution = false
    }
  },
  Zones = {
    VehicleSpawner = {
      Pos   = { x = -1094.8233642575, y = -260.02233886719, z = 36.70161819458 },
	  Heading    = 213.28,
      Size  = {x = 2.0, y = 2.0, z = 0.2},
      Color = {r = 204, g = 204, b = 0},
      Marker= 1,
      Blip  = true,
      Name  = _U('reporter_name'),
      Type  = "vehspawner",
      Spawner = 1,
      Hint  = _U('reporter_garage'),
      Caution = 0
    },

    VehicleSpawnPoint = {
      Pos   = { x = -1099.749267581, y = -256.52188110352, z = 37.699871063232 },
      Size  = {x = 3.0, y = 3.0, z = 1.0},
      Marker= -1,
      Blip  = false,
      Name  = _U('service_vh'),
      Type  = "vehspawnpt",
      Spawner = 1,
      Heading = 200.1
    },

    VehicleDeletePoint = {
      Pos   = { x = -1065.73300322266,  y = -228.27574157715, z = 37.106357574463 },
	  Heading    = 133.85,
      Size  = {x = 5.0, y = 5.0, z = 0.2},
      Color = {r = 255, g = 0, b = 0},
      Marker= 1,
      Blip  = false,
      Name  = _U('return_vh'),
      Type  = "vehdelete",
      Hint  = _U('return_vh_button'),
      Spawner = 1,
      Caution = 0,
      GPS = 0,
      Teleport = { x = -1074.123046875, y = -229.65292358398, z = 37.863231658936 }
    }
  }
}
