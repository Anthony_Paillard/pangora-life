Config.Jobs.seek = {
  BlipInfos = {
    Sprite = 439,
    Color = 5

  },
  Vehicles = {
    Truck = {
      Spawner = 1,
      Hash = "submersible",
      Trailer = "none",
      HasCaution = true
    }
  },
  Zones = {
    CloakRoom = { 
      Pos   = {x = 3807.621, y = 4478.498, z = 5.365},
      Size  = {x = 2.0, y = 2.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = true,
      Name  = _U('f_oil_refinerr'),
      Type  = "cloakroom",
      Hint  = _U('cloak_change'),
      GPS = 0
      -- GPS = {x = 554.597, y = -2314.43, z = 4.86293}
    },

    ChampPetrol = {
      Pos   = {x = 4154.45, y = 4756.17, z = -94.76},
      Size  = {x = 25.0, y = 25.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('f_drill_oilr'),
      Type  = "work2",
      Item  = {
        {
          name   = _U('f_fuelr'),
          db_name= "tresor",
          time   = 1000,
          max    = 10,
          add    = 1,
          remove = 1,
          requires = "nothing",
          requires_name = "Nothing",
          drop   = 100
        }
      },
      Hint  = _U('f_drillbuttonr'),
      GPS = {x = 4288.25, y = 4399.293, z = -58.745}
    },
ChampPetrol2 = {
      Pos   = {x = 4288.25, y = 4399.293, z = -58.745},
      Size  = {x = 25.0, y = 25.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('f_drill_oilr'),
      Type  = "work2",
      Item  = {
        {
          name   = _U('f_fuelr'),
          db_name= "tresor",
          time   = 1000,
          max    = 20,
          add    = 1,
          remove = 1,
          requires = "nothing",
          requires_name = "Nothing",
          drop   = 100
        }
      },
      Hint  = _U('f_drillbuttonr'),
      GPS = {x = 4489.854, y = 3938.44, z = -131.90}
    },
ChampPetrol3 = {
      Pos   = {x = 4489.854, y = 3938.44, z = -131.90},
      Size  = {x = 25.0, y = 25.0, z = 1.0},
      Color = {r = 255, g = 102, b = 231},
      Marker= 27,
      Blip  = false,
      Name  = _U('f_drill_oilr'),
      Type  = "work2",
      Item  = {
        {
          name   = _U('f_fuelr'),
          db_name= "tresor",
          time   = 1000,
          max    = 30,
          add    = 1,
          remove = 1,
          requires = "nothing",
          requires_name = "Nothing",
          drop   = 100
        }
      },
      Hint  = _U('f_drillbuttonr'),
      GPS = {x = 3847.760, y = 4445.267, z = -0.474}
    },

    VehicleSpawner = { -- 2
      Pos   = {x = 3866.184, y = 4463.797, z = 1.828},
      Size  = {x = 2.0, y =2.0, z = 1.0},
      Color = {r = 0, g = 153, b = 239},
      Marker= 27,
      Blip  = false,
      Name  = _U('spawn_veh'),
      Type  = "vehspawner",
      Spawner = 1,
      Hint  = _U('spawn_truck_button'),
      Caution = 500,
      GPS = {x = 4154.45, y = 4756.17, z = -94.76}
    },

    VehicleSpawnPoint = {
      Pos   = {x = 3869.958, y = 4467.650, z = -1.648},
      Size  = {x = 5.0, y = 5.0, z = 1.0},
      Marker= -1,
      Blip  = false,
      Name  = _U('service_vh'),
      Type  = "vehspawnpt",
      Spawner = 1,
      Heading = 353.0,
      GPS = {x = 4154.45, y = 4756.17, z = -94.76}
    },

    VehicleDeletePoint = {
      Pos   = {x = 3853.96, y = 4445.33, z = 0.7},
      Size  = {x = 5.0, y = 5.0, z = 1.0},
      Color = {r = 255, g = 0, b = 0},
      Marker= 27,
      Blip  = false,
      Name  = _U('return_vh'),
      Type  = "vehdelete",
      Hint  = _U('return_vh_button'),
      Spawner = 1,
      Caution = 500,
      Teleport = 0,
      GPS = {x = 144.724, y = 6459.697, z = 31.373}
    },

    Delivery = {
      Pos   = {x = 144.724, y = 6459.697, z = 31.373},
      Color = {r = 255, g = 102, b = 231},
      Size  = {x = 2.0, y = 2.0, z = 2.0},
      Marker= 27,
      Blip  = false,
      Name  = _U('f_deliver_gasr'),
      Type  = "delivery",
      Spawner = 1,
      Item  = {
        {
          name   = _U('delivery'),
          time   = 1000,
          remove = 1,
          max    = 50, -- if not present, probably an error at itemQtty >= item.max in esx_jobs_sv.lua
          price  = 10,
          requires = "tresor",
          requires_name = _U('f_gasr'),
          drop   = 100
        }
      },
      Hint  = _U('f_deliver_gasr'),
      GPS = {x = -65.66, y = -2231.83, z = 6.91}
    }
  }
}
