Config              = {}
Config.DrawDistance = 100.0
Config.Locale       = 'en'

Config.Plates = {

  fermier = "FERM",
  lumberjack = "BUCH",
  vigneron = "VIGN",
  textil = "COUT",
  samcro = "SAMC",

  taxi = "TAXI",
  fisherman = "FISH",
  cop = "LSPD",
  ambulance = "EMS",
  depanneur = "MECA",
  fuel = "FUEL",
  seek = "SEEK",
  lumberjack = "BUCH",
  miner = "MINE",
  reporter = "JOUR",
  slaughterer = "ABAT",
  surfeur = "SURF"
}

Config.Jobs = {}

Config.PublicZones = {

    --------------------Ascenseur Carshowroom--------------------------
   EnterBuilding23 = {
   Pos       = {x = -146.37,y = -603.91,z = 166.00 },
   Size      = { x = 1.5, y = 1.5, z = 0.2 },
   Color     = { r = 204, g = 204, b = 0 },
   Marker    = 23,
   Blip      = true,
   Name      = "Le Maclerait Libéré",
   Type      = "teleport",
   Hint      = "",
   Teleport  = {x = -138.88,y = -588.73,z = 166.00  },
  },

  ExitBuilding23 = {
   Pos       = {x = -138.88,y = -588.73,z = 166.00  },
   Size      = { x = 1.5, y = 1.5, z = 0.2 },
   Color     = { r = 204, g = 204, b = 0 },
   Marker    = 23,
   Blip      = true,
   Name      = "Le Maclerait Libéré",
   Type      = "teleport",
   Hint      = "Appuyez sur ~INPUT_PICKUP~ pour descendre au Concess",
   Teleport  = {x = -41.95,y = -1093.15,z = 26.42 },
   },

   ----- Gouvernement
     EnterBuilding30 = {
       Pos       = { x = -429.612, y = 1109.584, z = 326.682 },
       Size      = { x = 1.5, y = 1.5, z = 0.2 },
       Color     = { r = 64, g = 0, b = 74 },
       Marker    = 1,
       Blip      = false,
       Name      = "Le Maclerait Libéré",
       Type      = "teleport",
       Hint      = "Appuyez sur ~INPUT_PICKUP~ entrer au Gouvernement.",
       Teleport  = {x = 275.914,y = -268.664,z = 52.939 },
     },

     ExitBuilding30 = {
       Pos       = {x = 277.916,y = -268.098,z = 52.939},
       Size      = { x = 1.5, y = 1.5, z = 0.2 },
       Color     = { r = 64, g = 0, b = 74 },
       Marker    = 1,
       Blip      = false,
       Name      = "Le Maclerait Libéré",
       Type      = "teleport",
       Hint      = "Appuyez sur ~INPUT_PICKUP~ pour sortir du Gouvernement.",
       Teleport  = { x = -429.612, y = 1109.584, z = 326.682},
     },
}
