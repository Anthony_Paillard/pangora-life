ESX                    = nil

PlayersHarvesting      = {}
PlayersHarvesting2     = {}
PlayersCrafting        = {}
PlayersCrafting2       = {}
PlayersCrafting3       = {}
PlayersCrafting4       = {}
PlayersCrafting5       = {}
PlayersVente  	   		= {}
local CreatedInstances = {}


function randomFloat(lower, greater)
  return lower + math.random()  * (greater - lower);
end

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('esx:playerLoaded', function(source)
	
	local _source = source
	
	TriggerClientEvent('esx_coffeeshop:setTimeDiff', _source, os.time())
	TriggerClientEvent('esx_coffeeshop:onCreatedInstanceData', _source, CreatedInstances)

end)

if Config.MaxInService ~= 4 then
  TriggerEvent('esx_service:activateService', 'coffeeshop', Config.MaxInService)
end

TriggerEvent('esx_society:registerSociety', 'coffeeshop', 'Coffee Shop', 'society_coffeeshop', 'society_coffeeshop', 'society_coffeeshop', {type = 'private'})

-------------- Récupération du Tabac-------------
local function Harvest(source)

  SetTimeout(4000, function()

    if PlayersHarvesting[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local TabacQuantity = xPlayer.getInventoryItem('tabac').count

      if TabacQuantity >= 50 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
                xPlayer.addInventoryItem('tabac', 1)

        Harvest(source)
      end
    end
  end)
end

RegisterServerEvent('esx_coffeeshop:startHarvest')
AddEventHandler('esx_coffeeshop:startHarvest', function()

    local _source = source
    
    if PlayersHarvesting[_source] == false then
        TriggerClientEvent('esx:showNotification', _source, '~r~Pas bien de glitch!')
        PlayersHarvesting[_source] = false
        
    else
        PlayersHarvesting[_source] = true
        TriggerClientEvent('esx:showNotification', _source, _U('recovery_tabac'))
        Harvest(source)
    end
end)

RegisterServerEvent('esx_coffeeshop:stopHarvest')
AddEventHandler('esx_coffeeshop:stopHarvest', function()

    local _source = source
    if PlayersHarvesting[_source] == true then
        PlayersHarvesting[_source] = false
        TriggerClientEvent('esx:showNotification', _source, 'Vous ~r~sortez ~w~de la ~g~zone...')
        Harvest(source)
    else
        TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~recolter~w~ à présent!')        
        PlayersHarvesting[_source] = true
    end
end)


------------ Séchage Tabac  --------------
local function Craft(source)

  SetTimeout(4000, function()

    if PlayersCrafting2[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local Tabac2Quantity  = xPlayer.getInventoryItem('tabac').count
      if Tabac2Quantity <= 0 then
        TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
      else
                xPlayer.removeInventoryItem('tabac', 1)
                xPlayer.addInventoryItem('tabacsec', 1)

        Craft(source)
      end
    end
  end)
end

RegisterServerEvent('esx_coffeeshop:startCraft')
AddEventHandler('esx_coffeeshop:startCraft', function()

    local _source = source
    
    if PlayersCrafting2[_source] == false then
        TriggerClientEvent('esx:showNotification', _source, '~r~Pas bien de glitch!')
        PlayersCrafting2[_source] = false
        
    else
        PlayersCrafting2[_source] = true
        TriggerClientEvent('esx:showNotification', _source, _U('sechage_tabac'))
        Craft(_source)
    end
end)

RegisterServerEvent('esx_coffeeshop:stopCraft')
AddEventHandler('esx_coffeeshop:stopCraft', function()

    local _source = source
    if PlayersCrafting2[_source] == true then
        PlayersCrafting2[_source] = false
        TriggerClientEvent('esx:showNotification', _source, 'Vous ~r~sortez ~w~de la ~g~zone...')
        Craft(_source)
    else
        TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~sécher~w~ à présent!')        
        PlayersCrafting2[_source] = true
    end
end)

------------ Fabrication Produit Fini ---------
local function Craft2(source, _miam)

  SetTimeout(4000, function()

    if PlayersCrafting3[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)
		local Tabac3Quantity  = xPlayer.getInventoryItem('tabacsec').count
		local WeedQuantity  = xPlayer.getInventoryItem('weed_pooch').count
		
		if _miam == 'marlbo' then
			if Tabac3Quantity < 1 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
				xPlayer.removeInventoryItem('tabacsec', 1)
				xPlayer.addInventoryItem('marlbo', 1)
			end
			Craft2(source)
		 elseif _miam == 'luckystrike' then
			if Tabac3Quantity < 1then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
				xPlayer.removeInventoryItem('tabacsec', 1)
				xPlayer.addInventoryItem('luckystrike', 1)
			end
			Craft2(source)
		elseif _miam == 'camel' then
			if Tabac3Quantity < 1 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
				xPlayer.removeInventoryItem('tabacsec', 5)
				xPlayer.addInventoryItem('camel', 1)
			end
			Craft2(source)
		elseif _miam == 'jamnesia' then
			if Tabac3Quantity < 5  then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			elseif WeedQuantity < 2 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
			
				xPlayer.removeInventoryItem('tabacsec', 5)
				xPlayer.removeInventoryItem('weed_pooch', 5)
				xPlayer.addInventoryItem('jamnesia', 1)
			end
			Craft2(source)
		elseif _miam == 'jak47' then
			if Tabac3Quantity < 3 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			elseif WeedQuantity < 3 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
				xPlayer.removeInventoryItem('tabacsec', 3)
				xPlayer.removeInventoryItem('weed_pooch', 3)
				xPlayer.addInventoryItem('jak47', 1)
			end
			Craft2(source)
		elseif _miam == 'jwhitewidow' then
			if Tabac3Quantity < 2  then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			elseif WeedQuantity < 2 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
				xPlayer.removeInventoryItem('tabacsec', 2)
				xPlayer.removeInventoryItem('weed_pooch', 2)
				xPlayer.addInventoryItem('jwhitewidow', 1)
			end
			Craft2(source)
		elseif _miam == 'jafghan' then
			if Tabac3Quantity < 1  then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			elseif WeedQuantity < 1 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_ing'))
			else
				xPlayer.removeInventoryItem('tabacsec', 1)
				xPlayer.removeInventoryItem('weed_pooch', 1)
				xPlayer.addInventoryItem('jafghan', 1)
			end
			Craft2(source)
		end
    end
  end)
end

RegisterServerEvent('esx_coffeeshop:startCraft2')
AddEventHandler('esx_coffeeshop:startCraft2', function(miam)

    local _source = source
	local _miam = miam
    
    if PlayersCrafting3[_source] == false then
        TriggerClientEvent('esx:showNotification', _source, '~r~Pas bien de glitch!')
        PlayersCrafting3[_source] = false
        
    else
        PlayersCrafting3[_source] = true
        TriggerClientEvent('esx:showNotification', _source, _U('assemblage_caroule'))
        Craft2(_source, _miam)
    end
end)

RegisterServerEvent('esx_coffeeshop:stopCraft2')
AddEventHandler('esx_coffeeshop:stopCraft2', function()

    local _source = source
	local _miam = miam
    if PlayersCrafting3[_source] == true then
        PlayersCrafting3[_source] = false
        TriggerClientEvent('esx:showNotification', _source, 'Vous ~r~sortez ~w~de la ~g~zone...')
        Craft2(_source, _miam)
    else
        TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~rouler~w~ à présent!')        
        PlayersCrafting3[_source] = true
    end
end)

------------ Vente cigarette --------------
local function VenteB(source)

  SetTimeout(5000, function()

    if PlayersVente[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local MarlboQuantity  = xPlayer.getInventoryItem('marlbo').count
	  local societyAccount = nil
		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_coffeeshop', function(account)
			societyAccount = account
		end)
      if MarlboQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, _U('you_do_not_room'))
      else
            xPlayer.removeInventoryItem('marlbo', 1)
			xPlayer.addMoney(38)
			societyAccount.addMoney(37)

        VenteB(source)
      end
    end
  end)
end

RegisterServerEvent('esx_coffeeshop:startVente')
AddEventHandler('esx_coffeeshop:startVente', function()

    local _source = source
    
    if PlayersVente[_source] == false then
        TriggerClientEvent('esx:showNotification', _source, '~r~Pas bien de glitch!')
        PlayersVente[_source] = false
        
    else
        PlayersVente[_source] = true
        TriggerClientEvent('esx:showNotification', _source, _U('sell_vente'))
        VenteB(_source)
    end
end)

RegisterServerEvent('esx_coffeeshop:stopVente')
AddEventHandler('esx_coffeeshop:stopVente', function()

    local _source = source
    if PlayersVente[_source] == true then
        PlayersVente[_source] = false
        TriggerClientEvent('esx:showNotification', _source, 'Vous ~r~sortez ~w~de la ~g~zone...')
        VenteB(_source)
    else
        TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~vendre~w~ à présent!')        
        PlayersVente[_source] = true
    end
end)


----------------ACHAT PNJ---------------
RegisterServerEvent('esx_coffeeshop:pedBuyCig')
AddEventHandler('esx_coffeeshop:pedBuyCig', function()
  
  local _source       = source
  local xPlayer       = ESX.GetPlayerFromId(_source)
  local resellChances = {}
  local cigTypeMagic  = math.random(0, 100)
  local chosenCig     = nil
  local prices        = nil

  if highPrice then
    prices = Config.CigPricesHigh
  else
    prices = Config.CigPrices
  end

  for k,v in pairs(Config.CigResellChances) do
    table.insert(resellChances, {cig = k, chance = v})
  end

  table.sort(resellChances, function(a, b)
    return a.chance < b.chance
  end)

  local count = 0

  for i=1, #resellChances, 1 do
    
    count = count + resellChances[i].chance

    if cigTypeMagic <= count then
      chosenCig = resellChances[i].cig
      break
    end

  end

  local pricePerUnit = randomFloat(prices[chosenCig].min, prices[chosenCig].max)
  local quantity     = math.random(Config.CigResellQuantity[chosenCig].min, Config.CigResellQuantity[chosenCig].max)
  local item         = xPlayer.getInventoryItem(chosenCig)

  if item.count > 0 then

    if item.count < quantity then

      local price = math.floor(item.count * pricePerUnit)

      xPlayer.removeInventoryItem(chosenCig, item.count)
      xPlayer.addAccountMoney('black_money', price)
      
      TriggerClientEvent('esx:showNotification', _source, 'Vous avez gagné ~g~$' .. price .. '~s~ pour ~y~x' .. item.count .. ' ' .. item.label)
    else

      local price = math.floor(quantity * pricePerUnit)

      xPlayer.removeInventoryItem(chosenCig, quantity)
      xPlayer.addAccountMoney('black_money', price)

      TriggerClientEvent('esx:showNotification', _source, 'Vous avez gagné ~g~$' .. price .. '~s~ pour ~y~x' .. quantity .. ' ' .. item.label)
    end

  else
    TriggerClientEvent('esx:showNotification', _source, 'Vous n\'avez pas les cigarettes demandées [' .. item.label .. ']')
  end

end)

-- RegisterServerEvent('esx_coffeeshop:pedCallPolice')
-- AddEventHandler('esx_coffeeshop:pedCallPolice', function()
	
	-- local _source = source
	-- local xPlayer = ESX.GetPlayerFromId(_source)

	-- local xPlayers = ESX.GetPlayers()

	-- for i=1, #xPlayers, 1 do

		-- local xPlayer2 = ESX.GetPlayerFromId(xPlayers[i])
			
		-- if xPlayer2.job.name == 'police' then
			
			-- TriggerClientEvent('esx_phone:onMessage', xPlayer2.source, '', 'Une personne essaye de me vendre quelque chose de louche !', xPlayer.get('coords'), true, 'Alerte Moldu', false)
		-- end

	-- end

-- end)

ESX.RegisterServerCallback('esx_coffeeshop:tryRemoveInventoryItem', function(source, cb, itemName, itemCount)

  local xPlayer = ESX.GetPlayerFromId(source)
  local item    = xPlayer.getInventoryItem(itemName)

  if item.count >= itemCount then
    xPlayer.removeInventoryItem(itemName, itemCount)
    cb(true)
  else
    cb(false)
  end
end)

----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('marlbo', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')
	
	if lighter.count > 0 then
		xPlayer.removeInventoryItem('marlbo', 1)
		TriggerClientEvent('esx_coffeeshop:onSmokeCig', source)
		TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Marlboro','CHAR_PROPERTY_SONAR_COLLECTIONS','Tabac')
	else	
		TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end	
end)
----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('luckystrike', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')

	if lighter.count > 0 then
		xPlayer.removeInventoryItem('luckystrike', 1)
		TriggerClientEvent('esx_coffeeshop:onSmokeCig', source)
		TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Lucky Strike','CHAR_PROPERTY_SONAR_COLLECTIONS','Tabac')
	else	
		TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end	
end)

----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('camel', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')

	if lighter.count > 0 then
		xPlayer.removeInventoryItem('camel', 1)
		TriggerClientEvent('esx_coffeeshop:onSmokeCig', source)
		TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Cigare','CHAR_PROPERTY_SONAR_COLLECTIONS','Tabac')
	else
		TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end	

end)

----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('jamnesia', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')

	if lighter.count > 0 then
		xPlayer.removeInventoryItem('jamnesia', 1)
		TriggerClientEvent('esx_drugs:onPot', source)
		TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Joint d\'Amnésia','CHAR_PROPERTY_SONAR_COLLECTIONS','Weed Shop')
	else
		TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end	
end)

----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('jak47', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')

	if lighter.count > 0 then
		xPlayer.removeInventoryItem('jak47', 1)
		TriggerClientEvent('esx_drugs:onPot', source)
		TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Joint d\'AK-47','CHAR_PROPERTY_SONAR_COLLECTIONS','Weed Shop')
	else
		TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end			

end)

----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('jwhitewidow', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')

	if lighter.count > 0 then
		xPlayer.removeInventoryItem('jwhitewidow', 1)
		TriggerClientEvent('esx_drugs:onPot', source)
		TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Joint de FairyV','CHAR_PROPERTY_SONAR_COLLECTIONS','Weed Shop')
	else
		TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end		

end)

----------------------------
---- Utiliser Cigarette ----
----------------------------
ESX.RegisterUsableItem('jafghan', function(source)

    local xPlayer = ESX.GetPlayerFromId(source)
	local lighter = xPlayer.getInventoryItem('lighter')

	if lighter.count > 0 then
    xPlayer.removeInventoryItem('jafghan', 1)
    TriggerClientEvent('esx_drugs:onPot', source)
	TriggerClientEvent('esx_extended:showNotification',source,'Vous avez fumé ~g~1x ~b~Joint d\'Afghan','CHAR_PROPERTY_SONAR_COLLECTIONS','Weed Shop')
	else
	TriggerClientEvent('esx_extended:showNotification',source,'Vous n avez pas de ~ p ~ briquets','CHAR_BANK_BOL','Magasin')
	end		
end)

