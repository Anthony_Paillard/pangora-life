local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
ESX                           = nil
TimeDiff                      = 0
CurrentCig                    = nil
SpawnedObjects                = {}
SpawnedPeds                   = {}
local PlayerData              = {}
local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local LastPart                = nil
local LastData                = {}
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local isinservice             = false
local TargetCoords            = nil
local CurrentlyTowedVehicle   = nil
local PedBlacklist            = {}
local PedAttacking            = nil
local Blips                   = {}
local messageNotFinished      = false
local isInMarker              = false
local isInPublicMarker        = false
local hintIsShowed            = false
local hintToDisplay           = "no hint to display"
GUI.Time                      = 0



Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end
end)

function StartWalking(ped)
  Citizen.CreateThread(function()
    RequestAnimDict('move_m@generic_variations@walk')
    while not HasAnimDictLoaded('move_m@generic_variations@walk') do
      Citizen.Wait(0)
    end
    TaskPlayAnim(ped,  'move_m@generic_variations@walk',  'walk_a',  1.0,  -1.0,  -1,  0,  1,  false,  false,  false)
  end)
end

function IsGradeBoss()
    if PlayerData ~= nil then
        local IsGradeBoss = false
        if ((PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'viceboss') or (PlayerData.second_job.grade_name == 'boss' or PlayerData.second_job.grade_name == 'viceboss')) then
            IsGradeBoss = true
        end
        return IsGradeBoss
    end
end

function OpenTabacActionsMenu()

  local elements = {}
  
  if isinservice then
	table.insert(elements, {label = _U('civ_wear'), value = 'cloakroom2'})
	table.insert(elements, {label = _U('vehicle_list'), value = 'vehicle_list'})
	table.insert(elements,  {label = _U('deposit_stock'), value = 'put_stock'})
	table.insert(elements,  {label = _U('withdraw_stock'), value = 'get_stock'})
  else
	table.insert(elements, {label = _U('work_wear'), value = 'cloakroom'})
  end
  
  if isinservice and Config.EnablePlayerManagement and ((PlayerData.job ~= nil and PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'viceboss') or (PlayerData.second_job ~= nil and PlayerData.second_job.grade_name == 'boss' or PlayerData.second_job.grade_name == 'viceboss')) then
    table.insert(elements, {label = _U('boss_actions'), value = 'boss_actions'})
  end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'coffeeshop_actions',
    {
      title    = _U('coffeeshop'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'vehicle_list' then
	  --  TriggerServerEvent("player:serviceOn", "coffeeshop")
		TriggerEvent('esx_society:SpawnV','coffeeshop',Config.Zones.VehicleSpawnPoint.Pos,Config.Zones.VehicleSpawnPoint.Heading,Config.EnableSocietyOwnedVehicles)
      elseif data.current.value == 'cloakroom' then
		isinservice = true
		--TriggerServerEvent("player:serviceOn", "coffeeshop"
        TriggerEvent('esx_society:setClothes', "coffeeshop", "work_wear")
		menu.close()
		
	  elseif data.current.value == 'cloakroom2' then
		isinservice = false
      --TriggerServerEvent("player:serviceOff", "coffeeshop")	
        menu.close()
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

            TriggerEvent('skinchanger:loadSkin', skin)

        end)
      
	  elseif data.current.value == 'put_stock' then
        TriggerEvent('esx_society:OpenPutStocksMenu','coffeeshop',0)
      
	  elseif data.current.value == 'get_stock' then
        TriggerEvent('esx_society:OpenGetStocksMenu','coffeeshop',0)
      
	  elseif data.current.value == 'boss_actions' then
        TriggerEvent('esx_society:openBossMenu', 'coffeeshop', function(data, menu)
          menu.close()
        end)
      end

    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'coffeeshop_actions_menu'
      CurrentActionMsg  = _U('open_actions')
      CurrentActionData = {}
    end
  )
end

function OpenTabacHarvestMenu()

  if Config.EnablePlayerManagement and (PlayerData.job ~= nil or PlayerData.second_job ~= nil) then
    local elements = {
      {label = _U('tabac'), value = 'tabac'},
    }

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'coffeeshop_harvest',
      {
        title    = _U('harvest'),
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'tabac' then
          menu.close()
          TriggerServerEvent('esx_coffeeshop:startHarvest')
        end

      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'coffeeshop_harvest_menu'
        CurrentActionMsg  = _U('harvest_menu')
        CurrentActionData = {}
      end
    )
  else
    ESX.ShowNotification(_U('not_experienced_enough'))
  end

end

function OpenTabacCraftMenu()
  if Config.EnablePlayerManagement and (PlayerData.job ~= nil or PlayerData.second_job ~= nil) then

    local elements = {
      {label = _U('tabacsec'), value = 'tabacsec'},
    }

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'tabac_craft',
      {
        title    = _U('craft'),
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'tabacsec' then
          menu.close()
          TriggerServerEvent('esx_coffeeshop:startCraft')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'tabac_craft_menu'
        CurrentActionMsg  = _U('craft_menu')
        CurrentActionData = {}
      end
    )
  else
    ESX.ShowNotification(_U('not_experienced_enough'))
  end
end

function OpenTabacCraft2Menu()
  if Config.EnablePlayerManagement and (PlayerData.job ~= nil or PlayerData.second_job ~= nil) then

    local elements = {
      {label = _U('marlbo'), value = 'marlbo'},
      {label = _U('luckystrike'), value = 'luckystrike'},
      {label = _U('camel'),   value = 'camel'},
	  {label = _U('jamnesia'),   value = 'jamnesia'},
	  --{label = _U('jak47'),   value = 'jak47'},
	 -- {label = _U('jwhitewidow'),   value = 'jwhitewidow'},
	  {label = _U('jafghan'),   value = 'jafghan'}
    }

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'tabac_craft2',
      {
        title    = _U('craft'),
        elements = elements
      },
      function(data, menu)
	  
		    menu.close()
		    local miam = data.current.value
    ESX.UI.Menu.Open(
        'dialog', GetCurrentResourceName(), 'test',
        {
        title = "quantité ?"
        },
        function(data2, menu2)

          local qtty = tonumber(data2.value)

          if qtty == nil or qtty <=0 then
            ESX.ShowNotification(_U('invalid_amount'))
          else

            menu2.close()
            for i=1, qtty do

            TriggerServerEvent('esx_coffeeshop:startCraft2', miam)
            Citizen.Wait(4000)

           end 

          end

        end,
        function(data2, menu2)
        menu2.close()
        end
      )

		
		    

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'tabac_craft2_menu'
        CurrentActionMsg  = _U('craft_menu')
        CurrentActionData = {}
      end
    )
  else
    ESX.ShowNotification(_U('not_experienced_enough'))
  end
end

function OpenSocietyActionsMenu()

  local elements = {}

	table.insert(elements, {label = _U('billing'),    value = 'billing'})

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'unicorn_actions',
    {
      title    = _U('coffeeshop'),
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

		if data.current.value == 'billing' then
			OpenBillingMenu()
		end
     
    end,
    function(data, menu)

      menu.close()

    end
  )

end

function OpenBillingMenu()

  ESX.UI.Menu.Open(
    'dialog', GetCurrentResourceName(), 'billing',
    {
      title = _U('billing_amount')
    },
    function(data, menu)
    
      local amount = tonumber(data.value)
      local player, distance = ESX.Game.GetClosestPlayer()

      if player ~= -1 and distance <= 3.0 then

        menu.close()
        if amount == nil or amount < 0 then
            ESX.ShowNotification(_U('amount_invalid'))
        else
            TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_coffeeshop', _U('billing'), amount)
        end

      else
        ESX.ShowNotification(_U('no_players_nearby'))
      end

    end,
    function(data, menu)
        menu.close()
    end
  )
end
RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

RegisterNetEvent('esx:setSecondJob')
AddEventHandler('esx:setSecondJob', function(job)
  PlayerData.second_job = job
end)

AddEventHandler('esx_coffeeshop:hasEnteredMarker', function(zone)

  if zone == 'TabacActions' then
    CurrentAction     = 'coffeeshop_actions_menu'
    CurrentActionMsg  = _U('open_actions')
    CurrentActionData = {}
  elseif zone == 'Garage' then
    CurrentAction     = 'coffeeshop_harvest_menu'
    CurrentActionMsg  = _U('harvest_menu')
    CurrentActionData = {}
  elseif zone == 'Craft' then
    CurrentAction     = 'coffeeshop_craft_menu'
    CurrentActionMsg  = _U('craft_menu')
    CurrentActionData = {}
  elseif zone == 'Craft2' then
    CurrentAction     = 'coffeeshop_craft2_menu'
    CurrentActionMsg  = _U('craft_menu')
    CurrentActionData = {}
  elseif zone == 'Vente' then
    CurrentAction     = 'coffeeshop_vente_menu'
    CurrentActionMsg  = _U('vente_menu')
    CurrentActionData = {}
  elseif zone == 'VehicleDeleter' then

    local playerPed = GetPlayerPed(-1)

    if IsPedInAnyVehicle(playerPed,  false) then

      local vehicle = GetVehiclePedIsIn(playerPed,  false)

      CurrentAction     = 'delete_vehicle'
      CurrentActionMsg  = _U('veh_stored')
      CurrentActionData = {vehicle = vehicle}
    end
  end
  

end)

AddEventHandler('esx_coffeeshop:hasExitedMarker', function(zone)

  if zone == 'Craft' then
    TriggerServerEvent('esx_coffeeshop:stopCraft')
  elseif zone == 'Craft2' then
    TriggerServerEvent('esx_coffeeshop:stopCraft2')
  elseif zone == 'Garage' then
    TriggerServerEvent('esx_coffeeshop:stopHarvest')
    TriggerServerEvent('esx_coffeeshop:stopHarvest2')
  elseif zone == 'Vente' then
    TriggerServerEvent('esx_coffeeshop:stopVente')
  end

  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
end)


-- Create Blips
 Citizen.CreateThread(function()
   local blip = AddBlipForCoord(Config.Zones.VehicleDeleter.Pos.x, Config.Zones.VehicleDeleter.Pos.y, Config.Zones.VehicleDeleter.Pos.z)
   SetBlipSprite (blip, 79)
   SetBlipDisplay(blip, 4)
   SetBlipScale  (blip, 1.2)
   SetBlipColour (blip, 25)
   SetBlipAsShortRange(blip, true)
   BeginTextCommandSetBlipName("STRING")
   AddTextComponentString("Tabac")
   EndTextCommandSetBlipName(blip)
 end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    if (PlayerData.job ~= nil and PlayerData.job2 ~= nil) and ( PlayerData.job.name == 'coffeeshop'or PlayerData.job2.name == 'coffeeshop') then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
        end
      end
    end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(10)
    -- if ((PlayerData.job ~= nil and PlayerData.job.name == 'coffeeshop') or (PlayerData.second_job ~= nil and PlayerData.second_job.name == 'coffeeshop')) then
    if (PlayerData.job ~= nil and PlayerData.job2 ~= nil) and ( PlayerData.job.name == 'coffeeshop'or PlayerData.job2.name == 'coffeeshop') then
      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil
      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end
      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('esx_coffeeshop:hasEnteredMarker', currentZone)
      end
      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_coffeeshop:hasExitedMarker', LastZone)
		Citizen.Wait(5000)
      end
    end
  end
end)



-- Key Controls
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(10)

        if CurrentAction ~= nil then

          SetTextComponentFormat('STRING')
          AddTextComponentString(CurrentActionMsg)
          DisplayHelpTextFromStringLabel(0, 0, 1, -1)

          if IsControlJustReleased(0, 38) and (PlayerData.job ~= nil and PlayerData.second_job ~= nil) and ( PlayerData.job.name == 'coffeeshop'or PlayerData.second_job.name == 'coffeeshop') then

            if CurrentAction == 'coffeeshop_actions_menu' then
				OpenTabacActionsMenu()
			end
            

			if isinservice then
				if CurrentAction == 'coffeeshop_harvest_menu' then
					OpenTabacHarvestMenu()
				
				elseif CurrentAction == 'coffeeshop_craft_menu' then
					OpenTabacCraftMenu()
				
				elseif CurrentAction == 'coffeeshop_craft2_menu' then
					OpenTabacCraft2Menu()
				
				elseif CurrentAction == 'coffeeshop_vente_menu' then
					TriggerServerEvent('esx_coffeeshop:startVente')
				
				elseif CurrentAction == 'delete_vehicle' then
				  TriggerEvent('esx_society:vDelete')
				end
			end
		  end
		end	

        if isinservice and IsControlJustReleased(0, Keys['=']) and ((PlayerData.job ~= nil and PlayerData.job.name == 'coffeeshop') or (PlayerData.second_job ~= nil and PlayerData.second_job.name == 'coffeeshop')) then
            OpenSocietyActionsMenu()
        end

        if isinservice and IsControlJustReleased(0, Keys['DELETE']) and ((PlayerData.job ~= nil and PlayerData.job.name == 'coffeeshop') or (PlayerData.second_job ~= nil and PlayerData.second_job.name == 'coffeeshop')) then

          local playerPed = GetPlayerPed(-1)
          local coords    = GetEntityCoords(playerPed)
          
          local closestPed, closestDistance = ESX.Game.GetClosestPed({
            x = coords.x,
            y = coords.y,
            z = coords.z
          }, {playerPed})

          -- Fallback code
          if closestDistance == -1 then
            
            print('Using fallback code to find ped')

            local success, ped = GetClosestPed(coords.x,  coords.y,  coords.z,  5.0, 1, 0, 0, 0,  26)

            if DoesEntityExist(ped) then
              local pedCoords = GetEntityCoords(ped)
              closestPed      = ped
              closestDistance = GetDistanceBetweenCoords(coords.x,  coords.y,  coords.z,  pedCoords.x,  pedCoords.y,  pedCoords.z,  true)
            end

          end

          if closestPed ~= -1 and closestDistance <= 5.0 then

            if IsPedInAnyVehicle(closestPed,  false) then
              ESX.ShowNotification('Action ~r~impossible~s~, cette personne est en voiture')
            else

              local playerData    = ESX.GetPlayerData()
              local isBlacklisted = false

              for i=1, #PedBlacklist, 1 do
                if PedBlacklist[i] == closestPed then
                  isBlacklisted = true
                end
              end

              if isBlacklisted then
                ESX.ShowNotification('Vous avez déjà traité avec ce client')
              else

                table.insert(PedBlacklist, closestPed)

                local hasCig = {}

                for i=1, #playerData.inventory, 1 do
                  for j=1, #Config.Cig, 1 do
                    if playerData.inventory[i].name == Config.Cig[j] and playerData.inventory[i].count > 0 then
                      table.insert(hasCig,  Config.Cig[j])
                    end
                  end
                end

                if #hasCig > 0 then

                  local magic = GetRandomIntInRange(1, 100)

                  TaskStandStill(closestPed,  -1)
                  TaskLookAtEntity(closestPed,  playerPed,  -1,  2048,  3)

                  if magic <= 10 then

                    ESX.ShowNotification('Le client a violemment refusé vos produits !')

                    TaskStandStill(closestPed,  -1)

                    ESX.SetTimeout(5000, function()

                      StartWalking(closestPed)

                      ESX.SetTimeout(20000, function()

                        TaskStartScenarioInPlace(closestPed, 'WORLD_HUMAN_STAND_MOBILE', 0, true);
						local plyPos = GetEntityCoords(GetPlayerPed(-1), true)
						--TriggerServerEvent("call:makeCall", "police", {x=plyPos.x,y=plyPos.y,z=plyPos.z}, 'Une personne essaye de me vendre quelque chose de louche !')
                        --TriggerServerEvent('esx_coffeeshop:pedCallPolice')

                        ESX.SetTimeout(20000, function()
                          StartWalking(closestPed)
                        end)

                      end)

                    end)

                  elseif magic <= 30 then
                    
                    ESX.ShowNotification('Le client a refusé vos produits !')
                    StartWalking(closestPed)

                  elseif magic <= 50 then

                    ESX.ShowNotification('Le client a acheté vos produits plus chères !')

                    TriggerServerEvent('esx_coffeeshop:pedBuyCig', false)

                    ESX.SetTimeout(5000, function()
                      StartWalking(closestPed)
                    end)

                  elseif magic <= 90 then

                    PedAttacking = closestPed
                    
                    SetPedAlertness(closestPed,  3)
                    SetPedCombatAttributes(closestPed,  46,  true)

                    ESX.SetTimeout(120000, function()
                      PedAttacking = nil
                    end)

                  else
                    
                    TriggerServerEvent('esx_coffeeshop:pedBuyCig', true)
                    
                    ESX.ShowNotification('Le client a acheté vos produits au prix fort !!')

                    TaskStandStill(closestPed,  -1)

                    ESX.SetTimeout(5000, function()
                      StartWalking(closestPed)
                    end)
                  
                  end

                else
                  ESX.ShowNotification('Vous n\'avez plus rien à vendre')
                end

              end

            end

          else
            ESX.ShowNotification('Personne à proximité')
          end

        end

    end
end)

Citizen.CreateThread(function()
  while true do

    Citizen.Wait(10)

    if PedAttacking ~= nil then
      TaskCombatPed(PedAttacking,  GetPlayerPed(-1),  0,  16)
    end

  end
end)

----------------------------
---- UTILISER CIGARETTE ----
----------------------------
RegisterNetEvent('esx_coffeeshop:onSmokeCig')
AddEventHandler('esx_coffeeshop:onSmokeCig', function()
  TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_SMOKING", 0, 1)
  emotePlay = true
  Citizen.Wait(150000)
  ClearPedTasksImmediately(GetPlayerPed(-1))
  emotePlay = false
end)

function stopEmote()
  ClearPedTasks(GetPlayerPed(-1))
  emotePlay = false
end

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(10)
    if emotePlay then
      if IsControlJustPressed(1, 22) or IsControlJustPressed(1, 30) or IsControlJustPressed(1, 31) then
        stopEmote()
      end
    end
  end
end)

---------------------------------------------------------------------------------------------------------
--NB : gestion des menu
---------------------------------------------------------------------------------------------------------

RegisterNetEvent('NB:openMenuCoffeeShop')
AddEventHandler('NB:openMenuCoffeeShop', function()
  OpenMobileTabacActionsMenu()
end)


function dump(o, nb)
  if nb == nil then
    nb = 0
  end
   if type(o) == 'table' then
      local s = ''
      for i = 1, nb + 1, 1 do
        s = s .. "    "
      end
      s = '{\n'
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
          for i = 1, nb, 1 do
            s = s .. "    "
          end
         s = s .. '['..k..'] = ' .. dump(v, nb + 1) .. ',\n'
      end
      for i = 1, nb, 1 do
        s = s .. "    "
      end
      return s .. '}'
   else
      return tostring(o)
   end
end