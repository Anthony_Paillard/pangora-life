INSERT INTO `addon_account` (name, label, shared) VALUES 
	('society_coffeeshop','Coffee Shop',1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
	('society_coffeeshop','Coffee Shop',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES 
	('society_coffeeshop', 'Coffee Shop', 1)
;

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('coffeeshop', 'Coffee Shop', 1);

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('coffeeshop',0,'recrue','Récolteur',300,'{}','{}'),
  ('coffeeshop',1,'novice','Vendeur',350,'{}','{}'),
  ('coffeeshop',2,'viceboss','Gérant',400,'{}','{}'),
  ('coffeeshop',3,'boss','Patron',500,'{}','{}');

INSERT INTO `items` (name, label) VALUES
  ('tabac', 'Tabac'),
  ('tabacsec', 'Tabac Séché'),
  ('marlbo', 'Marlboro'),
  ('luckystrike', 'Lucky Strike'),
  ('camel', 'Camel'),
  ('jamnesia', 'Joint Amnesia'),
  ('jak47', 'Joint AK-47'),
  ('jwhitewidow', 'Joint White Widow'),
  ('jafghan', 'Joint Afghan');
