Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true
Config.EnableESXIdentity          = false
Config.Locale                     = 'fr'

Config.Cig = {
  'marlbo',
  'luckystrike',
  'camel',
  'jamnesia',
  'jak47',
  'jwhitewidow',
  'jafghan',
}

Config.CigResellChances = {
  marlbo = 50,
  luckystrike = 50,
  camel = 50,
  jamnesia = 50,
  jak47 = 50,
  jwhitewidow = 50,
  jafghan = 50,

}

Config.CigResellQuantity= {
  marlbo = {min = 1, max = 5},
  luckystrike = {min = 1, max = 5},
  camel = {min = 1, max = 5},
  jamnesia = {min = 1, max = 5},
  jak47 = {min = 1, max = 5},
  jwhitewidow = {min = 1, max = 5},
  jafghan = {min = 1, max = 5},
}

Config.CigPrices = {
  marlbo = {min = 75, max = 75},
  luckystrike = {min = 75, max = 75},
  camel = {min = 75, max = 75},
  jamnesia = {min = 75, max = 75},
  jak47 = {min = 75, max = 75},
  jwhitewidow = {min = 75, max = 75},
  jafghan = {min = 75, max = 75},
}

Config.CigPricesHigh = {
  marlbo = {min = 75, max = 75},
  luckystrike = {min = 75, max = 75},
  camel = {min = 75, max = 75},
  jamnesia = {min = 75, max = 75},
  jak47 = {min = 75, max = 75},
  jwhitewidow = {min = 75, max = 75},
  jafghan = {min = 75, max = 75},
}

Config.Time = {
    marlbo = 12 * 0,
    luckystrike = 12 * 0,
    camel = 12 * 0,
    jamnesia = 12 * 0,
    jak47 = 12 * 0,
    jwhitewidow = 12 * 0,
    jafghan = 12 * 0
}

Config.Zones = {
  
  TabacActions = {
    Pos   = {x = 2221.18, y = 5614.5859, z = 53.8690},
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 1, g = 223, b = 30 },
    Type  = 27,
  },

  Garage = {
    Pos   = {x = 2215.536, y = 5577.587, z = 52.833},
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 1, g = 223, b = 30 },
    Type  = 27,
  },

  Craft = {
    Pos   = { x = 1243.7514, y = 1868.7247, z = 78.1000 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 1, g = 223, b = 30 },
    Type  = 27,
  },

  Craft2 = {
    Pos   = {x = -1172.6076660156,y = -1576.1453857422,z = 3.3826694488525 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 1, g = 223, b = 30 },
    Type  = 27,
  },

  VehicleSpawnPoint = {
    Pos   = {x = 2209.373, y = 5615.556, z = 52.976},
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Type  = -1,
	Heading = 180,
  },

  VehicleDeleter = {
    Pos   = {x = 2195.918, y = 5605.011, z = 52.518},
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 1,
  },

  VehicleDelivery = {
    Pos   = { x = 188.0942, y = 300.5998, z = 105.4947 },
    Size  = { x = 20.0, y = 20.0, z = 3.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = -1,
  },
  
  Vente = {
    Pos   = { x = -1165.3656, y = -1566.8544, z = 3.4518 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 1, g = 223, b = 30 },
    Type  = 27,
  },
}