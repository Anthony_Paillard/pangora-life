Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 27
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.5 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = true
Config.EnableLicenses             = false
Config.EnableMoneyWash            = true
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.BallasStations = {

  Ballas = {

    Blip = {
      -- Pos     = { x = 104.568, y = -1941.990, z = 20.803 },
      Sprite  = 310,
      Display = 4,
      Scale   = 0.8,
      Colour  = 38,
    },

	  AuthorizedVehicles = {
		  { name = 'schafter5',  label = 'Véhicule Civil' },
		  { name = 'Akuma',    label = 'Moto' },
		  { name = 'Granger',   label = '4X4' },
		  { name = 'mule3',      label = 'Camion de Transport' },
	  },

    Cloakrooms = {
      --{ x = 144.57633972168, y = -2203.7377929688, z = 3.6880254745483},
    },

    Armories = {
      { x = 87.283, y = -1961.110, z = 20.117},
    },

    Vehicles = {
      {
        Spawner    = { x = 78.145, y = -1974.784, z = 19.747 },
        SpawnPoint = { x = 88.948, y = -1967.593, z = 19.747 },
        Heading    = 0.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 113.30500793457, y = -3109.3337402344, z = 5.0060696601868 },
        SpawnPoint = { x = 112.94457244873, y = -3102.5942382813, z = 5.0050659179688 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 87.244, y = -1969.511, z = 19.883 },
      
    },

    BossActions = {
      { x = 83.522, y = -1965.057, z = 20.117 },
    },

  },

}
