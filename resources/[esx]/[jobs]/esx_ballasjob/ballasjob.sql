INSERT INTO `addon_account` (`id`, `name`, `label`, `shared`) VALUES
(26, 'society_ballas', 'Ballas', 1);

INSERT INTO `addon_inventory` (`id`, `name`, `label`, `shared`) VALUES
(21, 'society_ballas', 'Ballas', 1);

INSERT INTO `datastore` (`id`, `name`, `label`, `shared`) VALUES
(22, 'society_ballas', 'Ballas', 1);

INSERT INTO `jobs` (`name`, `label`, `whitelisted`) VALUES
('ballas', 'Ballas', 1);

INSERT INTO `job_grades` (`id`, `job_name`, `grade`, `name`, `label`, `salary`, `skin_male`, `skin_female`) VALUES
(108, 'ballas', 0, 'Habitant', 'Habitant', 300, '{}', '{}'),
(109, 'ballas', 1, 'Recrue', 'Recrue', 350, '', ''),
(110, 'ballas', 2, 'Soldat', 'Soldat', 400, '{}', '{}'),
(111, 'ballas', 3, 'Sergent', 'Sergent', 650, '{}', '{}'),
(112, 'ballas', 4, 'Bras-Droit', 'Bras-Droit', 700, '{}', '{}'),
(113, 'ballas', 5, 'boss', 'OG', 850, '{}', '{}'),
(114, 'ballas', 6, 'boss', 'T.OG', 1000, '{}', '{}');

