Locales['fr'] = {

	-- Cloakroom
	['cloakroom']									 	= 'vestiaire du LSGC',
	['no_outfit']									 	= 'il n\'y a pas d\'uniforme à votre taille...',
	['citizen_wear']								= 'tenue civile',
	['work_wear']									 	= 'tenue de travail',
	['open_cloackroom']						 	= 'appuyez sur ~INPUT_CONTEXT~ pour accéder au ~h~~o~vestiaire~s~',
	['working_clothes_required']		= '~o~Vous n\'êtes pas autorisés à travailler sans votre tenue de travail !~s~',

	-- Vaults
	['vault']											 	= 'coffre',
	['get_object']									= 'prendre Objet',
	['put_object']									= 'déposer Objet',
	['society_stock']							 	= 'stock du LSGC',
	['inventory']									 	= 'inventaire',
	['quantity']										= 'quantité',
	['invalid_quantity']						= 'quantité invalide',
	['you_removed']								 	= 'vous avez retiré x',
	['you_added']									 	= 'vous avez ajouté x',
	['open_vault']									= 'appuyez sur ~INPUT_CONTEXT~ pour accéder au ~h~~o~coffre~s~',

	-- Vehicles
	['vehicle_menu']								= 'véhicules',
	['trailer_menu']								= 'remorques',
	['vehicle_out']								 	= '~o~Il y a déja un véhicule dehors ou un véhicule occupe le point de sortie~s~',
	['vehicle_spawner']						 	= 'appuyez sur ~INPUT_CONTEXT~ pour ~h~~o~sortir un véhicule~s~',
	['trailer_spawner']						 	= 'appuyez sur ~INPUT_CONTEXT~ pour ~h~~o~sortir une remorque~s~',
	['store_vehicle']							 	= 'appuyez sur ~INPUT_CONTEXT~ pour ~h~~o~ranger le véhicule~s~',
	['service_max']								 	= 'service complet : ',

	-- Billing Menu
	['billing']											= 'facture',
	['no_players_nearby']					 	= 'aucun joueur à proximité',
	['billing_amount']							= 'montant de la facture',
	['invalid_amount']							= 'montant invalide',
	['society_name_billing']				= 'LSPI',

	-- Boss Menu
	['open_bossmenu']							 	= 'appuyez sur ~INPUT_CONTEXT~ pour ouvrir la ~h~~o~gestion de l\'entreprise~s~',
	['boss_required']							 	= '~r~Vous n\'avez pas accès à la gestion de l\'entreprise...~s~',

	-- Station Config Menu
	['open_stationsconfig']				 	= 'appuyez sur ~INPUT_CONTEXT~ pour ouvrir la ~h~~o~gestion des stations~s~',
	['boss_station_required']			 	= '~r~Vous n\'avez pas accès à la gestion des stations...~s~',
	['name_station']								= 'nom de la station',
	['price_station']							 	= 'prix',
	['quantity_station']						= 'quantité',
	['maximum_station']						 	= 'maximum',
	['change_price_station']				= 'changer le prix',
	['price_station_info']					= '%s $/L',
	['quantity_station_info']			 	= '%s L',
	['maximum_station_info']				= '%s L',
	['change_price']								= 'modifier',

	-- Harvesting
	['menu_harvest']								= 'appuyez sur ~INPUT_CONTEXT~ pour ~h~~o~débuter la récupération~s~',
	['menu_harvest_title']					= 'récupération de %s',
	['you_do_not_room']						 	= '~r~Vous n\'avez plus de place dans votre inventaire~s~',
	['no_harvesting_yet']					 	= '~r~Vous ne pouvez pas récupérer maintenant~s~',
	['harvesting_allowed']					= '~g~Vous pouvez à nouveau récupérer convenablement~s~',
	['leaving_area']								= '~o~Vous venez de quitter la zone de récupération~s~',
	['harvesting']									= 'récupération de ~o~%s~s~ en cours...',

	-- Selling
	['menu_refill']								 	= 'appuyez sur ~INPUT_CONTEXT~ pour ~h~~o~remplir la station~s~',
	['refill_station']							= "remplir la station essence [<span style='color:orange';>%s</span>/<span style='color:blue';>%s</span>L]",

	-- Misc
	['map_blip']										= 'local du LSPI',
	['society']										 	= 'LSPI',
	['society_harvest']						 	= 'raffinerie',
	['no_farming_in_vehicle']			 	= 'il est difficile de faire quelque chose en restant dans son ~r~véhicule~w~ !',

	-- Items
	['gasoline']										= 'bidon d\'essence'

}
