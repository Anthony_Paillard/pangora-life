Locales['en'] = {

	-- Cloakroom
	['cloakroom']									 	= 'LSGC\'s cloakroom',
	['no_outfit']									 	= 'no clothes suit you...',
	['citizen_wear']								= 'citizen clothes',
	['work_wear']									 	= 'working clothes',
	['open_cloackroom']						 	= 'press ~INPUT_CONTEXT~ to open the ~h~~o~cloakroom~s~',
	['working_clothes_required']		= '~o~you are not allowed to work without your working clothes~s~',

	-- Vaults
	['vault']											 	= 'vault',
	['get_object']									= 'withdraw object',
	['put_object']									= 'deposit object',
	['society_stock']							 	= 'LSGC\'s stock',
	['inventory']									 	= 'inventory',
	['quantity']										= 'amount',
	['invalid_quantity']						= 'invalid quantity',
	['you_removed']								 	= 'you just withdraw x',
	['you_added']									 	= 'you just add x',
	['open_vault']									= 'press ~INPUT_CONTEXT~ to access the ~h~~o~vault~s~',

	-- Vehicles
	['vehicle_menu']								= 'vehicles',
	['trailer_menu']								= 'trailers',
	['vehicle_out']								 	= '~o~There is already a vehicle outside or a vehicle occupies the exit point~s~',
	['vehicle_spawner']						 	= 'press ~INPUT_CONTEXT~ to ~h~~o~take a vehicle~s~',
	['trailer_spawner']						 	= 'press ~INPUT_CONTEXT~ to ~h~~o~take a vehicle~s~',
	['store_vehicle']							 	= 'press ~INPUT_CONTEXT~ to ~h~~o~park the vehicle~s~',
	['service_max']								 	= 'full duty : ',

	-- Billing Menu
	['billing']											= 'billing',
	['no_players_nearby']					 	= 'no player nearby',
	['billing_amount']							= 'billing amount',
	['invalid_amount']							= 'invalid amount',
	['society_name_billing']				= 'LSGC',

	-- Boss Menu
	['open_bossmenu']							 	= 'press ~INPUT_CONTEXT~ to access the ~h~~o~company management~s~',
	['boss_required']							 	= '~r~You don\'t have the access to the company management...~s~',

	-- Station Config Menu
	['open_stationsconfig']				 	= 'press ~INPUT_CONTEXT~ to open the ~h~~o~gas stations management~s~',
	['boss_station_required']			 	= '~r~You don\'t have the access to the gas stations management...~s~',
	['name_station']								= 'name of the gas station',
	['price_station']							 	= 'price',
	['quantity_station']						= 'quantity',
	['maximum_station']						 	= 'maximum',
	['change_price_station']				= 'modify the price',
	['price_station_info']					= '%s $/L',
	['quantity_station_info']			 	= '%s L',
	['maximum_station_info']				= '%s L',
	['change_price']								= 'modify',

	-- Harvesting
	['menu_harvest']								= 'press ~INPUT_CONTEXT~ to ~h~~o~begin the harvesting~s~',
	['menu_harvest_title']					= 'harvestsing of %s',
	['you_do_not_room']						 	= '~r~You don\'t have enough place in your inventory~s~',
	['no_harvesting_yet']					 	= '~r~You can\'t harvest now~s~',
	['harvesting_allowed']					= '~g~You can harvest again~s~',
	['leaving_area']								= '~o~You just left the harvesting area~s~',
	['harvesting']									= 'harvesting of ~o~%s~s~ in progress...',

	-- Selling
	['menu_refill']								 	= 'press ~INPUT_CONTEXT~ to ~h~~o~refill the gas station~s~',
	['refill_station']							= "refilling of the gas station [<span style='color:orange';>%s</span>/<span style='color:blue';>%s</span>L]",

	-- Misc
	['map_blip']										= 'LSGC\'s local',
	['society']										 	= 'LSGC',
	['society_harvest']						 	= 'refinery',
	['no_farming_in_vehicle']			 	= 'it\'s hard to do anything by staying in a ~r~vehicle~w~!',

	-- Items
	['gasoline']										= 'gasoline can'

}
