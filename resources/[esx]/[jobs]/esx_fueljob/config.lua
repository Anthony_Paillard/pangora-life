Config														= {}
Config.Locale											= 'fr'
Config.DrawDistance								= 25.0

Config.EnablePlayerManagement			= true
Config.EnableSocietyOwnedVehicles	= true
Config.EnableVaultManagement			= true
Config.EnableMoneyWash						= false
Config.MaxInService								= -1

Config.PlatePrefix = "FUEL"
Config.AuthorizedVehicles = {
	{ name = 'tiptruck2',	 label = 'Camion transporteur' }
}
--Config.PlatePrefixTrailer = "FUET"
--Config.AuthorizedTrailers = {
	--{ name = 'baletrailer',		label = 'Remorque citerne' }
--}

Config.Blips = {

	Blip = {
		Pos		 	= { x = 494.913, y = -2183.17, z = 4.918 },
		Sprite	= 436,
		Display	= 4,
		Scale	 	= 1.0,
		Color	 	= 47
	}

}

Config.JobBlips = {

	Recolte = {
		Pos		 	= { x = 2738.356, y = 1652.814, z = 23.554 },
		Sprite	= 436,
		Display = 4,
		Scale	 	= 1.1,
		Color	 	= 47,
		Name		= _U('society_harvest')
	},

}

Config.Zones = {

	Vaults = {
		Pos	 	= { x = 489.151, y = -2182.274, z = 4.938 },
		Size	= { x = 1.3, y = 1.3, z = 1.0 },
		Color = { r = 220, g = 110, b = 0 },
		Type	= 23
	},

	BossActions = {
		Pos	 	= { x = 498.652, y = -2187.26, z = 4.948 },
		Size	= { x = 1.5, y = 1.5, z = 1.0 },
		Color = { r = 220, g = 110, b = 0 },
		Type	= 27
	},

	StationsConfig = {
		Pos	 	= { x = 496.966, y = -2190.25, z = 4.948 },
		Size	= { x = 1.5, y = 1.5, z = 1.0 },
		Color = { r = 220, g = 110, b = 0 },
		Type	= 27
	},
}

Config.StationsMarker = {
	Type 	= 1,
	Size	= { x = 3.0, y = 3.0, z = 0.8 },
	Color = { r = 220, g = 110, b = 0 }
}
Config.Stations = {

	{ x = 63.794,		 	y = 2783.171,	 	z = 56.878,	 	name = 'XERO Route 68' },
	{ x = 253.07,		 	y = 2599.383,	 	z = 43.85,		name = 'GLOBE OIL Route 68 #1' },
	{ x = 1031.234,	 	y = 2661.918,	 	z = 38.551,	 	name = 'GLOBE OIL Route 68 #2' },
	{ x = 1210.215,	 	y = 2644.472,	 	z = 36.819,	 	name = 'GLOBE OIL Route 68 #3' },
	{ x = 2538.717,	 	y = 2602.668,	 	z = 36.945,	 	name = 'GLOBE OIL Señora Way'},
	{ x = 2689.496,	 	y = 3254.722,	 	z = 54.368,	 	name = 'XERO Señora Freeway' },
	{ x = 1987.074,	 	y = 3776.968,	 	z = 31.181,	 	name = 'XERO Marina Drive' },
	{ x = 1692.712,	 	y = 4917.789,	 	z = 41.078,		name = 'LTD Grapeseed Main Street' },
	{ x = 1687.942,	 	y = 6436.665,	 	z = 31.491,	 	name = 'GLOBE OIL Great Ocean Highway #1' },
	{ x = 202.552,		y = 6603.158,	 	z = 30.647,	 	name = 'RON Great Ocean Highway #1' },
	{ x = -100.066,	 	y = 6399.516,	 	z = 30.454,	 	name = 'XERO Paleto Boulevard' },
	{ x = -2567.368,	y = 2346.994,	 	z = 32.076,	 	name = 'RON Route 68' },
 	{ x = -1820.114,	y = 806.095,		z = 137.7,		name = 'LTD Rockford Drive North' },
 	{ x = -1413.113,	y = -281.347,	 	z = 45.338,	 	name = 'LTD Rockford Drive South' },
 	{ x = -2111.768,	y = -331.704,	 	z = 12.021,	 	name = 'XERO Del Perro Freeway' },
 	{ x = -704.146,	 	y = -923.779,	 	z = 18.014,	 	name = 'LTD Lindsay Circus' },
 	{ x = -510.55,		y = -1214.025,	z = 17.484,	 	name = 'XERO Calais Avenue' },
 	{ x = -58.452,		y = -1775.04,	 	z = 28.046,	 	name = 'LTD Grove Street' },
	{ x = 284.767,		y = -1275.621,	z = 28.269,		name = 'XERO Strawberry Avenue' },
 	{ x = 831.645,		y = -1038.12,	 	z = 25.897,	 	name = 'RON Popular Street' },
	{ x = 1199.931,	 	y = -1384.724,	z = 34.227,	 	name = 'RON Capital Boulevard' },
	{ x = 1178.367,	 	y = -310.072,	 	z = 68.2,		 	name = 'LTD Mirror Park Boulevard' },
	{ x = 635.369,		y = 251.629,		z = 102.152,	name = 'GLOBE OIL Clinton Avenue' },
	{ x = 2554.784,	 	y = 343.579,		z = 107.467,	name = 'RON Palomino Freeway' },
	{ x = -340.973,	 	y = -1463.853,	z = 29.611,	 	name = 'GLOBE OIL Alta Street' },
	{ x = 1779.654,	 	y = 3320.066,	 	z = 40.412,	 	name = 'GLOBE OIL Panorama Drive' }

}

Config.Uniforms = {

	work_wear = {
		male = {
			['tshirt_1']	= 15, ['tshirt_2']	= 0,
			['torso_1']	 	= 42, ['torso_2']	 	= 0,
			['decals_1']	= 0,	['decals_2']	= 0,
			['arms']			= 11,
			['pants_1']	 	= 47, ['pants_2']	 	= 1,
			['shoes_1']	 	= 25, ['shoes_2']	 	= 0,
			['helmet_1']	= -1, ['helmet_2']	= 0,
			['chain_1']	 	= 0,	['chain_2']	 	= 0,
			['ears_1']		= -1, ['ears_2']		= 0
		},
		female = {
			['tshirt_1']	= 3,	['tshirt_2']	= 0,
			['torso_1']	 	= 86, ['torso_2']	 	= 0,
			['decals_1']	= 0,	['decals_2']	= 0,
			['arms']			= 9,
			['pants_1']	 	= 49, ['pants_2']	 	= 1,
			['shoes_1']	 	= 25, ['shoes_2']	 	= 0,
			['helmet_1']	= -1, ['helmet_2']	= 0,
			['chain_1']	 	= 0,	['chain_2']	 	= 0,
			['ears_1']		= -1, ['ears_2']		= 0
		}
}

}
