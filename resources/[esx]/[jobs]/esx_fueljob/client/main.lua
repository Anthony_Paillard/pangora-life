local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData							= {}
local HasAlreadyEnteredMarker	= false
local LastZone								= nil
local CurrentAction						= nil
local CurrentActionMsg				= ''
local CurrentActionData				= {}
local JobBlips								= {}
local isInWorkingClothes			= false
local isWorkingWithTrailer		= false

ESX														= nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
end)

function IsGradeBoss()
	if PlayerData ~= nil then
		local IsGradeBoss = false
		if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'viceboss' then
			IsGradeBoss = true
		end
		return IsGradeBoss
	end
end

function OpenVaultMenu()

	if Config.EnableVaultManagement then

		local elements = {
			{label = _U('get_object'), value = 'get_stock'},
			{label = _U('put_object'), value = 'put_stock'}
		}

		ESX.UI.Menu.CloseAll()

		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'vault',
			{
				title		= _U('vault'),
				align		= 'top-left',
				elements = elements,
			},
			function(data, menu)

				local action = data.current.value
				if action == 'put_stock' then
					TriggerEvent('esx_society:OpenPutStocksMenu',"fuel",0)
				elseif action == 'get_stock' then
					TriggerEvent('esx_society:OpenGetStocksMenu','fuel',0)
				end

			end,

			function(data, menu)

				menu.close()

				CurrentAction			= 'menu_vault'
				CurrentActionMsg	= _U('open_vault')
				CurrentActionData = {}
			end
		)

	end

end

function OpenStationsInfosMenu()

	ESX.TriggerServerCallback('LegacyFuel:requestStationsInfos', function(stationsInfos)

		local elements = {
			head = { _U('name_station'), _U('price_station'), _U('quantity_station'), _U('maximum_station') },
			rows = {}
		}

		for k,v in pairs(stationsInfos) do
			table.insert(elements.rows, {
				data = k,
				cols = {
					k,
					_U('price_station_info', v.price),
					_U('quantity_station_info', v.quantity),
					_U('maximum_station_info', v.maximum)
				}
			})
		end

		ESX.UI.Menu.CloseAll()
		ESX.UI.Menu.Open(
			'list', GetCurrentResourceName(), 'stations_infos',
			elements,
			function(data, menu)
			end,
			function(data, menu)
				menu.close()
				CurrentAction			= 'menu_stationsconfig'
				CurrentActionMsg	= _U('open_stationsconfig')
				CurrentActionData = {}
			end
		)

	end)

end

function OpenStationsConfigMenu()

	ESX.TriggerServerCallback('LegacyFuel:requestStationsInfos', function(stationsInfos)

		local elements = {
			head = { _U('name_station'), _U('price_station'), _U('quantity_station'), _U('maximum_station'), _U('change_price_station') },
			rows = {}
		}

		for k,v in pairs(stationsInfos) do
			table.insert(elements.rows, {
				data = k,
				cols = {
					k,
					_U('price_station_info', v.price),
					_U('quantity_station_info', v.quantity),
					_U('maximum_station_info', v.maximum),
					'{{' .. _U('change_price') .. '|change_price}}'
				}
			})
		end

		ESX.UI.Menu.CloseAll()
		ESX.UI.Menu.Open(
			'list', GetCurrentResourceName(), 'stations_infos',
			elements,
			function(data, menu)
				local name	 = data.data
				menu.close()
				OpenChangePriceMenu(name)
			end,
			function(data, menu)
				menu.close()
				CurrentAction			= 'menu_stationsconfig'
				CurrentActionMsg	= _U('open_stationsconfig')
				CurrentActionData = {}
			end
		)

	end)

end

function OpenChangePriceMenu(stationName)
	local price = ''
	DisplayOnscreenKeyboard(1, "FMMC_KEY_TIP", "", "", "", "", "", 10)
	while (UpdateOnscreenKeyboard() == 0) do
		DisableAllControlActions(0);
		Wait(0);
	end
	if (GetOnscreenKeyboardResult()) then
		price = tonumber(GetOnscreenKeyboardResult())
	end
	if price == '' or price == nil or price < 0 then
		ESX.ShowNotification(_U('invalid_amount'))
	else
		TriggerServerEvent('LegacyFuel:changeStationPrice', stationName, price)
	end
	CurrentAction			= 'menu_stationsconfig'
	CurrentActionMsg	= _U('open_stationsconfig')
	CurrentActionData = {}
end

function OpenSocietyActionsMenu()

	local elements = {
		{label = _U('billing'),		value = 'billing'}
	}

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'society_actions',
		{
			title			= _U('society'),
			align			= 'top-left',
			elements 	= elements
		},
		function(data, menu)
			local action = data.current.value
			if action == 'billing' then
				OpenBillingMenu()
			end
		end,
		function(data, menu)
			menu.close()
		end
	)

end

function OpenBillingMenu()

	ESX.UI.Menu.Open(
		'dialog', GetCurrentResourceName(), 'billing',
		{
			title = _U('billing_amount')
		},
		function(data, menu)

			local amount = tonumber(data.value)
			local player, distance = ESX.Game.GetClosestPlayer()

			if player ~= -1 and distance <= 3.0 then

				menu.close()
				if amount == nil or amount < 0 then
					ESX.ShowNotification(_U('invalid_amount'))
				else
					TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_fuel', _U('society_name_billing'), amount)
				end

			else
				ESX.ShowNotification(_U('no_players_nearby'))
			end

		end,
		function(data, menu)
			menu.close()
		end
	)
end

function OpenRefillMenu(station)
	ESX.TriggerServerCallback('LegacyFuel:requestStationsInfos', function(stationInfos)

		local stationQuantity = stationInfos[station].quantity
		local stationMaximum	= stationInfos[station].maximum
		local elements = {
			{ label =	_U('refill_station', stationQuantity, stationMaximum), value = station }
		}

		ESX.UI.Menu.CloseAll()
		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'menu_refill',
			{
				title		= station,
				elements = elements
			},
			function(data, menu)
				local action = data.current.value
				menu.close()
				TriggerServerEvent('LegacyFuel:refillStation', action)
				CurrentAction			= 'menu_refill'
				CurrentActionMsg	= _U('menu_refill')
				CurrentActionData = {station = station}
			end,
			function(data, menu)
				menu.close()
				CurrentAction			= 'menu_refill'
				CurrentActionMsg	= _U('menu_refill')
				CurrentActionData = {station = station}
			end
		)

	end)
end

AddEventHandler('esx_fueljob:hasEnteredMarker', function(zone, station)

	if zone == 'BossActions' then
		CurrentAction			= 'menu_boss_actions'
		CurrentActionMsg	= _U('open_bossmenu')
		CurrentActionData = {}
	elseif zone == 'StationsConfig' then
		CurrentAction			= 'menu_stationsconfig'
		CurrentActionMsg	= _U('open_stationsconfig')
		CurrentActionData = {}
	elseif zone == 'Vaults' and Config.EnableVaultManagement then
		CurrentAction			= 'menu_vault'
		CurrentActionMsg	= _U('open_vault')
		CurrentActionData = {}
	elseif zone == 'Station' then
		CurrentAction			= 'menu_refill'
		CurrentActionMsg	= _U('menu_refill')
		CurrentActionData = {station = station}
	end
end)

AddEventHandler('esx_fueljob:hasExitedMarker', function(zone)
	CurrentAction = nil
	ESX.UI.Menu.CloseAll()
end)

-- Display markers
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(10)
		local coords 	= GetEntityCoords(PlayerPedId())
		local isFound = false

		for k,v in pairs(Config.Zones) do
			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, false, 2, false, false, false, false)
				isFound = true
			end
		end

		for _,v in pairs(Config.Stations) do
			if(GetDistanceBetweenCoords(coords, v.x, v.y, v.z, true) < Config.DrawDistance) then
				DrawMarker(Config.StationsMarker.Type, v.x, v.y, v.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.StationsMarker.Size.x, Config.StationsMarker.Size.y, Config.StationsMarker.Size.z, Config.StationsMarker.Color.r, Config.StationsMarker.Color.g, Config.StationsMarker.Color.b, 100, false, false, 2, false, false, false, false)
				isFound = true
			end
		end

		if not isFound then
			Citizen.Wait(1000)
		end

	end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(5)

		local coords			= GetEntityCoords(PlayerPedId())
		local isInMarker	= false
		local currentZone, currentStation

		for k,v in pairs(Config.Zones) do
			if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
				isInMarker	= true
				currentZone = k
			end
		end

		for _,v in pairs(Config.Stations) do
			if(GetDistanceBetweenCoords(coords, v.x, v.y, v.z, true) < Config.StationsMarker.Size.x) then
				isInMarker			= true
				currentZone		 	= 'Station'
				currentStation	= v.name
			end
		end

		if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
			HasAlreadyEnteredMarker = true
			LastZone								= currentZone
			TriggerEvent('esx_fueljob:hasEnteredMarker', currentZone, currentStation)
		end

		if not isInMarker and HasAlreadyEnteredMarker then
			HasAlreadyEnteredMarker = false
			TriggerEvent('esx_fueljob:hasExitedMarker', LastZone)
		end

		if not isInMarker then
			Citizen.Wait(500)
		end
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(10)

		if CurrentAction == nil then
			Citizen.Wait(250)
		else

			ESX.ShowHelpNotification(CurrentActionMsg)

			if IsControlJustReleased(1,	Keys['E']) and GetLastInputMethod(2) then

				if CurrentAction == 'menu_vault' then
					OpenVaultMenu()

				elseif CurrentAction == 'menu_boss_actions' then

					if not IsGradeBoss() then
						ESX.ShowNotification(_U('boss_required'))
					else
						local options = {
							wash			= Config.EnableMoneyWash,
						}

						ESX.UI.Menu.CloseAll()

						TriggerEvent('esx_society:openBossMenu', 'fuel', function(data, menu)

							menu.close()
							CurrentAction			= 'menu_boss_actions'
							CurrentActionMsg	= _U('open_bossmenu')
							CurrentActionData = {}

						end,options)
					end

				elseif CurrentAction == 'menu_stationsconfig' then

					if not IsGradeBoss() then
						OpenStationsInfosMenu()
					else
						OpenStationsConfigMenu()
					end
				elseif CurrentAction == 'menu_refill' then
					if not IsPedOnFoot(PlayerPedId()) then
						ESX.ShowNotification(_U('no_farming_in_vehicle'))
					else
						OpenRefillMenu(CurrentActionData.station)
					end
				end

				CurrentAction = nil

			end

		end

		--[[
		if IsControlJustReleased(0,	Keys['F6']) and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'fuel_actions') then
				OpenSocietyActionsMenu()
		end
		]]--

	end
end)


---------------------------------------------------------------------------------------------------------
--NB : gestion des menu
---------------------------------------------------------------------------------------------------------

RegisterNetEvent('nb_menuperso:openMenuFuel')
AddEventHandler('nb_menuperso:openMenuFuel', function()
	OpenSocietyActionsMenu()
end)


function dump(o, nb)
  if nb == nil then
    nb = 0
  end
   if type(o) == 'table' then
      local s = ''
      for i = 1, nb + 1, 1 do
        s = s .. "    "
      end
      s = '{\n'
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
          for i = 1, nb, 1 do
            s = s .. "    "
          end
         s = s .. '['..k..'] = ' .. dump(v, nb + 1) .. ',\n'
      end
      for i = 1, nb, 1 do
        s = s .. "    "
      end
      return s .. '}'
   else
      return tostring(o)
   end
end