SET @job_name = 'fuel';
SET @society_name = 'society_fuel';
SET @job_Name_Caps = 'LSGC';

DELETE FROM items WHERE name = 'gasoline';
DELETE FROM addon_inventory_items WHERE name NOT IN (SELECT name FROM items);
DELETE FROM jobs WHERE name = 'fuel';
DELETE FROM job_grades WHERE job_name = 'fuel';
UPDATE users
SET job = 'unemployed', job_grade = 0
WHERE job = 'fuel';

INSERT INTO `addon_account` (name, label, shared) VALUES
	(@society_name, @job_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
	(@society_name, @job_Name_Caps, 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES
	(@society_name, @job_Name_Caps, 1)
;

INSERT INTO `jobs` (name, label, whitelisted) VALUES
	(@job_name, @job_Name_Caps, 0)
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
	(@job_name, 0, 'interim', 'Intérimaire', 200, '{}', '{}'),
	(@job_name, 1, 'fuel', 'Employé', 300, '{}', '{}'),
	(@job_name, 2, 'cheffuel', 'Employé Expérimenté', 350, '{}', '{}'),
	(@job_name, 3, 'viceboss', 'Associé', 400, '{}', '{}'),
	(@job_name, 4, 'boss', 'Patron', 500, '{}', '{}')
;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES
	('gasoline', "Bidon d'essence", 20)
;
