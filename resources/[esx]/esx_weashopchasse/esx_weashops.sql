USE `essentialmode`;

CREATE TABLE `weashopchasse` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,

  PRIMARY KEY (`id`)
);

INSERT INTO `licenses` (type, label) VALUES
  ('weapon2', "Permis de chasse")
;

INSERT INTO `weashopschasse` (name, item, price) VALUES
	('GunShop', 'WEAPON_SMOKEGRENADE', 100)
;
