Locales ['fr'] = {

  ['buy_license'] = 'Acheter un permis de chasse ?',
  ['yes'] = 'Oui',
  ['no'] = 'Non',
  ['buy'] = 'Vous avez acheté ~b~1x ',
  ['not_enough_black'] = '~r~Vous n\'avez pas assez d\'argent sale',
  ['not_enough'] = '~r~Vous n\'avez pas assez d\'argent',
  ['shop'] = 'Armurerie Chasse',
  ['shop_menu'] = 'Appuyez sur ~INPUT_CONTEXT~ pour ~b~accéder au magasin de chasse',
  ['map_blip'] = 'armurerie',

}
