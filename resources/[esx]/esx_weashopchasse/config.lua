Config                			= {}
Config.DrawDistance   			= 100
Config.Size           			= { x = 1.5, y = 1.5, z = 1.5 }
Config.Color          			= { r = 256, g = 0, b = 0 }
Config.Type           			= 27
Config.Locale         			= 'fr'
Config.EnableLicense  			= true
Config.LicensePrice   			= 1500

Config.Zones = {

    GunShop = {
        legal = 0,
        Items = {},
        Pos   = {
            { x = -3041.401,   y = 585.365,   z = 6.908 },
           -- { x = 810.25,     y = -2157.60,   z = 28.62 },
           -- { x = 1693.44,    y = 3760.16,    z = 33.71 },
           -- { x = -330.24,    y = 6083.88,    z = 30.45 },
          --  { x = 252.63,     y = -50.00,     z = 68.94 },
          --  { x = 22.09,      y = -1107.28,   z = 28.80 },
          --  { x = 2567.69,    y = 294.38,     z = 107.73 },
          --  { x = -1117.58,   y = 2698.61,    z = 17.55 },
        --    { x = 842.44,     y = -1033.42,   z = 27.19 },

        }
    },

  --  BlackWeashop = {
   --     legal = 1,
    --    Items = {},
    --    Pos   = {
   --         { x = 369.763,   y = 2977.728,  z = 39.833},
  --      }
--    },

}
