local Vehicles								= {}
local pumpLoc									= {}
local StationsInfos						= {}
local StationsBlips						= {}
local nearPump								= false
local currentStation					= nil
local IsFueling								= false
local IsFuelingWithJerryCan		= false
local InBlacklistedVehicle		= false
local NearVehicleWithJerryCan	= false
local	isEngineRunning					= false
local downFuel								= 0
local price										= 0
local cash 										= 0

ESX	= nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()

	Citizen.Wait(2000)

	ESX.TriggerServerCallback('LegacyFuel:requestStationsInfos', function(cb)
		StationsInfos = cb
	end)

	Citizen.Wait(500)

	DeleteBlips()
	Citizen.Wait(500)
	if PlayerData.job ~= nil and PlayerData.job.name == 'fuel' then
		CreateJobBlips()
	else
		CreateBlips()
	end
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
	PlayerData.job = job
	DeleteBlips()
	Citizen.Wait(500)
	if PlayerData.job ~= nil and PlayerData.job.name == 'fuel' then
		CreateJobBlips()
	else
		CreateBlips()
	end
end)

RegisterNetEvent('esx_AdvancedFuel:setDisplayFuel')
AddEventHandler('esx_AdvancedFuel:setDisplayFuel', function(trigger)
	viewMode = trigger
end)

RegisterNetEvent('LegacyFuel:newStationQuantityToPlayers')
AddEventHandler('LegacyFuel:newStationQuantityToPlayers', function(station, newQuantity)
	StationsInfos[station].quantity = newQuantity

	if PlayerData.job ~= nil and PlayerData.job.name == 'fuel' then
		Citizen.Wait(500)
		DeleteBlips()
		Citizen.Wait(500)
		CreateJobBlips()
	end
end)

-- Never turn engine off automaticaly
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)

		local ped = PlayerPedId()

		if DoesEntityExist(ped) and IsPedInAnyVehicle(ped, false) and IsControlJustPressed(2, 75) and not IsEntityDead(ped) and not IsPauseMenuActive() then
			local veh = GetVehiclePedIsIn(ped, false)
			local engineWasRunning = GetIsVehicleEngineRunning(veh)
			if engineWasRunning then
				repeat
					Citizen.Wait(100)
					local isStillInside = IsPedInAnyVehicle(ped, false)
				until isStillInside == false
				SetVehicleEngineOn(veh, true, true, true)
			end
		end

	end
end)

function CreateBlips()
	Citizen.CreateThread(function()
		for _, v in ipairs(Config.Stations) do
			local blip = AddBlipForCoord(v.x, v.y, v.z)

			SetBlipSprite(blip, 361)
			SetBlipScale(blip, 0.9)
			SetBlipColour(blip, 6)
			SetBlipDisplay(blip, 2)
			SetBlipAsShortRange(blip, true)

			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString(_U('gas_station'))
			EndTextCommandSetBlipName(blip)

			table.insert(StationsBlips, blip)
		end
	end)
end

function CreateJobBlips()
	Citizen.CreateThread(function()
		for _, v in ipairs(Config.Stations) do
			local blip = AddBlipForCoord(v.x, v.y, v.z)

			SetBlipSprite(blip, 361)
			SetBlipScale(blip, 0.9)
			if StationsInfos[v.name].quantity < 5 then
				SetBlipColour(blip, 40)
			elseif StationsInfos[v.name].quantity < StationsInfos[v.name].maximum * 25/100 then
				SetBlipColour(blip, 6)
			elseif StationsInfos[v.name].quantity < StationsInfos[v.name].maximum * 50/100 then
				SetBlipColour(blip, 17)
			elseif StationsInfos[v.name].quantity < StationsInfos[v.name].maximum * 75/100 then
				SetBlipColour(blip, 73)
			else
				SetBlipColour(blip, 69)
			end
			SetBlipDisplay(blip, 2)
			SetBlipAsShortRange(blip, true)

			BeginTextCommandSetBlipName("STRING")
			AddTextComponentString(_U('gas_station'))
			EndTextCommandSetBlipName(blip)

			table.insert(StationsBlips, blip)
		end
	end)
end

function DeleteBlips()
	if StationsBlips[1] ~= nil then
		for i=1, #StationsBlips, 1 do
			RemoveBlip(StationsBlips[i])
			StationsBlips[i] = nil
		end
	end
end


function DrawText3Ds(x,y,z, text)
	local onScreen,_x,_y = World3dToScreen2d(x,y,z)
	local px,py,pz = table.unpack(GetGameplayCamCoords())
	
	SetTextScale(0.45, 0.45)
	SetTextFont(4)
	SetTextProportional(1)
	SetTextColour(255, 255, 255, 215)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextEntry("STRING")
	SetTextCentre(1)
	AddTextComponentString(text)
	DrawText(_x,_y)
	local factor = (string.len(text)) / 370
	--DrawRect(_x,_y+0.0125, 0.015+ factor, 0.03, 41, 11, 41, 68)
end

function DrawAdvancedText(x,y ,w,h,sc, text, r,g,b,a,font,jus)
	SetTextFont(font)
	SetTextProportional(0)
	SetTextScale(sc, sc)
	N_0x4e096588b13ffeca(jus)
	SetTextColour(r, g, b, a)
	SetTextDropShadow(0, 0, 0, 0,255)
	SetTextEdge(1, 0, 0, 0, 255)
	SetTextDropShadow()
	SetTextOutline()
	SetTextEntry("STRING")
	AddTextComponentString(text)
	DrawText(x - 0.1+w, y - 0.02+h)
end

function loadAnimDict(dict)
	while(not HasAnimDictLoaded(dict)) do
		RequestAnimDict(dict)
		Citizen.Wait(1)
	end
end

function FuelVehicle()
	local ped 			= PlayerPedId()
	local coords		= GetEntityCoords(ped)
	local vehicle 	= GetPlayersLastVehicle()
	isEngineRunning = GetIsVehicleEngineRunning(vehicle)

	FreezeEntityPosition(ped, true)
	FreezeEntityPosition(vehicle, false)
	loadAnimDict("timetable@gardener@filling_can")
	TaskPlayAnim(ped, "timetable@gardener@filling_can", "gar_ig_5_filling_can", 1.0, 2, -1, 49, 0, 0, 0, 0)

	local boneIndex		= GetPedBoneIndex(ped, 18905) -- right hand : 57005 -- left hand : 18905
	local x,y,z = table.unpack(GetEntityCoords(ped))
	object = CreateObject(GetHashKey('prop_oilcan_02a'), x, y, z+0.2,	true,	true, true)
	AttachEntityToEntity(object, ped, boneIndex, 0.14, -0.12, 0.2, -70.0, 110.0, 10.0, 1, 1, 0, 1, 0, 1)
end

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(5)

		if not InBlacklistedVehicle then
			-- if Timer and not viewMode then
				-- DisplayHud()
			-- end

			if nearPump and IsCloseToLastVehicle then
				local vehicle	= GetPlayersLastVehicle()
				local fuel		= round(GetVehicleFuelLevel(vehicle), 1)
				
				if IsPedInAnyVehicle(PlayerPedId(), false) then
					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'], _U('must_exit_vehicle'))
				elseif IsFueling then
					local position = GetEntityCoords(vehicle)

					local colorFuel
					if fuel < 25 then colorFuel = '~r~'
					elseif fuel < 50 then colorFuel = '~o~'
					elseif fuel < 75 then colorFuel = '~y~'
					else colorFuel = '~g~'
					end

					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'], _U('press_to_cancel', round(price, 2)))
					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'] - 0.13, _U('station_name', pumpLoc['name'], pumpLoc['price']))
					DrawText3Ds(position.x, position.y, position.z + 0.5, _U('percentage_fuel', colorFuel, fuel) .. ' %')
					
					DisableControlAction(0, 0, true) -- Changing view (V)
					DisableControlAction(0, 22, true) -- Jumping (SPACE)
					DisableControlAction(0, 23, true) -- Entering vehicle (F)
					DisableControlAction(0, 24, true) -- Punching/Attacking
					DisableControlAction(0, 29, true) -- Pointing (B)
					DisableControlAction(0, 30, true) -- Moving sideways (A/D)
					DisableControlAction(0, 31, true) -- Moving back & forth (W/S)
					DisableControlAction(0, 37, true) -- Weapon wheel
					DisableControlAction(0, 44, true) -- Taking Cover (Q)
					DisableControlAction(0, 56, true) -- F9
					DisableControlAction(0, 82, true) -- Mask menu (,)
					DisableControlAction(0, 140, true) -- Hitting your vehicle (R)
					DisableControlAction(0, 166, true) -- F5
					DisableControlAction(0, 167, true) -- F6
					DisableControlAction(0, 168, true) -- F7
					DisableControlAction(0, 170, true) -- F3
					DisableControlAction(0, 288, true) -- F1
					DisableControlAction(0, 289, true) -- F2
					DisableControlAction(1, 323, true) -- Handsup (X)

					if IsControlJustReleased(0, 38) then
						loadAnimDict("reaction@male_stand@small_intro@forward")
						TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)

						IsFueling = false

						Citizen.Wait(500)
						DeleteObject(object)
						FreezeEntityPosition(PlayerPedId(), false)
						Citizen.Wait(500)
						ClearPedTasksImmediately(PlayerPedId())
						FreezeEntityPosition(vehicle, false)

						if PlayerData.job.name == 'fuel' then
							local normalPrice = price
							price = price * Config.DiscountForFuelJob / 100
							ESX.ShowNotification(_U('discount_as_fueljob', round((normalPrice - price), 0)))
						end
						TriggerServerEvent('LegacyFuel:PayFuel', price, currentStation, downFuel)

						downFuel = 0
						price = 0

					end
				elseif fuel > 95.0 then
					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'], _U('tank_full'))
				elseif cash <= 0 then
					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'], _U('not_enough_money'))
				else
					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'], _U('press_to_fill'))
					DrawText3Ds(pumpLoc['x'], pumpLoc['y'], pumpLoc['z'] - 0.13, _U('station_name', pumpLoc['name'], pumpLoc['price']))
					
					if IsControlJustReleased(0, 38) then

						if pumpLoc['quantity'] < 5 then
							ESX.ShowNotification(_U('not_enough_fuel'))
						else
							local vehicle = GetPlayersLastVehicle()
							local plate		= GetVehicleNumberPlateText(vehicle)
	
							ClearPedTasksImmediately(PlayerPedId())
	
							currentStation = pumpLoc['name']
							IsFueling = true
	
							FuelVehicle()
						end

					end
						
				end
			elseif NearVehicleWithJerryCan and not nearPump and Config.EnableJerryCans then
				local vehicle		= GetPlayersLastVehicle()
				local coords	 	= GetEntityCoords(vehicle)
				local fuel 		 	= round(GetVehicleFuelLevel(vehicle), 1)
				local jerrycan 	= GetAmmoInPedWeapon(PlayerPedId(), 883325847)
				
				if IsFuelingWithJerryCan then
					DrawText3Ds(coords.x, coords.y, coords.z + 0.5, _U('press_to_fill_jerrycan', fuel, jerrycan))

					DisableControlAction(0, 0, true) -- Changing view (V)
					DisableControlAction(0, 22, true) -- Jumping (SPACE)
					DisableControlAction(0, 23, true) -- Entering vehicle (F)
					DisableControlAction(0, 24, true) -- Punching/Attacking
					DisableControlAction(0, 29, true) -- Pointing (B)
					DisableControlAction(0, 30, true) -- Moving sideways (A/D)
					DisableControlAction(0, 31, true) -- Moving back & forth (W/S)
					DisableControlAction(0, 37, true) -- Weapon wheel
					DisableControlAction(0, 44, true) -- Taking Cover (Q)
					DisableControlAction(0, 56, true) -- F9
					DisableControlAction(0, 82, true) -- Mask menu (,)
					DisableControlAction(0, 140, true) -- Hitting your vehicle (R)
					DisableControlAction(0, 166, true) -- F5
					DisableControlAction(0, 167, true) -- F6
					DisableControlAction(0, 168, true) -- F7
					DisableControlAction(0, 170, true) -- F3
					DisableControlAction(0, 288, true) -- F1
					DisableControlAction(0, 289, true) -- F2
					DisableControlAction(1, 323, true) -- Handsup (X)

					if IsControlJustReleased(0, 38) then
						loadAnimDict("reaction@male_stand@small_intro@forward")
						TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)

						Citizen.Wait(2500)
						ClearPedTasksImmediately(PlayerPedId())
						FreezeEntityPosition(PlayerPedId(), false)
						FreezeEntityPosition(vehicle, false)

						IsFuelingWithJerryCan = false
					end
				else
					DrawText3Ds(coords.x, coords.y, coords.z + 0.5, _U('press_to_cancel_jerrycan'))

					if IsControlJustReleased(0, 38) then
						local vehicle = GetPlayersLastVehicle()
						local plate	 = GetVehicleNumberPlateText(vehicle)

						ClearPedTasksImmediately(PlayerPedId())

						IsFuelingWithJerryCan = true

						FuelVehicle()
					end
				end
			end
		else
			Citizen.Wait(1000)
		end
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(500)

		if IsFueling then
			local vehicle		= GetPlayersLastVehicle()
			local plate			= GetVehicleNumberPlateText(vehicle)
			local fuel 		 	= GetVehicleFuelLevel(vehicle)
			local integer		= math.random(6, 10)
			local fuelthis 	= integer / 10
			local newfuel		= fuel + fuelthis
			downFuel				= downFuel + fuelthis
			StationsInfos[currentStation].quantity = StationsInfos[currentStation].quantity - fuelthis
			
			price = price + (StationsInfos[currentStation].price * fuelthis)

			if cash >= price then
				TriggerServerEvent('LegacyFuel:CheckServerFuelTable', plate)
				Citizen.Wait(150)

				if newfuel < 100 then
					if StationsInfos[currentStation].quantity < 5 then
						SetVehicleFuelLevel(vehicle, newfuel)
						loadAnimDict("reaction@male_stand@small_intro@forward")
						TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)
						
						IsFueling = false
						
						ESX.ShowNotification(_U('no_more_fuel'))
						Citizen.Wait(500)
						DeleteObject(object)
						FreezeEntityPosition(PlayerPedId(), false)
						Citizen.Wait(500)
						ClearPedTasksImmediately(PlayerPedId())
						FreezeEntityPosition(vehicle, false)
						
						if PlayerData.job.name == 'fuel' then
							local normalPrice = price
							price = price * Config.DiscountForFuelJob / 100
							ESX.ShowNotification(_U('discount_as_fueljob', round((normalPrice - price), 0)))
						end
						TriggerServerEvent('LegacyFuel:PayFuel', price, currentStation, downFuel)
						
						downFuel = 0
						price = 0
						
						for i = 1, #Vehicles do
							if Vehicles[i].plate == plate then
								TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))
							
								table.remove(Vehicles, i)
								table.insert(Vehicles, {plate = plate, fuel = newfuel})
							
								break
							end
						end
					else
						if isEngineRunning then
							local randomVehicleBreak = math.random(1, 20)
							PlaySoundFromEntity(-1, "PED_PHONE_DIAL_01", vehicle, 0, 0, 0);
							if randomVehicleBreak == 1 then
								ESX.ShowNotification(_U('vehicle_on_fire_1'))
								ESX.ShowNotification(_U('vehicle_on_fire_2'))
								SetVehicleEngineHealth(vehicle, -20.0)
								SetVehicleEngineOn(vehicle, false, false, true)
								isEngineRunning = false
							end
						end

						SetVehicleFuelLevel(vehicle, newfuel)

						for i = 1, #Vehicles do
							if Vehicles[i].plate == plate then
								TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))

								table.remove(Vehicles, i)
								table.insert(Vehicles, {plate = plate, fuel = newfuel})

								break
							end
						end
					end
				else
					SetVehicleFuelLevel(vehicle, 100.0)
					loadAnimDict("reaction@male_stand@small_intro@forward")
					TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)

					IsFueling = false

					Citizen.Wait(500)
					DeleteObject(object)
					FreezeEntityPosition(PlayerPedId(), false)
					Citizen.Wait(500)
					ClearPedTasksImmediately(PlayerPedId())
					FreezeEntityPosition(vehicle, false)

					if PlayerData.job.name == 'fuel' then
						local normalPrice = price
						price = price * Config.DiscountForFuelJob / 100
						ESX.ShowNotification(_U('discount_as_fueljob', round((normalPrice - price), 0)))
					end
					TriggerServerEvent('LegacyFuel:PayFuel', price, currentStation, downFuel)

					downFuel = 0
					price = 0

					for i = 1, #Vehicles do
						if Vehicles[i].plate == plate then
							TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))

							table.remove(Vehicles, i)
							table.insert(Vehicles, {plate = plate, fuel = newfuel})

							break
						end
					end
				end
			else
				SetVehicleFuelLevel(vehicle, newfuel)
				loadAnimDict("reaction@male_stand@small_intro@forward")
				TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)

				IsFueling = false

				Citizen.Wait(500)
				DeleteObject(object)
				FreezeEntityPosition(PlayerPedId(), false)
				Citizen.Wait(500)
				ClearPedTasksImmediately(PlayerPedId())
				FreezeEntityPosition(vehicle, false)

				if PlayerData.job.name == 'fuel' then
					local normalPrice = price
					price = price * Config.DiscountForFuelJob / 100
					ESX.ShowNotification(_U('discount_as_fueljob', round((normalPrice - price), 0)))
				end
				TriggerServerEvent('LegacyFuel:PayFuel', price, currentStation, downFuel)

				downFuel = 0
				price = 0

				for i = 1, #Vehicles do
					if Vehicles[i].plate == plate then
						TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))

						table.remove(Vehicles, i)
						table.insert(Vehicles, {plate = plate, fuel = newfuel})

						break
					end
				end
			end
		elseif IsFuelingWithJerryCan then
			local vehicle	 = GetPlayersLastVehicle()
			local plate		 = GetVehicleNumberPlateText(vehicle)
			local fuel 			= GetVehicleFuelLevel(vehicle)
			local integer	 = math.random(6, 10)
			local fuelthis	= integer / 10
			local newfuel	 = fuel + fuelthis
			local jerryfuel = fuelthis * 100
			local jerrycurr = GetAmmoInPedWeapon(PlayerPedId(), 883325847)
			local jerrynew	= jerrycurr - jerryfuel

			if jerrycurr >= jerryfuel then
				TriggerServerEvent('LegacyFuel:CheckServerFuelTable', plate)
				Citizen.Wait(150)
				SetPedAmmo(PlayerPedId(), 883325847, round(jerrynew, 0))

				if newfuel < 100 then
					SetVehicleFuelLevel(vehicle, newfuel)

					for i = 1, #Vehicles do
						if Vehicles[i].plate == plate then
							TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))

							table.remove(Vehicles, i)
							table.insert(Vehicles, {plate = plate, fuel = newfuel})

							break
						end
					end
				else
					SetVehicleFuelLevel(vehicle, 100.0)
					loadAnimDict("reaction@male_stand@small_intro@forward")
					TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)

					Citizen.Wait(2500)
					ClearPedTasksImmediately(PlayerPedId())
					FreezeEntityPosition(PlayerPedId(), false)
					FreezeEntityPosition(vehicle, false)

					IsFuelingWithJerryCan = false

					for i = 1, #Vehicles do
						if Vehicles[i].plate == plate then
							TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))

							table.remove(Vehicles, i)
							table.insert(Vehicles, {plate = plate, fuel = newfuel})

							break
						end
					end
				end
			else
				loadAnimDict("reaction@male_stand@small_intro@forward")
				TaskPlayAnim(PlayerPedId(), "reaction@male_stand@small_intro@forward", "react_forward_small_intro_a", 1.0, 2, -1, 49, 0, 0, 0, 0)

				Citizen.Wait(2500)
				ClearPedTasksImmediately(PlayerPedId())
				FreezeEntityPosition(PlayerPedId(), false)
				FreezeEntityPosition(vehicle, false)

				IsFuelingWithJerryCan = false

				for i = 1, #Vehicles do
					if Vehicles[i].plate == plate then
						TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, round(GetVehicleFuelLevel(vehicle), 1))

						table.remove(Vehicles, i)
						table.insert(Vehicles, {plate = plate, fuel = newfuel})

						break
					end
				end
			end
		end
	end
end)

-- Citizen.CreateThread(function()
	-- while true do
		-- Citizen.Wait(250)

		-- if IsPedInAnyVehicle(PlayerPedId()) then
			-- Citizen.Wait(2500)

			-- Timer = true
		-- else
			-- Timer = false
		-- end
	-- end
-- end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1500)

		nearPump 			 					= false
		IsCloseToLastVehicle 		= false
		found 				 					= false
		NearVehicleWithJerryCan = false

		local myCoords = GetEntityCoords(PlayerPedId())
		
		for i = 1, #Config.models do
			local closestPump = GetClosestObjectOfType(myCoords.x, myCoords.y, myCoords.z, 2.0, Config.models[i], false, false)
			
			if closestPump ~= nil and closestPump ~= 0 then
				local coords		= GetEntityCoords(closestPump)
				local vehicle	 = GetPlayersLastVehicle()

				nearPump = true
				local stationName, stationPrice, stationQuantity
				for _,v in ipairs(Config.Stations) do
					if GetDistanceBetweenCoords(coords, v.x, v.y, v.z, true) < 50.0 then
						stationName = v.name
					end
				end
				if StationsInfos[stationName] == nil then
					Citizen.Wait(1000)
				end

				pumpLoc	= {['x'] = coords.x, ['y'] = coords.y, ['z'] = coords.z + 1.2, ['name'] = stationName, ['price'] = StationsInfos[stationName].price, ['quantity'] = StationsInfos[stationName].quantity, ['maximum'] = StationsInfos[stationName].maximum}

				if vehicle ~= nil then
					local vehcoords = GetEntityCoords(vehicle)
					local mycoords	= GetEntityCoords(PlayerPedId())
					local distance	= GetDistanceBetweenCoords(vehcoords.x, vehcoords.y, vehcoords.z, mycoords.x, mycoords.y, mycoords.z)

					if distance < 3.3 then
						IsCloseToLastVehicle = true
					end
				end
				break
			end
		end

		if IsPedInAnyVehicle(PlayerPedId(), false) then
			local vehicle = GetPlayersLastVehicle()
			local plate	 	= GetVehicleNumberPlateText(vehicle)
			local fuel 		= GetVehicleFuelLevel(vehicle)
			local found	 	= false

			TriggerServerEvent('LegacyFuel:CheckServerFuelTable', plate)

			Citizen.Wait(500)

			for i = 1, #Vehicles do
				if Vehicles[i].plate == plate then
					found = true
					fuel	= round(Vehicles[i].fuel, 1)

					break
				end
			end

			if not found then
				integer = math.random(300, 800)
				fuel 	= integer / 10

				table.insert(Vehicles, {plate = plate, fuel = fuel})

				TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, fuel)
			end

			SetVehicleFuelLevel(vehicle, fuel)
		end

		local currentVeh = GetDisplayNameFromVehicleModel(GetEntityModel(GetVehiclePedIsUsing(PlayerPedId())))

		for i = 1, #Config.blacklistedVehicles do
			if Config.blacklistedVehicles[i] == currentVeh then
				InBlacklistedVehicle = true
				found 				 = true
				
				break
			end
		end

		if not found then
			InBlacklistedVehicle = false
		end

		if nearPump then
			TriggerServerEvent('LegacyFuel:CheckCashOnHand')
		end

		
		local CurrentWeapon = GetSelectedPedWeapon(PlayerPedId())	
		if CurrentWeapon == 883325847 then
			local MyCoords 		= GetEntityCoords(PlayerPedId())
			local Vehicle			= GetClosestVehicle(MyCoords.x, MyCoords.y, MyCoords.z, 3.0, false, 23)

			if Vehicle ~= 0 then
				NearVehicleWithJerryCan = true
			end
		end
		
	end
end)

function round(num, numDecimalPlaces)
  if numDecimalPlaces and numDecimalPlaces>0 then
    local mult = 10^numDecimalPlaces
    return math.floor(num * mult + 0.5) / mult
  end
  return math.floor(num + 0.5)
end

function GetSeatPedIsIn(ped)
	local vehicle = GetVehiclePedIsIn(ped, false)

	for i = -2, GetVehicleMaxNumberOfPassengers(vehicle) do
		if GetPedInVehicleSeat(vehicle, i) == ped then
			return i
		end
	end

	return -2
end

function DisplayHud()
	if IsPedInAnyVehicle(PlayerPedId(), false) and GetSeatPedIsIn(PlayerPedId()) == -1 then
		local vehicle 		= GetPlayersLastVehicle()
		local fuel				= math.ceil(round(GetVehicleFuelLevel(vehicle), 1))
		local fuelDisplay = fuel * 0.001535

		DrawRect(0.177, 0.888, 0.0080, 0.160, 0, 0, 0, 180)
		DrawRect(0.177, 0.888, 0.0050, 0.1535, 80, 80, 80, 180)
		DrawRect(0.177, 0.888, 0.0050, fuelDisplay, 228, 90, 43, 200)
	end
end

RegisterNetEvent('LegacyFuel:ReturnFuelFromServerTable')
AddEventHandler('LegacyFuel:ReturnFuelFromServerTable', function(vehInfo)
	local fuel	 = round(vehInfo.fuel, 1)

	for i = 1, #Vehicles do
		if Vehicles[i].plate == vehInfo.plate then
			table.remove(Vehicles, i)

			break
		end
	end

	table.insert(Vehicles, {plate = vehInfo.plate, fuel = fuel})
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(5000)

		local vehicle = GetVehiclePedIsIn(PlayerPedId())
		local engine	= GetIsVehicleEngineRunning(vehicle)

		if vehicle and engine then
			if not InBlacklistedVehicle then
				local plate				 = GetVehicleNumberPlateText(vehicle)
				local rpm 		 		 = GetVehicleCurrentRpm(vehicle)
				local fuel		 		 = GetVehicleFuelLevel(vehicle)
				local rpmfuelusage = 0
				if rpm > 0.9 then
					rpmfuelusage = fuel - rpm / 3.0 -- 0.818
					Citizen.Wait(1000)
				elseif rpm > 0.8 then
					rpmfuelusage = fuel - rpm / 3.5 -- 0.615
					Citizen.Wait(1500)
				elseif rpm > 0.7 then
					rpmfuelusage = fuel - rpm / 4.0 -- 0.304
					Citizen.Wait(2000)
				elseif rpm > 0.6 then
					rpmfuelusage = fuel - rpm / 4.5-- 0.146
					Citizen.Wait(3000)
				elseif rpm > 0.5 then
					rpmfuelusage = fuel - rpm / 5.0 -- 0.087
					Citizen.Wait(4000)
				elseif rpm > 0.4 then
					rpmfuelusage = fuel - rpm / 5.5 -- 0.062
					Citizen.Wait(5000)
				elseif rpm > 0.3 then
					rpmfuelusage = fuel - rpm / 6.0 -- 0.043
					Citizen.Wait(6000)
				elseif rpm > 0.20 then
					rpmfuelusage = fuel - rpm / 6.5 -- 0.026
					Citizen.Wait(8000)
				else
					rpmfuelusage = fuel - rpm / 7.0 -- 0.0064
					Citizen.Wait(15000)
				end

				for i = 1, #Vehicles do
					if Vehicles[i].plate == plate then
						SetVehicleFuelLevel(vehicle, rpmfuelusage)

						local updatedfuel = round(GetVehicleFuelLevel(vehicle), 1)

						if updatedfuel ~= 0 then
							TriggerServerEvent('LegacyFuel:UpdateServerFuelTable', plate, updatedfuel)

							table.remove(Vehicles, i)
							table.insert(Vehicles, {plate = plate, fuel = rpmfuelusage})
						end

						break
					end
				end

				if rpmfuelusage < Config.VehicleFailure then
					SetVehicleUndriveable(vehicle, true)
				elseif rpmfuelusage == 0 then
					SetVehicleEngineOn(vehicle, false, false, false)
				--else
					--SetVehicleUndriveable(vehicle, false)
				end
			end
		end
	end
end)

RegisterNetEvent('LegacyFuel:RecieveCashOnHand')
AddEventHandler('LegacyFuel:RecieveCashOnHand', function(cb)
	cash = cb
end)

function dump(o, nb)
  if nb == nil then
    nb = 0
  end
   if type(o) == 'table' then
      local s = ''
      for i = 1, nb + 1, 1 do
        s = s .. "    "
      end
      s = '{\n'
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
          for i = 1, nb, 1 do
            s = s .. "    "
          end
         s = s .. '['..k..'] = ' .. dump(v, nb + 1) .. ',\n'
      end
      for i = 1, nb, 1 do
        s = s .. "    "
      end
      return s .. '}'
   else
      return tostring(o)
   end
end