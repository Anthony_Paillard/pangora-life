Locales['fr'] = {

	['must_exit_vehicle'] 				= '~r~Vous devez quitter votre véhicule pour faire le plein !~s~',
	['press_to_cancel']						= 'appuyez sur ~b~E~s~ pour ~r~arrêter~s~ le remplissage de votre réservoir. ($~o~%s~s~)',
	['press_to_fill']							= 'appuyez sur ~b~E~s~ pour ~g~débuter~s~ le remplissage de votre réservoir',
	['press_to_cancel_jerrycan']	= 'press ~n~E~w~ to ~r~cancel~s~ fueling the vehicle. Currently at: %s% - Jerry Can: %s',
	['press_to_fill_jerrycan']		= 'press ~n~E~w~ to ~g~fuel~s~ the vehicle with your gas can',
	['percentage_fuel']						= '%s%s~s~',
	['tank_full']									= '~r~Le réservoir du véhicule est plein !~s~',
	['not_enough_money']					= '~r~Vous n\'avez pas assez d\'argent sur vous pour acheter du carburant~s~',
	['gas_station']								= 'station essence',
	['you_paid']									= 'vous avez payé $~o~%s~s~ d\'essence à la station',
	['station_name']							= '[~y~%s~s~] ($~g~%s~s~/L)',
	['not_enough_fuel']						= '~r~Il n\'y a pas assez d\'essence dans la station pour remplir votre véhicule...',
	['no_more_fuel']							= '~r~Il n\'y a plus assez d\'essence dans la station pour continuer le remplissage...',
	['vehicle_on_fire_1']					= '~r~Vous n\'aviez pas éteint votre moteur avant de remplir votre réservoir d\'essence...~s~',
	['vehicle_on_fire_2']					= 'le moteur de votre véhicule est désormais ~o~endommagé~s~',

	-- fuel society
	['not_enough_item']						= '~r~Vous n\'avez pas de bidons d\'essence pour remplir la station...~s~',
	['refilling_station']					= 'vous venez de remplir la station pour un montant de $~g~%s~s~',
	['comp_earned']								= 'votre société a gagné un montant de $~g~%s~s~',
	['station_already_full']			= '~r~La station essence est déjà pleine...~s~',
	['discount_as_fueljob']				= 'vous profitez d\'une réduction de $~g~%s~s~ en travaillant dans cette société',

}