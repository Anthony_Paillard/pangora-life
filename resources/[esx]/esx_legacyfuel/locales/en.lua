Locales['en'] = {

	['must_exit_vehicle'] 				= '~r~You must leave your vehicle before refueling!~s~',
	['press_to_cancel']						= 'press ~b~E~s~ to ~r~cancel~s~ filling your tank. ($~o~%s~s~)',
	['press_to_fill']							= 'press~b~E~s~ pour ~g~start~s~ filling your tank',
	['press_to_cancel_jerrycan']	= 'press ~n~E~w~ to ~r~cancel~s~ fueling the vehicle. Currently at: %s% - Jerry Can: %s',
	['press_to_fill_jerrycan']		= 'press ~n~E~w~ to ~g~fuel~s~ the vehicle with your gas can',
	['percentage_fuel']						= '%s%s~s~',
	['tank_full']									= '~r~The vehicle\'s tank is full~s~',
	['not_enough_money']					= '~r~You don\'t have enough money to buy gasoline...~s~',
	['gas_station']								= 'gas station',
	['you_paid']									= 'you paid $~o~%s~s~ of gasoline in the gas station',
	['station_name']							= '[~y~%s~s~] ($~g~%s~s~/L)',
	['not_enough_fuel']						= '~r~There is not enough gasoline in the gas station to refill up your car...',
	['no_more_fuel']							= '~r~There is no more gasoline in the gas station to continue...',

	-- fuel society
	['not_enough_item']						= '~r~You don\'t have gasoline can to refill the gas station...~s~',
	['refilling_station']					= 'you just refill up the station for $~g~%s~s~',
	['comp_earned']								= 'your society just earned $~g~%s~s~',
	['station_already_full']			= '~r~The gas station is already full...~s~',
	['discount_as_fueljob']				= 'you have a discount of $~g~%s~s~ by working in this company',

}