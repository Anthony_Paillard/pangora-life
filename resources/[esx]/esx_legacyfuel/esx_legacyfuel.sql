CREATE TABLE `fuel_stations` (
	`name` VARCHAR(100) NOT NULL COLLATE 'utf8mb4_bin',
	`price` FLOAT NOT NULL,
	`quantity` FLOAT NOT NULL,
	`maximum` INT(11) NOT NULL,
	PRIMARY KEY (`name`)
)
COLLATE='utf8mb4_bin'
ENGINE=InnoDB
;


INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Alta Street', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Clinton Avenue', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Great Ocean Highway #1', 5, 400, 400);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Panorama Drive', 5, 200, 200);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Route 68 #1', 5, 200, 200);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Route 68 #2', 5, 400, 400);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Route 68 #3', 5, 300, 300);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('GLOBE OIL Señora Way', 5, 100, 100);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('LTD Grapeseed Main Street', 5, 200, 200);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('LTD Grove Street', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('LTD Lindsay Circus', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('LTD Mirror Park Boulevard', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('LTD Rockford Drive North', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('LTD Rockford Drive South', 5, 400, 400);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('RON Capital Boulevard', 5, 400, 400);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('RON Great Ocean Highway #1', 5, 400, 400);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('RON Palomino Freeway', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('RON Popular Street', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('RON Route 68', 5, 600, 600);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Calais Avenue', 5, 800, 800);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Del Perro Freeway', 5, 900, 900);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Marina Drive', 5, 400, 400);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Paleto Boulevard', 5, 200, 200);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Route 68', 5, 100, 100);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Señora Freeway', 5, 300, 300);
INSERT INTO `fuel_stations` (`name`, `price`, `quantity`, `maximum`) VALUES ('XERO Strawberry Avenue', 5, 900, 900);
