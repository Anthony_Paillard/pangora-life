ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) 
ESX = obj 
Wait(2000)
    setOwnedFuelLevelInTable()
	-- setOwnedSocietyFuelLevelInTable()
	fillStationsInfos()
end)

local StationsInfos = {}
local Vehicles = {
	{ plate = '00000000', fuel = 0}
}

function fillStationsInfos()
	MySQL.Async.fetchAll("SELECT name, price, quantity, maximum FROM fuel_stations",{}, function(data) 
		for _,v in pairs(data) do
			StationsInfos[v.name]= {price = round(v.price, 2), quantity = round(v.quantity, 1), maximum = v.maximum}
		end
	end)
end

ESX.RegisterServerCallback('LegacyFuel:requestStationsInfos', function(source, cb)
	cb(StationsInfos)
end)

function setOwnedFuelLevelInTable()
	MySQL.Async.fetchAll("SELECT vehicle FROM owned_vehicles",{}, function(data)
		if data ~= nil then
			for _,v in pairs(data) do
				local vehicle = json.decode(v.vehicle)
				local plateOwnedVehicle 	= vehicle.plate
				if vehicle.modelName ~= nil then
					local fuelOwnedVehicle = round(vehicle.fuelLevel, 1)
					table.insert(Vehicles, {plate = plateOwnedVehicle, fuel = fuelOwnedVehicle})
				end
			end
		end
	end)
end

function setOwnedSocietyFuelLevelInTable()
	MySQL.Async.fetchAll("SELECT vehicle FROM owned_vehicles_society",{}, function(data)
		if data ~= nil then
			for _,v in pairs(data) do
				local vehicle = json.decode(v.vehicle)
				local plateOwnedVehicle 	= vehicle.plate
				if vehicle.modelName ~= nil then
					local fuelOwnedVehicle = round(vehicle.fuelLevel, 1)
					table.insert(Vehicles, {plate = plateOwnedVehicle, fuel = fuelOwnedVehicle})
				end
			end
		end
	end)
end

function setFuelLevelInStation(station, quantity)
	MySQL.Async.execute("UPDATE fuel_stations SET quantity =@quantity WHERE name=@station",{['@quantity'] = quantity, ['@station'] = station})
	Citizen.Wait(100)
	TriggerClientEvent('LegacyFuel:newStationQuantityToPlayers', -1, station, quantity)
end

function setPriceInStation(station, price)
	MySQL.Async.execute("UPDATE fuel_stations SET price =@price WHERE name=@station",{['@price'] = price, ['@station'] = station})
	Citizen.Wait(100)
	TriggerClientEvent('LegacyFuel:newStationPriceToPlayers', -1, station, price)
end

RegisterServerEvent('LegacyFuel:PayFuel')
AddEventHandler('LegacyFuel:PayFuel', function(price, station, newStationQuantity)
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local amount	= round(price, 0)
	local newStationQuantity = round(newStationQuantity, 1)

	StationsInfos[station].quantity = StationsInfos[station].quantity - newStationQuantity
	
	if StationsInfos[station].quantity < 0 then
		StationsInfos[station].quantity = 0
	end

	setFuelLevelInStation(station, StationsInfos[station].quantity)

	local societyAccount = nil
	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_fuel', function(account)
		societyAccount = account
	end)
	if societyAccount ~= nil then
		societyAccount.addMoney(amount)
	end

	xPlayer.removeMoney(amount)
	TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_paid', amount))
end)

RegisterServerEvent('LegacyFuel:refillStation')
AddEventHandler('LegacyFuel:refillStation', function(station)
	local _source			 	= source
	local xPlayer				= ESX.GetPlayerFromId(_source)

	local itemQuantity 		= xPlayer.getInventoryItem('essence').count
	local stationQuantity = StationsInfos[station].quantity
	local stationMaximum 	= StationsInfos[station].maximum
	
	if itemQuantity == 0 then
		TriggerClientEvent('esx:showNotification', _source, _U('not_enough_item'))
	elseif stationQuantity > stationMaximum - Config.FuelQuantityInItem then
		TriggerClientEvent('esx:showNotification', _source, _U('station_already_full'))
	else

		local quantityMaxToRefill 		= math.floor(stationMaximum - stationQuantity)
		local itemQuantityMaxToRefill	= math.floor(quantityMaxToRefill / Config.FuelQuantityInItem)

		if itemQuantity > itemQuantityMaxToRefill then
			itemQuantity = itemQuantityMaxToRefill
		end

		local societyAccount
		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_fuel', function(account)
			societyAccount = account
		end)
		local finalPrice = round(((StationsInfos[station].price * Config.SellingPercentage / 100) * itemQuantity), 0)
		if societyAccount ~= nil then
			xPlayer.removeInventoryItem('essence', itemQuantity)
			xPlayer.addMoney(tonumber(finalPrice))
			societyAccount.addMoney(tonumber(finalPrice))
			TriggerClientEvent('esx:showNotification', _source, _U('comp_earned', finalPrice))
		else
			xPlayer.removeInventoryItem('essence', itemQuantity)
			xPlayer.addMoney(tonumber(finalPrice))
		end
		local newStationQuantity = round(stationQuantity + (itemQuantity * Config.FuelQuantityInItem), 1)

		StationsInfos[station].quantity = newStationQuantity
		setFuelLevelInStation(station, newStationQuantity)
		TriggerClientEvent('esx:showNotification', _source, _U('refilling_station', finalPrice))

	end
end)

RegisterServerEvent('LegacyFuel:changeStationPrice')
AddEventHandler('LegacyFuel:changeStationPrice', function(stationName, newPrice)
	local newPrice = round(newPrice, 2)
	StationsInfos[stationName].price = newPrice
	setPriceInStation(stationName, newPrice)
end)

RegisterServerEvent('LegacyFuel:UpdateServerFuelTable')
AddEventHandler('LegacyFuel:UpdateServerFuelTable', function(plate, fuel)
	local found = false

	for i = 1, #Vehicles do
		if Vehicles[i].plate == plate then 
			found = true
			if fuel ~= Vehicles[i].fuel then
				table.remove(Vehicles, i)
				table.insert(Vehicles, {plate = plate, fuel = fuel})
			end
			break 
		end
	end

	if not found then
		table.insert(Vehicles, {plate = plate, fuel = fuel})
	end
end)

RegisterServerEvent('LegacyFuel:CheckServerFuelTable')
AddEventHandler('LegacyFuel:CheckServerFuelTable', function(plate)
	local _source = source
	for i = 1, #Vehicles do
		if Vehicles[i].plate == plate then
			local vehInfo = {plate = Vehicles[i].plate, fuel = Vehicles[i].fuel}
			TriggerClientEvent('LegacyFuel:ReturnFuelFromServerTable', _source, vehInfo)
			break
		end
	end
end)

RegisterServerEvent('LegacyFuel:CheckCashOnHand')
AddEventHandler('LegacyFuel:CheckCashOnHand', function()
	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local cb 			= xPlayer.getMoney()

	TriggerClientEvent('LegacyFuel:RecieveCashOnHand', _source, cb)
end)

function round(num, numDecimalPlaces)
  if numDecimalPlaces and numDecimalPlaces>0 then
    local mult = 10^numDecimalPlaces
    return math.floor(num * mult + 0.5) / mult
  end
  return math.floor(num + 0.5)
end

AddEventHandler('onMySQLReady', function()
    setOwnedFuelLevelInTable()
	-- setOwnedSocietyFuelLevelInTable()
	fillStationsInfos()
end)