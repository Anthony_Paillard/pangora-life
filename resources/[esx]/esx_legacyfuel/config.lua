Config = {}
Config.Locale = 'fr'

Config.EnableBlips						= true
Config.EnableJerryCans				= true
Config.EnableBuyableJerryCans	= false -- Coming soon, currently useless
Config.VehicleFailure					= 10 -- At what fuel-percentage should the engine stop functioning properly? (Defualt: 10)

Config.FuelQuantityInItem			= 2	-- L
Config.SellingPercentage			= 80 -- % of the price station
Config.DiscountForFuelJob			= 95 -- % of the price station

Config.blacklistedVehicles = {
	[1] = 'BMX',
	[2] = 'CRUISER',
	[3] = 'FIXTER',
	[4] = 'SCORCHER',
	[5] = 'TRIBIKE',
	[6] = 'TRIBIKE2',
	[7] = 'TRIBIKE3',
	[8] = 'TUG',
	[9] = 'LUXOR',
	[10] = 'LUXOR2',
	[11] = 'MICROLIGHT',
	[12] = 'VELUM2',
	[13] = 'DINGHY',
	[14] = 'JETMAX',
	[15] = 'MARQUIS',
	[16] = 'PREDATOR',
	[17] = 'SEASHARK',
	[18] = 'SPEEDER',
	[19] = 'SQUALO',
	[20] = 'TORO',
	[21] = 'SUNTRAP',
	[22] = 'TROPIC',
	[23] = 'BUZZARD2',
	[24] = 'CARGOBOB',
	[25] = 'CARGOBOB2',
	[26] = 'FROGGER',
	[27] = 'FROGGER2',
	[28] = 'MAVERICK',
	[29] = 'SPARROW',
	[30] = 'SVOLITO',
	[31] = 'SVOLITO2',
	[32] = 'SWIFT',
	[33] = 'SWIFT2',
	[34] = 'VALKYRIE',
	[35] = 'VOLATUS',
	[36] = 'polmav'
}

Config.electric_model = {
	"VOLTIC",
	"SURGE",
	"DILETTANTE",
	"KHAMELION",
	"CADDY",
	"CADDY2",
	"AIRTUG"
}

Config.electric_Stations = {
    {x=-55.938,y=-1756.631,z=29.121},
    {x=1217.986,y=-1385.517,z=35.139},
    {x=286.122,y=-1264.363,z=29.284},
    {x=-545.695,y=-1222.068,z=18.279},
    {x=820.384,y=-1038.054,z=26.549},
    {x=-714.51,y=-919.327,z=19.014},
    {x=1170.2,y=-320.836,z=69.177},
    {x=-1414.777,y=-279.977,z=46.293},
    {x=-2076.135,y=-324.185,z=13.158},
    {x=626.667,y=249.346,z=103.046},
    {x=2563.333,y=353.385,z=108.464},
    {x=-1816.798,y=798.601,z=138.106},
    {x=2541.891,y=2609.431,z=37.945},
    {x=1189.907,y=2658.11,z=37.831},
    {x=1031.292,y=2660.109,z=39.551},
    {x=265.647,y=2600.547,z=44.774},
    {x=63.991,y=2787.675,z=57.88},
    {x=-2553.863,y=2321.008,z=33.06},
    {x=2680.59,y=3274.363,z=55.241},
    {x=1984.787,y=3783.526,z=32.181},
    {x=1696.633,y=4917.12,z=42.078},
    {x=1689.197,y=6435.085,z=32.559},
    {x=152.939,y=6629.043,z=31.717},
    {x=-92.825,y=6393.375,z=31.452}
}

Config.models = {
	[1] = -2007231801,
	[2] = 1339433404,
	[3] = 1694452750,
	[4] = 1933174915,
	[5] = -462817101,
	[6] = -469694731,
	[7] = -164877493
}

Config.Stations = {
	{ x = 49.4187,	 	y = 2778.793,		z = 58.043,		name = 'XERO Route 68' },
 	{ x = 263.894,	 	y = 2606.463,		z = 44.983,		name = 'GLOBE OIL Route 68 #1' },
 	{ x = 1039.958,		y = 2671.134,		z = 39.550,		name = 'GLOBE OIL Route 68 #2' },
 	{ x = 1207.260,		y = 2660.175,		z = 37.899,		name = 'GLOBE OIL Route 68 #3' },
 	{ x = 2539.685,		y = 2594.192,		z = 37.944,		name = 'GLOBE OIL Señora Way' },
 	{ x = 2679.858,		y = 3263.946,		z = 55.240,		name = 'XERO Señora Freeway' },
 	{ x = 2005.055,		y = 3773.887,		z = 32.403,		name = 'XERO Marina Drive' },
 	{ x = 1687.156,		y = 4929.392,		z = 42.078,		name = 'LTD Grapeseed Main Street' },
 	{ x = 1701.314,		y = 6416.028,		z = 32.763,		name = 'GLOBE OIL Great Ocean Highway #1' },
 	{ x = 179.857,	 	y = 6602.839,		z = 31.868,		name = 'RON Great Ocean Highway #1' },
 	{ x = -94.4619,		y = 6419.594,		z = 31.489,		name = 'XERO Paleto Boulevard' },
 	{ x = -2554.996, 	y = 2334.40,		z = 33.078,		name = 'RON Route 68' },
 	{ x = -1800.375, 	y = 803.661,		z = 138.651,	name = 'LTD Rockford Drive North' },
 	{ x = -1437.622, 	y = -276.747,		z = 46.207,		name = 'LTD Rockford Drive South' },
 	{ x = -2096.243, 	y = -320.286,		z = 13.168,		name = 'XERO Del Perro Freeway' },
 	{ x = -724.619, 	y = -935.1631,	z = 19.213,		name = 'LTD Lindsay Circus' },
 	{ x = -526.019, 	y = -1211.003,	z = 18.184,		name = 'XERO Calais Avenue' },
 	{ x = -70.2148, 	y = -1761.792,	z = 29.534,		name = 'LTD Grove Street' },
	{ x = 265.648,		y = -1261.309,	z = 29.292,		name = 'XERO Strawberry Avenue' },
 	{ x = 819.653,		y = -1028.846,	z = 26.403, 	name = 'RON Popular Street' },
	{ x = 1208.951, 	y = -1402.567, 	z = 35.224,		name = 'RON Capital Boulevard' },
 	{ x = 1181.381, 	y = -330.847,		z = 69.316,		name = 'LTD Mirror Park Boulevard' },
 	{ x = 620.843,		y =	269.100,		z = 103.089,	name = 'GLOBE OIL Clinton Avenue' },
	{ x = 2581.321, 	y = 362.039, 		z = 108.468,	name = 'RON Palomino Freeway' },
	{ x = -319.665, 	y = -1471.486, 	z = 29.549,		name = 'GLOBE OIL Alta Street' },
	{ x = 1786.066, 	y = 3330.974, 	z = 40.374,		name = 'GLOBE OIL Panorama Drive' }
}
